;;=====================================================================================
;;=====================================================================================
;;=========================== remove-applic-lambda-nil: ===============================
;;=====================================================================================
;;===================================================================================== 
(define remove-applic-lambda-nil
	(lambda (sexpr)
		;(display "sexpr: ") (display sexpr) (newline)
		(cond 
 			((and 
				(list? sexpr)
				(eq? (length sexpr) 3)
				(eq? (car sexpr) 'applic)
				(null? (caddr sexpr))
				; inner lambda
				(list? (cadr sexpr))
				(or 
					(and 
						(eq? (caadr sexpr) 'lambda-simple)
						(null? (cadadr sexpr)))
					(and
						(eq? (caadr sexpr) 'lambda-opt)
						(null? (cadadr sexpr))
						(list? (car (cddadr sexpr))))))
 			 ;(display "sexpr case 1: ") (display sexpr) (newline)
			 (remove-applic-lambda-nil (car (cddadr sexpr))))
			((and 
				(list? sexpr)
				(not (null? sexpr)))
			 ;(display "sexpr case 2: ") (display sexpr) (newline)
			 (cons (remove-applic-lambda-nil (car sexpr))
			 	   (remove-applic-lambda-nil (cdr sexpr))))
			(else 
			 ;(display "sexpr case 3: ") (display sexpr) (newline)
			 sexpr))))
;;=====================================================================================
;;=====================================================================================
;;=========================== remove-applic-lambda-nil: ===============================
;;=====================================================================================
;;===================================================================================== 






;;=====================================================================================
;;=====================================================================================
;;=================================== box-set: ========================================
;;=====================================================================================
;;=====================================================================================
(define box-set
	(lambda (sexpr)
		(box-set-traverse sexpr '())))


;; We box a variable only if all are true:
;; • It has a bound occurrence in the body of the procedure
;; • It is set (via a set!-expression) somewhere in the body of the procedure.
;; • It has a get-occurrence somewhere in the body of the procedure. A get means that it either
;; 	 appears as a (pvar name minor) or (bvar name major minor).
;;
;; To box a variable (pvar name minor), we must do 3 things:
;; 1. Add the expression (set (pvar name minor) (box (pvar name minor))) right under the
;; 	  lambda-expression in which it is defined.
;; 2. Replace any get-occurrences of v with box-get records. These occurrences can be either as
;; 	  parameters or as bound variables, depending on where the variable occurrence was found.
;; 3. Replace any set-occurrences of v with box-set records. These occurrences can be either as
;; 	  parameters or as bound variables, depending on where the variable occurrence was found.
;;
;; sexpr is an expression that we want to annotate for boxing of variables.
;; params-to-box is a list of variable names.
;; the idea is to always update a list of variable names and to use them to box the
;; variables as we traverse the expression.
;;
;; Invariant about params-to-box: there will not be 2 records with the same
;; variable name. Each var name that has a record will has its record updated
;; as we pass through different scopes.
(define box-set-traverse
	(lambda (sexpr params-to-box)
		(cond 
			((not (list? sexpr)) sexpr)
			((null? sexpr) sexpr)
			((eq? (car sexpr) 'set)
				(update-set-to-box-set sexpr params-to-box))
			((eq? (car sexpr) 'var)
				(update-var-to-box-get sexpr params-to-box))
			((eq? (car sexpr) 'lambda-simple) 
				(list
					'lambda-simple ;; lambda-simple
					(cadr sexpr) ;; the normal parameters
					(if (null? (update-params-to-box '() (cadr sexpr) (cddr sexpr)))
						(car (box-set-traverse (cddr sexpr) (update-params-to-box params-to-box (cadr sexpr) (cddr sexpr))))
						(list ;; the sequence of everything else
							'seq ;; seq
							(append
								(map (lambda (param) (list 'set (list 'var param) (list 'box (list 'var param)))) (update-params-to-box '() (cadr sexpr) (cddr sexpr))) ;; a list of the set expressions in beginning of lambda
								(if (and (list? (caddr sexpr)) (eq? (caaddr sexpr) 'seq)) ;; if the body of the lambda is/isn't a sequence
									(map (lambda (body-item) (box-set-traverse body-item (update-params-to-box params-to-box (cadr sexpr) (cddr sexpr)))) (cadr (caddr sexpr))) ;; a list of the body, case 1 (is)
									(box-set-traverse (cddr sexpr) (update-params-to-box params-to-box (cadr sexpr) (cddr sexpr))))))))) ;; a list of the body, case 2 (isn't)
			((eq? (car sexpr) 'lambda-opt)
				(list
					'lambda-opt
					(cadr sexpr)
					(caddr sexpr)
					(if (null? (update-params-to-box '() (append (cadr sexpr) (list (caddr sexpr))) (cadddr sexpr)))
						(car (box-set-traverse (cdddr sexpr) (update-params-to-box params-to-box (append (cadr sexpr) (list (caddr sexpr)))  (cadddr sexpr))))
						(list 
							'seq
							(append
								(map (lambda (param) (list 'set (list 'var param) (list 'box (list 'var param)))) (update-params-to-box '() (append (cadr sexpr) (list (caddr sexpr))) (cadddr sexpr)))
								(if (and (list? (cadddr sexpr)) (eq? (car (cadddr sexpr)) 'seq)) ;; if the body of the lambda is/isn't a sequence
									(map (lambda (body-item) (box-set-traverse body-item (update-params-to-box params-to-box (append (cadr sexpr) (list (caddr sexpr))) (cadddr sexpr)))) (cadr (cadddr sexpr)))
									(box-set-traverse (cdddr sexpr) (update-params-to-box params-to-box (append (cadr sexpr) (list (caddr sexpr)))  (cadddr sexpr)))))))))
			(else
				(map (lambda (x) (box-set-traverse x params-to-box)) sexpr)))))

;; we get an s-expression in the form of '(set (var <name>) <sexpr>).
;; we also get the list of params eligible to boxing.
;; if (cadadr set-sexpr) is in the list, we return '(box-set (var <name>) (box-set-traverse <sexpr> params-to-box)).
;; if (cadadr set-sexpr) is not in the list, we return '(set (var <name>) (box-set-traverse <sexpr> params-to-box)).
(define update-set-to-box-set
	(lambda (set-sexpr params-to-box)
		;(display "update-set-to-box-set set-sexpr: ") (display set-sexpr) (newline)
		;(display "update-set-to-box-set params-to-box: ") (display params-to-box) (newline) (newline)
		(if (null? (filter (lambda (param-to-box) (eq? (cadadr set-sexpr) param-to-box)) params-to-box))
			(list 'set (cadr set-sexpr) (box-set-traverse (caddr set-sexpr) params-to-box))
			(list 'box-set (cadr set-sexpr) (box-set-traverse (caddr set-sexpr) params-to-box)))))

;; we get an s-expression in the form of '(var <name>).
;; we also get the list of params eligible to boxing.
;; if (cadr var-sexpr) is in the list, we return '(box-get (var <name>)).
;; if (cadr var-sexpr) is not in the list, we return '(var <name>).
(define update-var-to-box-get
	(lambda (var-sexpr params-to-box)
		;(display "update-var-to-box-get var-sexpr: ") (display var-sexpr) (newline)
		;(display "update-var-to-box-get params-to-box: ") (display params-to-box) (newline) (newline)
		(if (null? (filter (lambda (param-to-box) (eq? (cadr var-sexpr) param-to-box)) params-to-box))
			var-sexpr
			(list 'box-get var-sexpr))))

;; The function receives the old parameters to box (we also call it the environment),
;; the parameters of the current lambda, and also the body of the current lambda.
;; The body can be single or a sequence, it doesnt matter.
;; The function will return a new list of parameters to box.
;; 
;; Algorithm:
;; 1. remove from old-params-to-box the parameters that are in the lambda-params.
;;	  (i.e: those parameters will have to be evaluated again as eligible for box-setting, for this scope)
;; 2. go over the lambda-params and evaluate their eligibility for box-setting (using the params-to-box function).
;; 3. append the eligible parameters to the old-params-to-box\lambda-params list, and return it.
(define update-params-to-box
	(lambda (old-params-to-box lambda-params lambda-body)
		(append 
			(filter (lambda (old-param)
						(null? (filter (lambda (lambda-param)
									(eq? old-param lambda-param)) lambda-params))) old-params-to-box)
			(params-to-box lambda-params lambda-body))))

;; This function recieves the params of the current lambda (denoted as "params") 
;; and it's body (denoted as "body").
;; returns a list of the params in "params" which are eligable for boxing.
;; The function uses filter, and since filter's predicat has an arity of 1, 
;; we create a lambda that receives a param and checks if we should box it 
;; in the context of the given body.
(define params-to-box
	(lambda (params body)
		(filter	
			(lambda (param)
				(to-box? param body)) 
			params)))

;; only if all are true:
;; • It has a bound occurrence in the body of the procedure
;; • It is set (via a set!-expression) somewhere in the body of the procedure.
;; • It has a get-occurrence somewhere in the body of the procedure. A get means that it either
;; appears as a (pvar name minor) or (bvar name major minor).
(define to-box?
	(lambda (param body)
		;(display "body: ") (display body) (newline)
		;(display "is-bound-in-lambda-body? ") (display param) (display ": ") (display (is-bound-in-lambda-body? param body)) (newline)
		;(display "has-set-in-lambda-body? ") (display param) (display ": ") (display (has-set-in-lambda-body? param body)) (newline)
		;(display "has-get-in-lambda-body? ") (display param) (display ": ") (display (has-get-in-lambda-body? param body)) (newline) (newline)
		(and 
			(is-bound-in-lambda-body? param body)
			(has-set-in-lambda-body? param body)
			(has-get-in-lambda-body? param body))))

;; This functions skips a level and seeks set or get inside a nested lambda.
;; Here we get a param and the body of the outer lambda.
;; we want to make sure the param is in the body of an <inner> lambda, and not in it's params.
;; Invariant: when we get here, we are in the body of the <outer> lambda.
(define is-bound-in-lambda-body?
	(lambda (param body)
		(cond 
			((not (list? body)) #f)
			((null? body) #f)
			((eq? 'lambda-simple (car body))
				(let ((lambda-params (cadr body))
					  (lambda-body (caddr body)))
					(and
						(null? (filter (lambda (lambda-param) (eq? param lambda-param)) lambda-params)) ;; Not occuring in params.
						(occurs-in-nested-body? param lambda-body))))
			((eq? 'lambda-opt (car body))
				(let ((lambda-params (append (cadr body) (list (caddr body))))
					  (lambda-body (cadddr body)))
					(and
						(null? (filter (lambda (lambda-param) (eq? param lambda-param)) lambda-params))
						(occurs-in-nested-body? param lambda-body))))
			(else 
				(or (is-bound-in-lambda-body? param (car body))
					(is-bound-in-lambda-body? param (cdr body)))))))

;; In this function, we already know we are in a nested lambda,
;; and that our param does not appear as a param to this specific lambda.
;; all left to make sure is that our param either:
;; 1. appears in this lambda's body
;; 2. appears in an inner lambda's body and not in it's params.
(define occurs-in-nested-body?
	(lambda (param body)
		;(display "param: ") (display param) (newline)
		;(display "body: ") (display body) (newline) (newline)
		(cond
			((not (list? body)) #f)
			((null? body) #f)
			((eq? (car body) 'set)
				;(display "cadadr body: ") (display (cadadr body)) (newline) (newline)
				(or
					(eq? (cadadr body) param)
					(occurs-in-nested-body? param (caddr body))))
			((eq? (car body) 'var)
				(eq? (cadr body) param))
			((eq? 'lambda-simple (car body))
				(let ((lambda-params (cadr body))
					  (lambda-body (caddr body)))
					(and
						(null? (filter (lambda (lambda-param) (eq? param lambda-param)) lambda-params)) ;; Not occuring in params.
						(occurs-in-nested-body? param lambda-body))))
			((eq? 'lambda-opt (car body))
				(let ((lambda-params (append (cadr body) (list (caddr body))))
					  (lambda-body (cadddr body)))
					(and
						(null? (filter (lambda (lambda-param) (eq? param lambda-param)) lambda-params))
						(occurs-in-nested-body? param lambda-body))))
			(else 
				(or (occurs-in-nested-body? param (car body))
					(occurs-in-nested-body? param (cdr body)))))))

;; This function seeks a set expression inside the body of the lambda.
;; Note: we dont have to skip a level to get a set expression, which means
;; that we start seeking for a set expression even in the body of the outer lambda,
;; and we dont have to skip to a nested level for a set expression to be valid.
(define has-set-in-lambda-body?
	(lambda (param body)
		(cond
			((not (list? body)) #f)
			((null? body) #f)
			((eq? (car body) 'set)
				(eq? (cadadr body) param))
			((eq? 'lambda-simple (car body))
				(let ((lambda-params (cadr body))
					  (lambda-body (caddr body)))
					(and
						(null? (filter (lambda (lambda-param) (eq? param lambda-param)) lambda-params)) ;; Not occuring in params.
						(has-set-in-lambda-body? param lambda-body))))
			((eq? 'lambda-opt (car body))
				(let ((lambda-params (append (cadr body) (list (caddr body))))
					  (lambda-body (cadddr body)))
					(and
						(null? (filter (lambda (lambda-param) (eq? param lambda-param)) lambda-params))
						(has-set-in-lambda-body? param lambda-body))))			
			(else 
				(or (has-set-in-lambda-body? param (car body))
					(has-set-in-lambda-body? param (cdr body)))))))			
			
;; This function seeks a get expression inside the body of the lambda.
;; Note: we dont have to skip a level to get a get expression, which means
;; that we start seeking for a get expression even in the body of the outer lambda,
;; and we dont have to skip to a nested level for a set expression to be valid.
;; Another note: here if we see a set expression, we skip its second item
;; (the (var <var_name) expression).
(define has-get-in-lambda-body?
	(lambda (param body)
		(cond
			((not (list? body)) #f)
			((null? body) #f)
			((eq? (car body) 'var)
				(eq? (cadr body) param))
			((eq? (car body) 'set)
				(has-get-in-lambda-body? param (caddr body)))
			((eq? 'lambda-simple (car body))
				(let ((lambda-params (cadr body))
					  (lambda-body (caddr body)))
					(and
						(null? (filter (lambda (lambda-param) (eq? param lambda-param)) lambda-params)) ;; Not occuring in params.
						(has-get-in-lambda-body? param lambda-body))))
			((eq? 'lambda-opt (car body))
				(let ((lambda-params (append (cadr body) (list (caddr body))))
					  (lambda-body (cadddr body)))
					(and
						(null? (filter (lambda (lambda-param) (eq? param lambda-param)) lambda-params))
						(has-get-in-lambda-body? param lambda-body))))			
			(else 
				(or (has-get-in-lambda-body? param (car body))
					(has-get-in-lambda-body? param (cdr body)))))))
;;=====================================================================================
;;=====================================================================================
;;=================================== box-set: ========================================
;;=====================================================================================
;;===================================================================================== 

	




;;=====================================================================================
;;=====================================================================================
;;================================== pe->lex-pe: ======================================
;;=====================================================================================
;;=====================================================================================
(define pe->lex-pe
	(lambda (sexpr)
		(traverse sexpr '())))

;; sexpr is an expression that we want to annotate for lexical addresses.
;; addresses is a list of lists (variable and their tagged address)
;; the idea is to always update a list of addresses and to use them to tag the
;; variables as we traverse the expression.
;;
;; Invariant about addresses: there will not be 2 records with the same
;; variable name. Each var name that has a record will has its record updated
;; as we pass through different scopes.
(define traverse
	(lambda (sexpr addresses)
		;(display "traverse addresses: ") (display addresses) (newline) (newline)
		(cond
			((not (list? sexpr)) sexpr)
			((null? sexpr) sexpr)
			((eq? (car sexpr) 'var)
				;(display "var hit on: ") (display sexpr) (newline) (newline)
				(update-var-address (cadr sexpr) addresses))
			((eq? (car sexpr) 'lambda-simple) 
				(append (list 'lambda-simple (cadr sexpr)) (traverse (cddr sexpr) (update-address-list addresses (cadr sexpr)))))
			((eq? (car sexpr) 'lambda-opt)
				(append (list 'lambda-opt (cadr sexpr) (caddr sexpr)) (traverse (cdddr sexpr) (update-address-list addresses (append (cadr sexpr) (list (caddr sexpr)))))))
			(else
				(map (lambda (x) (traverse x addresses)) sexpr)))))

;; we get the name of a var denoted as '(var <name>)
;; (meaning we just get the symbol <name>).
;; we also get the list of lexical addresses for the callee context.
;; if var is in the list, we just get the item itself
;; (it is already in the formm of '(p/bvar name ...)).
;; if var is not in the list, we return a free var form of the var.
(define update-var-address
	(lambda (var addresses)
		;(display "update-var-address var: ") (display var) (newline)
		;(display "update-var-address addresses: ") (display addresses) (newline) (newline)
		(if (null? (filter (lambda (addr) (eq? var (cadr addr))) addresses))
			(list 'fvar var)
			(car (filter (lambda (addr) (eq? var (cadr addr))) addresses)))))

;; every time we get inside a scope of a lambda, we need to update the
;; addresses list to the context of that lambda. meaning that all of the said
;; lambda's params will be added as pvars, and alredy existing vars will be
;; updated accordingly.
;; pseudo code:
;; 1. update level of params and transform pvars to bvars
;; 2. insert the params into the semi-updated addresses
(define update-address-list
	(lambda (addresses params)
		;(display "update-address-list addresses: ") (display addresses) (newline)
		;(display "update-address-list params: ") (display params) (newline) (newline)
		(fold-left
			(lambda (acc curr curr-index)
				;(display "acc: ") (display acc) (newline)
				;(display "curr: ") (display curr) (newline)
				;(display "curr-index: ") (display curr-index) (newline) (newline)
				(if (null? (filter (lambda (acc-addr) (eq? curr (cadr acc-addr))) acc))
					(append acc (list (list 'pvar curr curr-index)))
					(subst (list 'pvar curr curr-index) (car (filter (lambda (acc-addr) (eq? curr (cadr acc-addr))) acc)) acc)))
			(map 
				(lambda (addr)
					(if (eq? (car addr) 'pvar)
						(list 'bvar (cadr addr) 0 (caddr addr))
						(list 'bvar (cadr addr) (+ 1 (caddr addr)) (cadddr addr)))) 
				addresses)
			params
			(enumerate params))))
;;=====================================================================================
;;=====================================================================================
;;================================== pe->lex-pe: ======================================
;;=====================================================================================
;;=====================================================================================






;;=====================================================================================
;;=====================================================================================
;;================================= annotate-tc: ======================================
;;=====================================================================================
;;=====================================================================================

;; Base cases are the empty list and a non-list.
;; Then if it is a list that is lambda expression, tail-positionate its body.
;; Otherwise, its some list. Go over its members with annotate-tc.
(define annotate-tc
	(lambda (sexpr)
		(cond
			((not (list? sexpr)) sexpr)
			((null? sexpr) sexpr)
			((eq? (car sexpr) 'lambda-simple) 
				(append (list 'lambda-simple (cadr sexpr)) (tail-positionate (cddr sexpr))))
			((eq? (car sexpr) 'lambda-opt)
				(append (list 'lambda-opt (cadr sexpr) (caddr sexpr)) (tail-positionate (cdddr sexpr))))
			(else
				(map annotate-tc sexpr)))))


;; Tail-positionate is called when there is a certain need to transform an expression
;; to tail position i.e. when the 'body' is the body of a lambda or when it is the 
;; then or else cases of an if statement.
;; It is first called from the function 'annotate-tc' if it was the body of a lambda,
;; and for that we call the parameter 'body'
(define tail-positionate
	(lambda (body)
		(cond
			((not (list? body)) body)
			((null? body) body)
			((eq? (car body) 'or)
				(list 
					'or 
					(append (map annotate-tc (reverse (cdr (reverse (cadr body)))))
							(list (tail-positionate (car (reverse (cadr body))))))))
			((eq? (car body) 'seq)
				(list 
					'seq 
					(append (map annotate-tc (reverse (cdr (reverse (cadr body)))))
							(list (tail-positionate (car (reverse (cadr body))))))))
			((eq? (car body) 'set)
				(append (list 'set) (annotate-tc (cdr body))))
			((eq? (car body) 'box-set)
				(append (list 'box-set) (annotate-tc (cdr body))))
			((eq? (car body) 'applic)
				(append (list 'tc-applic) (annotate-tc (cdr body))))
			((eq? (car body) 'if3)
				(append (list 'if3 (annotate-tc (cadr body))) (tail-positionate (cddr body))))
			(else
				(map tail-positionate body)))))
;;=====================================================================================
;;=====================================================================================
;;================================= annotate-tc: ======================================
;;=====================================================================================
;;=====================================================================================






