

%:
	echo '(load "project/final_project.scm") (compile-scheme-file "$(MAKECMDGOALS).scm" "$(MAKECMDGOALS).s")' | scheme -q
	nasm -f elf64 $(MAKECMDGOALS).s
	gcc -ggdb -lc -o $(MAKECMDGOALS) $(MAKECMDGOALS).o

clean:
	rm -f gopnik.s gopnik.o gopnik