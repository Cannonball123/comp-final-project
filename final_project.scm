(load "project/sexpr-parser.scm")
(load "project/tag-parser.scm")
(load "project/semantic-analyzer.scm")


;- use JL, JG when comparing signed integers,
;- use JA, JB when comparing unsigned integers,



;; ========================================================================================================================================
;; ========================================================================================================================================
;; ================================================= UTILITY FUNCTIONS AND SET! VARIABLES =================================================
;; ========================================================================================================================================
;; ========================================================================================================================================

;;; Description: These definitions define global variables that will be changed with set! in the process of building the tables.
(define complete-assembly-string "")
(define asts '())
(define astInd 0)

(define const-list (list (void) #f #t '()))
(define const-table '(("T_UNDEFINED" "sobUndef" "undef")))
(define const-table-string "")
(define const-strInd 0)
(define const-symInd 0)
(define const-pairInd 0)
(define const-vecInd 0)

(define global-var-list '())
(define global-var-table '())
(define global-var-table-string "")

(define symbol-table-init-string "")

(define primitives-string "")

(define code-string "")
(define code-ident "")
(define code-constInd 0)
(define code-ifInd 0)
(define code-orInd 0)
(define code-seqInd 0)
(define code-defineInd 0)
(define code-fvarInd 0)
(define code-lambda-simpleInd 0)
(define code-lambda-depth 0)
(define code-lambda-optInd 0)
(define code-applicInd 0)
(define code-pvarInd 0)
(define code-bvarInd 0)



;;; Description: resets the global variables to their initial values.
(define reset-utility-set-vars!
	(lambda ()
		(begin
			(set! complete-assembly-string "")
			(set! asts '())
			(set! astInd 0)

			(set! const-list (list (void) #f #t '()))
			(set! const-table '(("T_UNDEFINED" "sobUndef" "undef")))
			(set! const-table-string "")
			(set! const-strInd 0)
			(set! const-symInd 0)
			(set! const-pairInd 0)
			(set! const-vecInd 0)

			(set! global-var-list '())
			(set! global-var-table '())
			(set! global-var-table-string "")

			(set! symbol-table-init-string "")

			(set! primitives-string "")

			(set! code-string "")
			(set! code-ident "")
			(set! code-constInd 0)
			(set! code-ifInd 0)
			(set! code-orInd 0)
			(set! code-seqInd 0)
			(set! code-defineInd 0)
			(set! code-fvarInd 0)
			(set! code-lambda-simpleInd 0)
			(set! code-lambda-depth 0)
			(set! code-lambda-optInd 0)
			(set! code-applicInd 0)
			(set! code-pvarInd 0)
			(set! code-bvarInd 0))))

;; ========================================================================================================================================
;; ========================================================================================================================================
;; ================================================= UTILITY FUNCTIONS AND SET! VARIABLES =================================================
;; ========================================================================================================================================
;; ========================================================================================================================================





;; ========================================================================================================================================
;; ========================================================================================================================================
;; ========================================================== PIPELINE INTERFACE ==========================================================
;; ========================================================================================================================================
;; ========================================================================================================================================

;;; Usage: (file->list "gopnik.scm")
;;; Description: This function takes a file as described in Usage, and returns a list of chars.
;;; Example: (assuming that gopnok.scm contains: ((lambda (a) (+ 5 a)) 4) )
;;;			> (file->list "gopnik.scm")
;;;			(#\( #\( #\l #\a #\m #\b #\d #\a #\space #\( #\a #\) #\space
;;;			 #\( #\+ #\space #\5 #\space #\a #\) #\) #\space #\4 #\))
;;;			> 
(define file->list
	(lambda (in-file)
		(let ((in-port (open-input-file in-file)))
			(letrec ((run
						(lambda ()
							(let ((ch (read-char in-port)))
								(if (eof-object? ch)
									(begin
										(close-input-port in-port)
										'())
									(cons ch (run)))))))
				(run)))))


;;; Usage: (pipeline (file->list "gopnik.scm"))
;;; Note: preferably use it in composition with file->list
;;; Description: This function take a list of chars, and returns the result of piping these chars
;;;				 through the entire transformations of our assignments.
;;; Example: (assuming that gopnok.scm contains: ((lambda (a) (+ 5 a)) 4) )
;;;			> (pipeline (file->list "gopnik.scm"))
;;;			((applic
;;;			   (lambda-simple
;;;			     (a)
;;;			     (tc-applic (fvar +) ((const 5) (pvar a 0))))
;;;			   ((const 4))))
;;;			>
(define pipeline
	(lambda (s)
		((star <sexpr>) s
		 (lambda (m r)
			(map (lambda (e)
					(annotate-tc
						(pe->lex-pe
							(box-set
								(remove-applic-lambda-nil
									(parse e))))))
				m))
		(lambda (f) 'fail))))

;; ========================================================================================================================================
;; ========================================================================================================================================
;; ========================================================== PIPELINE INTERFACE ==========================================================
;; ========================================================================================================================================
;; ========================================================================================================================================





;; ========================================================================================================================================
;; ========================================================================================================================================
;; ========================================================= CONST TABLE BUILDING =========================================================
;; ========================================================================================================================================
;; ========================================================================================================================================

;;; Usage: (make-const-list (pipeline (file->list <scheme file>)))
;;; Description: This function recives a list of asts and sets const-lst to a constant list for those asts, that does not contain duplicates.
;;; Example: (assuming that the file "gopnik.scm" contains:
;;;					((lambda (a) (+ 5 a)) 4)
;;;					(define adder3
;;;						; this is a comment in the middle of the function definition!
;;;						(lambda (x) (+ 3 x)))
;;;					(adder3 4)
;;;					##6-##(adder3 8)
;;;					(* #;(/ 5 0) 8 9)
;;;					(- 2)
;;;					-2
;;;				)
;;;			> (make-const-list (pipeline (file->list "gopnik.scm")))
;;;			> const-list
;;;			(5 4 3 6 8 9 2 -2)
(define make-const-list
	(lambda (asts)
		(cond
			((not (list? asts)) (set! const-list (append const-list '())))
			((null? asts) (set! const-list (append const-list asts)))
			((and (list? asts) (eq? (car asts) 'const) (not (member (cadr asts) const-list)))
				(set! const-list (append const-list (list (cadr asts)))))
			(else
				(begin
					(make-const-list (car asts))
					(make-const-list (cdr asts)))))))


;;; Usage: (make-const-table <list of constants without duplicates> )
;;; Description: This function takes a constant list without duplicates, and sets const-table to a constant table for that list, 
;;;				 that does not contain duplicates.
;;; Pre-Condition: We assume that the list that we recive is a proper list.
;;; Example: (assuming that the constant list is: '(5 4 #t () "av" (1 2 3)) )
;;;			> (make-const-table '(5 4 #t () "av" (1 2 3)))
;;;			> const-table
;;;			(("T_INTEGER" "sobInt5" 5) ("T_INTEGER" "sobInt4" 4) ("T_BOOL" "sobTrue" #t)
;;;			  ("T_NIL" "sobNil" ()) ("T_STRING" "sobStr0" "av")
;;;			  ("T_INTEGER" "sobInt3" 3)
;;;			  ("T_PAIR" "sobPair0" (3) ("sobInt3" "sobNil"))
;;;			  ("T_INTEGER" "sobInt2" 2)
;;;			  ("T_PAIR" "sobPair1" (2 3) ("sobInt2" "sobPair0"))
;;;			  ("T_INTEGER" "sobInt1" 1)
;;;			  ("T_PAIR" "sobPair2" (1 2 3) ("sobInt1" "sobPair1")))
(define make-const-table
	(lambda (const-lst)
		(cond 
			((null? const-lst) (set! const-table (append const-table '())))
			((null? (car const-lst))
				(if (not (member (car const-lst) (map (lambda (const) (caddr const)) const-table)))
					(begin
						(set! const-table (append const-table (list (list "T_NIL" "sobNil" (car const-lst)))))
						(make-const-table (cdr const-lst)))
					(make-const-table (cdr const-lst))))
			((integer? (car const-lst))
				(if (not (member (car const-lst) (map (lambda (const) (caddr const)) const-table)))
					(begin
						(if (negative? (car const-lst))
							(set! const-table (append const-table (list (list "T_INTEGER" (string-append "sobIntNeg" (number->string (abs (car const-lst)))) (car const-lst)))))
							(set! const-table (append const-table (list (list "T_INTEGER" (string-append "sobInt" (number->string (car const-lst))) (car const-lst))))))
						(make-const-table (cdr const-lst)))
					(make-const-table (cdr const-lst))))
			((number? (car const-lst))
				(if (not (member (car const-lst) (map (lambda (const) (caddr const)) const-table)))
					(begin
						(make-const-table (list (numerator (car const-lst))))
						(make-const-table (list (denominator (car const-lst))))
						(if (negative? (numerator (car const-lst)))
							(set! const-table (append const-table (list (list
																			"T_FRACTION"
																			(string-append "sobFracNeg" (number->string (abs (numerator (car const-lst)))) "_" (number->string (denominator (car const-lst))))
																			(car const-lst)
																			(list
																				(cadr (car (filter (lambda (const) (equal? (numerator (car const-lst)) (caddr const))) const-table)))
																				(cadr (car (filter (lambda (const) (equal? (denominator (car const-lst)) (caddr const))) const-table))))))))
							(set! const-table (append const-table (list (list
																			"T_FRACTION"
																			(string-append "sobFrac" (number->string (numerator (car const-lst))) "_" (number->string (denominator (car const-lst))))
																			(car const-lst)
																			(list
																				(cadr (car (filter (lambda (const) (equal? (numerator (car const-lst)) (caddr const))) const-table)))
																				(cadr (car (filter (lambda (const) (equal? (denominator (car const-lst)) (caddr const))) const-table)))))))))
						(make-const-table (cdr const-lst)))
					(make-const-table (cdr const-lst))))
			((boolean? (car const-lst))
				(if (equal? #f (car const-lst))
					(if (not (member (car const-lst) (map (lambda (const) (caddr const)) const-table)))
						(begin
							(set! const-table (append const-table (list (list "T_BOOL" "sobFalse" (car const-lst)))))
							(make-const-table (cdr const-lst)))
						(make-const-table (cdr const-lst)))
					(if (not (member (car const-lst) (map (lambda (const) (caddr const)) const-table)))
						(begin
							(set! const-table (append const-table (list (list "T_BOOL" "sobTrue" (car const-lst)))))
							(make-const-table (cdr const-lst)))
						(make-const-table (cdr const-lst)))))
			((char? (car const-lst))
				(if (not (member (car const-lst) (map (lambda (const) (caddr const)) const-table)))
					(begin
						(set! const-table (append const-table (list (list "T_CHAR" (string-append "sobChar" (char-to-asm-char-string (car const-lst))) (car const-lst)))))
						(make-const-table (cdr const-lst)))
					(make-const-table (cdr const-lst))))
			((string? (car const-lst))
				(if (not (member (car const-lst) (map (lambda (const) (caddr const)) const-table)))
					(begin
						(set! const-table (append const-table (list (list "T_STRING" (string-append "sobStr" (number->string const-strInd)) (car const-lst)))))
						(set! const-strInd (+ const-strInd 1))
						(make-const-table (cdr const-lst)))
					(make-const-table (cdr const-lst))))
			((symbol? (car const-lst))
				(if (not (member (car const-lst) (map (lambda (const) (caddr const)) const-table)))
					(begin
						(if (not (member (symbol->string (car const-lst)) (map (lambda (const) (caddr const)) const-table)))
							(begin
								(set! const-table (append const-table (list (list "T_STRING" (string-append "sobStr" (number->string const-strInd)) (symbol->string (car const-lst))))))
								(set! const-strInd (+ const-strInd 1))))
						(set! const-table (append const-table (list (list
																		"T_SYMBOL"
																		(string-append "sobSym" (number->string const-symInd))
																		(car const-lst)
																		(cadr (car (filter (lambda (const) (equal? (symbol->string (car const-lst)) (caddr const))) const-table)))))))
						(set! const-symInd (+ const-symInd 1))
						(make-const-table (cdr const-lst)))
					(make-const-table (cdr const-lst))))
			((pair? (car const-lst))
				(if (not (member (car const-lst) (map (lambda (const) (caddr const)) const-table)))
					(begin
						(make-const-table (list (cdr (car const-lst))))
						(make-const-table (list (car (car const-lst))))
						(set! const-table (append const-table (list (list
																		"T_PAIR"
																		(string-append "sobPair" (number->string const-pairInd))
																		(car const-lst)
																		(list
																			(cadr (car (filter (lambda (const) (equal? (car (car const-lst)) (caddr const))) const-table)))
																			(cadr (car (filter (lambda (const) (equal? (cdr (car const-lst)) (caddr const))) const-table))))))))
						(set! const-pairInd (+ const-pairInd 1))
						(make-const-table (cdr const-lst)))
					(make-const-table (cdr const-lst))))
			((vector? (car const-lst))
				(if (not (member (car const-lst) (map (lambda (const) (caddr const)) const-table)))
					(begin
						(make-const-table (vector->list (car const-lst)))
						(set! const-table (append const-table (list (list
																		"T_VECTOR"
																		(string-append "sobVec" (number->string const-vecInd))
																		(car const-lst)
																		(vector->list (vector-map (lambda (vector-item) (cadr (car (filter (lambda (const) (equal? vector-item (caddr const))) const-table)))) (car const-lst)))))))
						(set! const-vecInd (+ const-vecInd 1))
						(make-const-table (cdr const-lst)))
				(make-const-table (cdr const-lst))))
			(else
				(if (not (member (void) (map (lambda (const) (caddr const)) const-table)))
					(begin
						(set! const-table (append const-table (list (list "T_VOID" "sobVoid" (void)))))
						(make-const-table (cdr const-lst)))
					(make-const-table (cdr const-lst)))))))


;;; Example: 
;;;			> (char-to-asm-char-string "\n")
;;;			"_newline")
(define char-to-asm-char-string
	(lambda (char)
		(cond
			((= (char->integer char) 0)
				"_nul")
			((= (char->integer char) 9)
				"_tab")
			((= (char->integer char) 10)
				"_newline")
			((= (char->integer char) 12)
				"_page")
			((= (char->integer char) 13)
				"_return")
			((= (char->integer char) 32)
				"_space")
			((= (char->integer char) 34)
				"_dquote")
			((or (< (char->integer char) 32) (> (char->integer char) 126))
				(string-append "_" (number->string (char->integer char))))
			(else
				(string-append "_" (string char))))))

;; ========================================================================================================================================
;; ========================================================================================================================================
;; ========================================================= CONST TABLE BUILDING =========================================================
;; ========================================================================================================================================
;; ========================================================================================================================================





;; ========================================================================================================================================
;; ========================================================================================================================================
;; ========================================================= CONST TABLE WRITING ==========================================================
;; ========================================================================================================================================
;; ========================================================================================================================================

;;; Usage: (make-const-table-string <constant table> )
;;; Description: This function takes a constant table and generates its representative string.
;;; Example: (assuming that the constant table is:
;;;				(("T_INTEGER" "sobInt5" 5) ("T_INTEGER" "sobInt4" 4) ("T_BOOL" "sobTrue" #t)
;;;			  	 ("T_NIL" "sobNil" ()) ("T_STRING" "sobStr0" "av")
;;;			  	 ("T_INTEGER" "sobInt3" 3)
;;;			  	 ("T_PAIR" "sobPair0" (3) ("sobInt3" "sobNil"))
;;;			  	 ("T_INTEGER" "sobInt2" 2)
;;;			  	 ("T_PAIR" "sobPair1" (2 3) ("sobInt2" "sobPair0"))
;;;			  	 ("T_INTEGER" "sobInt1" 1)
;;;			  	 ("T_PAIR" "sobPair2" (1 2 3) ("sobInt1" "sobPair1")))
;;;			 )
;;;			> (make-const-table '(5 4 #t () "av" (1 2 3)))
;;;			> (make-const-table-string const-table)
;;;			> const-table-string
;;;			"sobInt5:\n\tdq MAKE_LITERAL(T_INTEGER, 5)\nsobInt4:\n\tdq MAKE_LITERAL(T_INTEGER, 4)\nsobTrue:\n\tdq SOB_TRUE\nsobNil:\n\tdq SOB_NIL\nsobStr0:\n\tdq MAKE_LITERAL_STRING \"av\"\nsobInt3:\n\tdq MAKE_LITERAL(T_INTEGER, 3)\nsobPair0:\n\tdq MAKE_LITERAL_PAIR(sobInt3, sobNil)\nsobInt2:\n\tdq MAKE_LITERAL(T_INTEGER, 2)\nsobPair1:\n\tdq MAKE_LITERAL_PAIR(sobInt2, sobPair0)\nsobInt1:\n\tdq MAKE_LITERAL(T_INTEGER, 1)\nsobPair2:\n\tdq MAKE_LITERAL_PAIR(sobInt1, sobPair1)\n"
(define make-const-table-string
	(lambda (const-tab)
		(cond
			((null? const-tab) (set! const-table-string (string-append const-table-string "")))
			((equal? (car (car const-tab)) "T_NIL")
				(begin
					(set! const-table-string (string-append const-table-string (cadr (car const-tab)) ":\n" "\tdq SOB_NIL\n"))
					(make-const-table-string (cdr const-tab))))
			((equal? (car (car const-tab)) "T_INTEGER")
				(begin
					(set! const-table-string (string-append const-table-string (cadr (car const-tab)) ":\n" "\tdq MAKE_LITERAL(T_INTEGER, " (number->string (caddr (car const-tab))) ")\n"))
					(make-const-table-string (cdr const-tab))))
			((equal? (car (car const-tab)) "T_FRACTION")
				(begin
					(set! const-table-string (string-append const-table-string (cadr (car const-tab)) ":\n" "\tdq MAKE_LITERAL_FRACTION(" (car (cadddr (car const-tab))) ", " (cadr (cadddr (car const-tab))) ")\n"))
					(make-const-table-string (cdr const-tab))))
			((and (equal? (car (car const-tab)) "T_BOOL") (equal? (caddr (car const-tab)) #t))
				(begin
					(set! const-table-string (string-append const-table-string (cadr (car const-tab)) ":\n" "\tdq SOB_TRUE\n"))
					(make-const-table-string (cdr const-tab))))
			((and (equal? (car (car const-tab)) "T_BOOL") (equal? (caddr (car const-tab)) #f))
				(begin
					(set! const-table-string (string-append const-table-string (cadr (car const-tab)) ":\n" "\tdq SOB_FALSE\n"))
					(make-const-table-string (cdr const-tab))))
			((equal? (car (car const-tab)) "T_CHAR")
				(begin
					(set! const-table-string (string-append const-table-string (cadr (car const-tab)) ":\n" "\tdq MAKE_LITERAL(T_CHAR, " (number->string (char->integer (caddr (car const-tab)))) ")\n"))
					(make-const-table-string (cdr const-tab))))
			((equal? (car (car const-tab)) "T_STRING")
				(begin
					(set! const-table-string (string-append const-table-string (cadr (car const-tab)) ":\n" "\tMAKE_LITERAL_STRING " (str-to-asm-str (caddr (car const-tab))) "\n"))
					(make-const-table-string (cdr const-tab))))
			((equal? (car (car const-tab)) "T_SYMBOL")
				(begin
					(set! const-table-string (string-append const-table-string (cadr (car const-tab)) ":\n" "\tdq MAKE_LITERAL_SYMBOL(" (cadddr (car const-tab)) ")\n"))
					(make-const-table-string (cdr const-tab))))
			((equal? (car (car const-tab)) "T_PAIR")
				(begin
					(set! const-table-string (string-append const-table-string (cadr (car const-tab)) ":\n" "\tdq MAKE_LITERAL_PAIR(" (car (cadddr (car const-tab))) ", " (cadr (cadddr (car const-tab))) ")\n"))
					(make-const-table-string (cdr const-tab))))
			((equal? (car (car const-tab)) "T_VECTOR")
				(if (null? (cadddr (car const-tab)))
					(begin
						(set! const-table-string (string-append const-table-string (cadr (car const-tab)) ":\n" "\tdq 11\n"))
						(make-const-table-string (cdr const-tab)))
					(begin
						(set! const-table-string (string-append const-table-string (cadr (car const-tab)) ":\n" "\tMAKE_LITERAL_VECTOR " (car (cadddr (car const-tab))) (fold-left (lambda (acc vec-label) (string-append acc ", " vec-label)) "" (cdr (cadddr (car const-tab)))) "\n"))
						(make-const-table-string (cdr const-tab)))))
			((equal? (car (car const-tab)) "T_VOID")
				(begin
					(set! const-table-string (string-append const-table-string (cadr (car const-tab)) ":\n" "\tdq SOB_VOID\n"))
					(make-const-table-string (cdr const-tab))))
			(else ;; Its T_UNDEFINED
				(begin
					(set! const-table-string (string-append const-table-string (cadr (car const-tab)) ":\n" "\tdq SOB_UNDEFINED\n"))
					(make-const-table-string (cdr const-tab)))))))


;;; Example: 
;;;			> (str-to-asm-str "avi \tnatan")
;;;			"\"avi\", CHAR_SPACE, CHAR_TAB, \"natan\""
(define str-to-asm-str
	(lambda (str)
		(fold-left
			(lambda (str-acc char-num)
				(cond
					((= char-num 0)
						(if (null? (string->list str-acc))
							(string-append str-acc "CHAR_NUL")
							(string-append str-acc ", CHAR_NUL")))
					((= char-num 9)
						(if (null? (string->list str-acc))
							(string-append str-acc "CHAR_TAB")
							(string-append str-acc ", CHAR_TAB")))
					((= char-num 10)
						(if (null? (string->list str-acc))
							(string-append str-acc "CHAR_NEWLINE")
							(string-append str-acc ", CHAR_NEWLINE")))
					((= char-num 12)
						(if (null? (string->list str-acc))
							(string-append str-acc "CHAR_PAGE")
							(string-append str-acc ", CHAR_PAGE")))
					((= char-num 13)
						(if (null? (string->list str-acc))
							(string-append str-acc "CHAR_RETURN")
							(string-append str-acc ", CHAR_RETURN")))
					((= char-num 32)
						(if (null? (string->list str-acc))
							(string-append str-acc "CHAR_SPACE")
							(string-append str-acc ", CHAR_SPACE")))
					((= char-num 34)
						(if (null? (string->list str-acc))
							(string-append str-acc "CHAR_DQUOTE")
							(string-append str-acc ", CHAR_DQUOTE")))
					(else
						(cond
							((null? (string->list str-acc))
								(string-append str-acc "\"" (string (integer->char char-num)) "\""))
							((= (char->integer (car (reverse (string->list str-acc)))) 34)
								(string-append (list->string (reverse (cdr (reverse (string->list str-acc))))) (string (integer->char char-num)) "\""))
							(else
								(string-append str-acc ", \"" (string (integer->char char-num)) "\""))))))
			"\"\""
			(map char->integer (string->list str)))))

;; ========================================================================================================================================
;; ========================================================================================================================================
;; ========================================================= CONST TABLE WRITING ==========================================================
;; ========================================================================================================================================
;; ========================================================================================================================================





;; ========================================================================================================================================
;; ========================================================================================================================================
;; ==================================================== GLOBAL VARIABLE TABLE BUILDING ====================================================
;; ========================================================================================================================================
;; ========================================================================================================================================

;;; Usage: (make-global-var-list (pipeline (file->list <scheme file>)))
;;; Description: This function creates a global var list for a list of asts, that does not contain duplicates.
;;; Example: (assuming that the file "gopnik.scm" contains:
;;;					((lambda (a) (+ 5 a)) 4)
;;;					(define adder3
;;;						; this is a comment in the middle of the function definition!
;;;						(lambda (x) (+ 3 x)))
;;;					(adder3 4)
;;;					##6-8
;;;					##6-##(adder3 8)
;;;					(* #;(/ 5 0) 8 9)
;;;					(- 2)
;;;					-2
;;;				)
;;;			> (compile-scheme-file "gopnik.scm" "gopnik.s")
;;;			> global-var-list
;;;			(+ adder3 - *)
(define make-global-var-list
	(lambda (asts)
		(cond
			((not (list? asts)) (set! global-var-list (append global-var-list '())))
			((null? asts) (set! global-var-list (append global-var-list asts)))
			((and (list? asts) (eq? (car asts) 'fvar) (not (member (cadr asts) global-var-list)))
				(set! global-var-list (append global-var-list (list (cadr asts)))))
			(else
				(begin
					(make-global-var-list (car asts))
					(make-global-var-list (cdr asts)))))))

;;; Usage: (make-global-var-table <list of global variables without duplicates> )
;;; Description: This function takes a list of global variables without duplicates, and sets global-var-table to a global variable table
;;;				 for that list, that does not contain duplicates.
;;; Pre-Condition: We assume that the list that we recive is a proper list.
;;; Example: (assuming that the constant list is: '(append apply < =) )
;;;			> (make-global-var-table '(append apply < =))
;;;			(("append" append) ("apply" apply) ("_LT_" <) ("_EQ_" =))
(define make-global-var-table
	(lambda (global-var-lst)
		(cond
			((null? global-var-lst)
				(set! global-var-table (append global-var-table '())))
			(else
				(begin
					(set! global-var-table (append global-var-table (list (list (var-to-asm-var (car global-var-lst)) (car global-var-lst)))))
					(make-global-var-table (cdr global-var-lst)))))))

;;; Example:
;;;			> (var-to-asm-var '*pack)
;;;			"_MU_pack"
(define var-to-asm-var
	(lambda (var)
		(fold-left
			(lambda (str-acc char-num)
				(cond
					((= char-num 33)
						(string-append str-acc "_BG_"))	; Bang, !
					((= char-num 42)
						(string-append str-acc "_MU_"))	; Multiplication, *
					((= char-num 43)
						(string-append str-acc "_PL_"))	; Plus, +
					((= char-num 45)
						(string-append str-acc "_MN_"))	; Minus, -
					((= char-num 47)
						(string-append str-acc "_DV_"))	; Division, /
					((= char-num 60)
						(string-append str-acc "_LT_"))	; Less-than, <
					((= char-num 61)
						(string-append str-acc "_EQ_"))	; Equal, =
					((= char-num 62)
						(string-append str-acc "_GT_"))	; Greater-than, >
					(else
						(string-append str-acc (string (integer->char char-num))))))
			""
			(map char->integer (string->list (symbol->string var))))))

;; ========================================================================================================================================
;; ========================================================================================================================================
;; ==================================================== GLOBAL VARIABLE TABLE BUILDING ====================================================
;; ========================================================================================================================================
;; ========================================================================================================================================





;; ========================================================================================================================================
;; ========================================================================================================================================
;; ==================================================== GLOBAL VARIABLE TABLE WRITING =====================================================
;; ========================================================================================================================================
;; ========================================================================================================================================

;;; Usage: (make-global-var-table-string <global var table without duplicates> )
;;; Description: This function takes a global var table without duplicates, and generates a string that represents the global variables.
;;; Example: (assuming that the file gopnik.scm contains:
;;;				append
;;;				apply
;;;				<
;;;				*pack
;;;			 )
;;;			> (compile-scheme-file "gopnik.scm" "gopnik.s")
;;;			> global-var-table-string
;;;			"append:\n\tdq SOB_UNDEFINED\napply:\n\tdq SOB_UNDEFINED\narithLessThan:\n\tdq SOB_UNDEFINED\n_MU_pack:\n\tdq SOB_UNDEFINED\n"
(define make-global-var-table-string
	(lambda (global-var-tab)
		(cond
			((null? global-var-tab) (set! global-var-table-string (string-append global-var-table-string "")))
			(else
				(begin
					(set! global-var-table-string (string-append global-var-table-string (car (car global-var-tab)) ":\n\tdq SOB_UNDEFINED\n"))
					(make-global-var-table-string (cdr global-var-tab)))))))

;; ========================================================================================================================================
;; ========================================================================================================================================
;; ==================================================== GLOBAL VARIABLE TABLE WRITING =====================================================
;; ========================================================================================================================================
;; ========================================================================================================================================





;; ========================================================================================================================================
;; ========================================================================================================================================
;; ====================================================== SYMBOL TABLE INITIALIZATION =====================================================
;; ========================================================================================================================================
;; ========================================================================================================================================

;;; Description: This function goes over the constant table and for every symbol, generates assembly code that creates a bucket in the
;;;				 symbol table that references to the appropriate constant symbol.
(define make-symbol-table-init-string
	(lambda (const-tab)
		(for-each
			(lambda (const-rec)
				(cond
					((equal? (car const-rec) "T_SYMBOL")
						(begin
							(set! symbol-table-init-string (string-append
																symbol-table-init-string
																"\n\t;================= " (cadr const-rec) " ================="
																"\n\tmov\t\tr15, symbol_table ; r15 holds pointer to the first node in the symbol table - could be a non empty pair, or the empty list\n\n"


																"\n\t." (cadr const-rec) "_symbol_table_traverse_loop:"
																"\n\tmov\t\tr13, qword [r15]"
																"\n\tcmp\t\tr13, SOB_NIL"
																"\n\tje\t\t." (cadr const-rec) "_symbol_table_traverse_loop_end\n"

																"\n\tDATA_LOWER r13"
																"\n\tadd\t\tr13, start_of_data"
																"\n\tmov\t\tr15, r13"
																"\n\tjmp\t\t." (cadr const-rec) "_symbol_table_traverse_loop\n"

																"\n\t." (cadr const-rec) "_symbol_table_traverse_loop_end:\n\n"


																"\n\tmov\t\trdi, 8"
																"\n\tcall\tmalloc"
																"\n\tmov\t\tqword [rax], SOB_NIL"
																"\n\tmov\t\tr13, " (cadr const-rec)
																"\n\tsub\t\tr13, start_of_data"
																"\n\tshl\t\tr13, 30"
																"\n\tsub\t\trax, start_of_data"
																"\n\tor\t\tr13, rax"
																"\n\tshl\t\tr13, TYPE_BITS"
																"\n\tor\t\tr13, T_PAIR"
																"\n\tmov\t\tqword [r15], r13"
																"\n\t;================= " (cadr const-rec) " =================\n"))))
					(else
						(begin
							(set! symbol-table-init-string (string-append symbol-table-init-string ""))))))
			const-tab)))

;; ========================================================================================================================================
;; ========================================================================================================================================
;; ====================================================== SYMBOL TABLE INITIALIZATION =====================================================
;; ========================================================================================================================================
;; ========================================================================================================================================





;; ========================================================================================================================================
;; ========================================================================================================================================
;; ====================================================== PRIMITIVE FUNCTIONS WRITING =====================================================
;; ========================================================================================================================================
;; ========================================================================================================================================

(define make-primitive-functions-string
	(lambda (global-var-tab)
		(for-each
			(lambda (fvar-rec)
				(cond
					;((eq? (cadr fvar-rec) 'append)
					;	(begin
					;		(set! primitives-string (string-append
					;									primitives-string
					;									"\n\t;================= append ================="
					;									"\n\tmov\t\trbx, 0"
					;									"\n\tmov\t\trdi, 16"
					;									"\n\tcall\tmalloc"
					;									"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
					;									"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


					;									"\n\t" (car fvar-rec) "_body:"
					;									"\n\tpush\trbp"
					;									"\n\tmov\t\trbp, rsp\n\n"


					;									; actual body
					;									; actual body


					;									"\n\t." (car fvar-rec) "_body_end:"
					;									"\n\tmov\t\trsp, rbp"
					;									"\n\tpop\t\trbp"
					;									"\n\tret\n\n"


					;									"\n\t" (car fvar-rec) "_handle_end:"
					;									"\n\tmov\t\trax, [rax]"
					;									"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
					;									"\n\t;================= append ================\n"))))
					((eq? (cadr fvar-rec) 'apply)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;================= apply ================="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 2"
														"\n\tjl\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														"\n\t; see if last argument is a proper list, and calculate its length"
														"\n\tmov\t\tr13, qword [rbp + 5*8]"
														"\n\tlea\t\tr13, [rbp + 5*8 + r13*8]"
														"\n\tmov\t\tr13, [r13]"
														"\n\tmov\t\tr11, 0"


														"\n\t." (car fvar-rec) "_last_arg_list_check_loop:"
														"\n\tmov\t\tr12, r13"
														"\n\tmov\t\tr12, [r12]"
														"\n\tTYPE\tr12"
														"\n\tcmp\t\tr12, T_NIL"
														"\n\tje\t\t." (car fvar-rec) "_last_arg_list_check_loop_end\n"

														"\n\tmov\t\tr12, r13"
														"\n\tmov\t\tr12, [r12]"
														"\n\tTYPE\tr12"
														"\n\tcmp\t\tr12, T_PAIR"
														"\n\tjne\t\t." (car fvar-rec) "_not_a_proper_list\n"

														"\n\tmov\t\tr13, [r13]"
														"\n\tDATA_LOWER r13"
														"\n\tadd\t\tr13, start_of_data"
														"\n\tinc\t\tr11"
														"\n\tjmp\t\t." (car fvar-rec) "_last_arg_list_check_loop\n"

														"\n\t." (car fvar-rec) "_last_arg_list_check_loop_end:\n\n"


														"\n\t; see if first argument is a procedure"
														"\n\tmov\t\tr13, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [r13]"
														"\n\tTYPE\tr13"
														"\n\tcmp\t\tr13, T_CLOSURE"
														"\n\tjne\t\t." (car fvar-rec) "_not_a_procedure\n\n"


														"\n\tmov\t\tr15, qword [rbp + 6*8]"
														"\n\tmov\t\tr15, [r15] ; r15 holds the procedure object"
														"\n\tmov\t\tr14, qword [rbp + 5*8] ; r14 holds the old argument number ( argc(4) )"
														"\n\tlea\t\tr13, [rbp + 7*8] ; r13 holds the address of the first argument after the procedure on the stack"
														"\n\tmov\t\tr12, qword [rbp + 5*8]"
														"\n\tlea\t\tr12, [rbp + 5*8 + r12*8] ; r12 holds the address of the list on the stack"
														"\n\tlea\t\tr8, [rbp + 2*8]"
														"\n\tmov\t\tr9, r11"
														"\n\tshl\t\tr9, 3"
														"\n\tsub\t\tr8, r9 ; r8 holds the address of where to we should copy the old rbp"
														"\n\tmov\t\tr10, r8"
														"\n\t; copy old rbp, ret, AST rbp, AST end label"
														"\n\t; save ret, AST rbp, and AST end label into registers"
														"\n\tmov\t\trax, qword [rbp + 1*8] ; save ret"
														"\n\tmov\t\trdx, qword [rbp + 2*8] ; save AST rbp"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; save AST end label"
														"\n\tmov\t\tr9, qword [rbp] ; copy old rbp"
														"\n\tmov\t\tqword [r8], r9"
														"\n\tadd\t\tr8, 8"
														"\n\tmov\t\tqword [r8], rax ; copy ret"
														"\n\tadd\t\tr8, 8"
														"\n\tmov\t\tqword [r8], rdx ; copy AST rbp"
														"\n\tadd\t\tr8, 8"
														"\n\tmov\t\tqword [r8], rcx ; copy AST end label"
														"\n\tadd\t\tr8, 8"
														"\n\t; insert the procedure environment"
														"\n\tmov\t\tr9, r15"
														"\n\tCLOSURE_ENV r9"
														"\n\tmov\t\tqword [r8], r9 ; inserting environment"
														"\n\tadd\t\tr8, 8"
														"\n\tmov\t\tr9, r14"
														"\n\tadd\t\tr9, r11"
														"\n\tsub\t\tr9, 2"
														"\n\tmov\t\tqword [r8], r9 ; inserting new arg count ( argc(6) )"
														"\n\tadd\t\tr8, 8\n\n"


														"\n\t; copy the non - listed arguments"
														"\n\t." (car fvar-rec) "_copy_non_list_args_loop:"
														"\n\tcmp\t\tr13, r12"
														"\n\tje\t\t." (car fvar-rec) "_copy_non_list_args_loop_end\n"

														"\n\tmov\t\tr9, qword [r13]"
														"\n\tmov\t\tqword [r8], r9"
														"\n\tadd\t\tr8, 8"
														"\n\tadd\t\tr13, 8"
														"\n\tjmp\t\t." (car fvar-rec) "_copy_non_list_args_loop\n"

														"\n\t." (car fvar-rec) "_copy_non_list_args_loop_end:\n\n"


														"\n\t; copy the arguments from the list"
														"\n\tmov\t\tr13, [r13] ; r13 now holds the list object\n"

														"\n\t." (car fvar-rec) "_copy_listed_args_loop:"
														"\n\tmov\t\tr9, r13"
														"\n\tmov\t\tr9, [r9]"
														"\n\tTYPE\tr9"
														"\n\tcmp\t\tr9, T_NIL"
														"\n\tje\t\t." (car fvar-rec) "_copy_listed_args_loop_end\n"

														"\n\tmov\t\tr9, r13"
														"\n\tmov\t\tr9, [r9]"
														"\n\tDATA_UPPER r9"
														"\n\tadd\t\tr9, start_of_data"
														"\n\tmov\t\tqword [r8], r9"
														"\n\tadd\t\tr8, 8"
														"\n\tmov\t\tr9, r13"
														"\n\tmov\t\tr9, [r9]"
														"\n\tDATA_LOWER r9"
														"\n\tadd\t\tr9, start_of_data"
														"\n\tmov\t\tr13, r9"
														"\n\tjmp\t\t." (car fvar-rec) "_copy_listed_args_loop\n"

														"\n\t." (car fvar-rec) "_copy_listed_args_loop_end:\n\n"


														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_not_a_procedure:"
														"\n\tmov\t\tr13, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [r13]"
														"\n\tpush\tr13"
														"\n\tcall\tattempt_to_apply_non_procedure"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label (TOP of the ast)"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_not_a_proper_list:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_proper_list_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\tr13, qword [rbp + 5*8]"
														"\n\tmov\t\tr13, qword [rbp + 5*8 + r13*8]"
														"\n\tmov\t\tr13, [r13]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_proper_list_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_proper_list_string1:"
														"\n\t\tdb \"Exception in apply: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_proper_list_string2:"
														"\n\t\tdb \" is not a proper list\", 10, 0\n\n"
														; actual body


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, r10"
														"\n\tpop\t\trbp"
														"\n\tmov\t\tr9, r15"
														"\n\tCLOSURE_CODE r9"
														"\n\tjmp\t\tr9"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;================= apply =================\n"))))
					((eq? (cadr fvar-rec) '<)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;=================== < ==================="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\t; check that there is at least 1 argument"
														"\n\tcmp\t\tqword [rbp + 5*8], 0"
														"\n\tje\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														"\n\t; check that all of the arguments are integers or fractions"
														"\n\tmov\t\tr9, 0 ; r9 is a counter for the number of arguments\n"
														
														; checking the types of the arguments
														"\n\t." (car fvar-rec) "_arg_type_check_loop:"
														"\n\tcmp\t\tr9, qword [rbp + 5*8]"
														"\n\tje\t\t." (car fvar-rec) "_lesser\n"

														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tTYPE\t\tr8"
														"\n\tcmp\t\tr8, T_INTEGER"
														"\n\tje\t\t." (car fvar-rec) "_inc_counter\n"

														"\n\tcmp\t\tr8, T_FRACTION"
														"\n\tje\t\t." (car fvar-rec) "_inc_counter\n"

														"\n\tjmp\t\t." (car fvar-rec) "_not_a_number\n"

														"\n\t." (car fvar-rec) "_inc_counter:\n"
														"\n\tinc\t\tr9"
														"\n\tjmp\t\t." (car fvar-rec) "_arg_type_check_loop\n\n"


														; actual body
														"\n\t." (car fvar-rec) "_lesser:"
														"\n\tmov\t\tr9, 1 ; r9 is a counter for the number of arguments\n"

														"\n\t; initialize r15/r14 as a fraction representing the first argument"
														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tTYPE\tr8"
														"\n\tcmp\t\tr8, T_INTEGER"
														"\n\tje\t\t." (car fvar-rec) "_initialize_integer\n"

														"\n\t; initialize fraction"
														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_UPPER r8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr15, r8"
														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_LOWER r8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr14, r8"
														"\n\tjmp\t\t." (car fvar-rec) "_lesser_loop\n"

														"\n\t." (car fvar-rec) "_initialize_integer:"
														"\n\t; initialize integer"
														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr15, r8"
														"\n\tmov\t\tr14, 1\n\n"


														"\n\t." (car fvar-rec) "_lesser_loop:"
														"\n\t; loop over the arguments and check if they keep the lesser total order"
														"\n\tcmp\t\tr9, qword [rbp + 5*8]"
														"\n\tje\t\t." (car fvar-rec) "_lesser_order_is_true\n"

														"\n\t; get the next arg into r13/r12"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tTYPE\tr8"
														"\n\tcmp\t\tr8, T_INTEGER"
														"\n\tje\t\t." (car fvar-rec) "_next_arg_is_integer\n"

														"\n\t; next arg is fraction"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_UPPER r8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr13, r8"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_LOWER r8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr12, r8"
														"\n\tjmp\t\t." (car fvar-rec) "_fraction_extensions\n"

														"\n\t." (car fvar-rec) "_next_arg_is_integer:"
														"\n\t; next arg is integer"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr13, r8"
														"\n\tmov\t\tr12, 1\n"

														"\n\t." (car fvar-rec) "_fraction_extensions:"
														"\n\tmov\t\tr11, r14 ; saving r14"
														"\n\tmov\t\tr10, r12 ; saving r12"
														"\n\t; expanding the acc"
														"\n\timul\t\tr15, r10"
														"\n\timul\t\tr14, r10"
														"\n\t; expanding the curr"
														"\n\timul\t\tr13, r11"
														"\n\timul\t\tr12, r11"

														"\n\t; comparing the numerators"
														"\n\tcmp\t\tr15, r13"
														"\n\tjl\t\t." (car fvar-rec) "_lesser_order_kept"
														"\n\tjmp\t\t." (car fvar-rec) "_lesser_order_is_false\n"

														"\n\t." (car fvar-rec) "_lesser_order_kept:"
														"\n\t; in this scenario, copy curr to acc, and do a fraction simplification on acc. then increase counter"
														"\n\tmov\t\tr15, r13"
														"\n\tmov\t\tr14, r12\n"

														"\n\t." (car fvar-rec) "_gcd_for_acc:"
														"\n\tmov\t\trdx, 0"
														"\n\tmov\t\trax, r15 ; first - acc numerator"
														"\n\tmov\t\trbx, r14 ; second - acc denominator"
														"\n\tcmp\t\trax, 0"
														"\n\tjge\t\t." (car fvar-rec) "_numerator_no_neg"
														"\n\tneg\t\trax"
														"\n\t." (car fvar-rec) "_numerator_no_neg:"
														"\n\tcmp\t\trbx, 0"
														"\n\tjge\t\t." (car fvar-rec) "_denominator_no_neg"
														"\n\tneg\t\trbx"
														"\n\t." (car fvar-rec) "_denominator_no_neg:"
														"\n\tcmp\t\trax, rbx"
														"\n\tjge\t\t." (car fvar-rec) "_gcd_loop\n"

														"\n\txchg\t\trax, rbx\n"
														
														"\n\t." (car fvar-rec) "_gcd_loop:"
														"\n\tcmp\t\trbx, 0"
														"\n\tje\t\t." (car fvar-rec) "_gcd_loop_end"
														"\n\tmov\t\trdx, 0"
														"\n\tdiv\t\trbx"
														"\n\tmov\t\trax, rbx"
														"\n\tmov\t\trbx, rdx"
														"\n\tjmp\t\t." (car fvar-rec) "_gcd_loop"
														"\n\t." (car fvar-rec) "_gcd_loop_end:\n"

														"\n\t; at this point rax holds the gcd, divide r15 and r14 by the gcd"
														"\n\tmov\t\tr10, rax ; save gcd"
														"\n\tmov\t\trdx, 0"
														"\n\tmov\t\trax, r15"
														"\n\tidiv\tr10"
														"\n\tmov\t\tr15, rax"
														"\n\tmov\t\trdx, 0"
														"\n\tmov\t\trax, r14"
														"\n\tidiv\tr10"
														"\n\tmov\t\tr14, rax"

														"\n\tinc\t\tr9"
														"\n\tjmp\t\t." (car fvar-rec) "_lesser_loop\n\n"


														"\n\t." (car fvar-rec) "_lesser_order_is_true:"
														"\n\tmov\t\trax, sobTrue"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_lesser_order_is_false:"
														"\n\tmov\t\trax, sobFalse"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"
														; actual body


														"\n\t." (car fvar-rec) "_not_a_number:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_number_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tpush\tr8"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_number_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_number_string1:"
														"\n\t\tdb \"Exception in <: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_number_string2:"
														"\n\t\tdb \" is not a number\", 10, 0\n\n"


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;=================== < ===================\n"))))
					((eq? (cadr fvar-rec) '=)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;=================== = ==================="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\t; check that there is at least 1 argument"
														"\n\tcmp\t\tqword [rbp + 5*8], 0"
														"\n\tje\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														"\n\t; check that all of the arguments are integers or fractions"
														"\n\tmov\t\tr9, 0 ; r9 is a counter for the number of arguments\n"
														
														; checking the types of the arguments
														"\n\t." (car fvar-rec) "_arg_type_check_loop:"
														"\n\tcmp\t\tr9, qword [rbp + 5*8]"
														"\n\tje\t\t." (car fvar-rec) "_equal\n"

														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tTYPE\t\tr8"
														"\n\tcmp\t\tr8, T_INTEGER"
														"\n\tje\t\t." (car fvar-rec) "_inc_counter\n"

														"\n\tcmp\t\tr8, T_FRACTION"
														"\n\tje\t\t." (car fvar-rec) "_inc_counter\n"

														"\n\tjmp\t\t." (car fvar-rec) "_not_a_number\n"

														"\n\t." (car fvar-rec) "_inc_counter:\n"
														"\n\tinc\t\tr9"
														"\n\tjmp\t\t." (car fvar-rec) "_arg_type_check_loop\n\n"


														; actual body
														"\n\t." (car fvar-rec) "_equal:"
														"\n\tmov\t\tr9, 1 ; r9 is a counter for the number of arguments\n"

														"\n\t; initialize r15/r14 as a fraction representing the first argument"
														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tTYPE\tr8"
														"\n\tcmp\t\tr8, T_INTEGER"
														"\n\tje\t\t." (car fvar-rec) "_initialize_integer\n"

														"\n\t; initialize fraction"
														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_UPPER r8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr15, r8"
														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_LOWER r8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr14, r8"
														"\n\tjmp\t\t." (car fvar-rec) "_equal_loop\n"

														"\n\t." (car fvar-rec) "_initialize_integer:"
														"\n\t; initialize integer"
														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr15, r8"
														"\n\tmov\t\tr14, 1\n\n"


														"\n\t." (car fvar-rec) "_equal_loop:"
														"\n\t; loop over the arguments and check if they keep the equal total order"
														"\n\tcmp\t\tr9, qword [rbp + 5*8]"
														"\n\tje\t\t." (car fvar-rec) "_equal_order_is_true\n"

														"\n\t; get the next arg into r13/r12"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tTYPE\tr8"
														"\n\tcmp\t\tr8, T_INTEGER"
														"\n\tje\t\t." (car fvar-rec) "_next_arg_is_integer\n"

														"\n\t; next arg is fraction"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_UPPER r8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr13, r8"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_LOWER r8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr12, r8"
														"\n\tjmp\t\t." (car fvar-rec) "_fraction_extensions\n"

														"\n\t." (car fvar-rec) "_next_arg_is_integer:"
														"\n\t; next arg is integer"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr13, r8"
														"\n\tmov\t\tr12, 1\n"

														"\n\t." (car fvar-rec) "_fraction_extensions:"
														"\n\tmov\t\tr11, r14 ; saving r14"
														"\n\tmov\t\tr10, r12 ; saving r12"
														"\n\t; expanding the acc"
														"\n\timul\t\tr15, r10"
														"\n\timul\t\tr14, r10"
														"\n\t; expanding the curr"
														"\n\timul\t\tr13, r11"
														"\n\timul\t\tr12, r11"

														"\n\t; comparing the numerators"
														"\n\tcmp\t\tr15, r13"
														"\n\tje\t\t." (car fvar-rec) "_equal_order_kept"
														"\n\tjmp\t\t." (car fvar-rec) "_equal_order_is_false\n"

														"\n\t." (car fvar-rec) "_equal_order_kept:"
														"\n\t; in this scenario, copy curr to acc, and do a fraction simplification on acc. then increase counter"
														"\n\tmov\t\tr15, r13"
														"\n\tmov\t\tr14, r12\n"

														"\n\t." (car fvar-rec) "_gcd_for_acc:"
														"\n\tmov\t\trdx, 0"
														"\n\tmov\t\trax, r15 ; first - acc numerator"
														"\n\tmov\t\trbx, r14 ; second - acc denominator"
														"\n\tcmp\t\trax, 0"
														"\n\tjge\t\t." (car fvar-rec) "_numerator_no_neg"
														"\n\tneg\t\trax"
														"\n\t." (car fvar-rec) "_numerator_no_neg:"
														"\n\tcmp\t\trbx, 0"
														"\n\tjge\t\t." (car fvar-rec) "_denominator_no_neg"
														"\n\tneg\t\trbx"
														"\n\t." (car fvar-rec) "_denominator_no_neg:"
														"\n\tcmp\t\trax, rbx"
														"\n\tjge\t\t." (car fvar-rec) "_gcd_loop\n"

														"\n\txchg\t\trax, rbx\n"
														
														"\n\t." (car fvar-rec) "_gcd_loop:"
														"\n\tcmp\t\trbx, 0"
														"\n\tje\t\t." (car fvar-rec) "_gcd_loop_end"
														"\n\tmov\t\trdx, 0"
														"\n\tdiv\t\trbx"
														"\n\tmov\t\trax, rbx"
														"\n\tmov\t\trbx, rdx"
														"\n\tjmp\t\t." (car fvar-rec) "_gcd_loop"
														"\n\t." (car fvar-rec) "_gcd_loop_end:\n"

														"\n\t; at this point rax holds the gcd, divide r15 and r14 by the gcd"
														"\n\tmov\t\tr10, rax ; save gcd"
														"\n\tmov\t\trdx, 0"
														"\n\tmov\t\trax, r15"
														"\n\tidiv\tr10"
														"\n\tmov\t\tr15, rax"
														"\n\tmov\t\trdx, 0"
														"\n\tmov\t\trax, r14"
														"\n\tidiv\tr10"
														"\n\tmov\t\tr14, rax"

														"\n\tinc\t\tr9"
														"\n\tjmp\t\t." (car fvar-rec) "_equal_loop\n\n"


														"\n\t." (car fvar-rec) "_equal_order_is_true:"
														"\n\tmov\t\trax, sobTrue"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_equal_order_is_false:"
														"\n\tmov\t\trax, sobFalse"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"
														; actual body


														"\n\t." (car fvar-rec) "_not_a_number:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_number_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tpush\tr8"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_number_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_number_string1:"
														"\n\t\tdb \"Exception in =: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_number_string2:"
														"\n\t\tdb \" is not a number\", 10, 0\n\n"


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;=================== = ===================\n"))))
					((eq? (cadr fvar-rec) '>)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;=================== > ==================="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\t; check that there is at least 1 argument"
														"\n\tcmp\t\tqword [rbp + 5*8], 0"
														"\n\tje\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														"\n\t; check that all of the arguments are integers or fractions"
														"\n\tmov\t\tr9, 0 ; r9 is a counter for the number of arguments\n"
														
														; checking the types of the arguments
														"\n\t." (car fvar-rec) "_arg_type_check_loop:"
														"\n\tcmp\t\tr9, qword [rbp + 5*8]"
														"\n\tje\t\t." (car fvar-rec) "_greater\n"

														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tTYPE\t\tr8"
														"\n\tcmp\t\tr8, T_INTEGER"
														"\n\tje\t\t." (car fvar-rec) "_inc_counter\n"

														"\n\tcmp\t\tr8, T_FRACTION"
														"\n\tje\t\t." (car fvar-rec) "_inc_counter\n"

														"\n\tjmp\t\t." (car fvar-rec) "_not_a_number\n"

														"\n\t." (car fvar-rec) "_inc_counter:\n"
														"\n\tinc\t\tr9"
														"\n\tjmp\t\t." (car fvar-rec) "_arg_type_check_loop\n\n"


														; actual body
														"\n\t." (car fvar-rec) "_greater:"
														"\n\tmov\t\tr9, 1 ; r9 is a counter for the number of arguments\n"

														"\n\t; initialize r15/r14 as a fraction representing the first argument"
														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tTYPE\tr8"
														"\n\tcmp\t\tr8, T_INTEGER"
														"\n\tje\t\t." (car fvar-rec) "_initialize_integer\n"

														"\n\t; initialize fraction"
														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_UPPER r8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr15, r8"
														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_LOWER r8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr14, r8"
														"\n\tjmp\t\t." (car fvar-rec) "_greater_loop\n"

														"\n\t." (car fvar-rec) "_initialize_integer:"
														"\n\t; initialize integer"
														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr15, r8"
														"\n\tmov\t\tr14, 1\n\n"


														"\n\t." (car fvar-rec) "_greater_loop:"
														"\n\t; loop over the arguments and check if they keep the greater total order"
														"\n\tcmp\t\tr9, qword [rbp + 5*8]"
														"\n\tje\t\t." (car fvar-rec) "_greater_order_is_true\n"

														"\n\t; get the next arg into r13/r12"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tTYPE\tr8"
														"\n\tcmp\t\tr8, T_INTEGER"
														"\n\tje\t\t." (car fvar-rec) "_next_arg_is_integer\n"

														"\n\t; next arg is fraction"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_UPPER r8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr13, r8"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_LOWER r8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr12, r8"
														"\n\tjmp\t\t." (car fvar-rec) "_fraction_extensions\n"

														"\n\t." (car fvar-rec) "_next_arg_is_integer:"
														"\n\t; next arg is integer"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr13, r8"
														"\n\tmov\t\tr12, 1\n"

														"\n\t." (car fvar-rec) "_fraction_extensions:"
														"\n\tmov\t\tr11, r14 ; saving r14"
														"\n\tmov\t\tr10, r12 ; saving r12"
														"\n\t; expanding the acc"
														"\n\timul\t\tr15, r10"
														"\n\timul\t\tr14, r10"
														"\n\t; expanding the curr"
														"\n\timul\t\tr13, r11"
														"\n\timul\t\tr12, r11"

														"\n\t; comparing the numerators"
														"\n\tcmp\t\tr15, r13"
														"\n\tjg\t\t." (car fvar-rec) "_greater_order_kept"
														"\n\tjmp\t\t." (car fvar-rec) "_greater_order_is_false\n"

														"\n\t." (car fvar-rec) "_greater_order_kept:"
														"\n\t; in this scenario, copy curr to acc, and do a fraction simplification on acc. then increase counter"
														"\n\tmov\t\tr15, r13"
														"\n\tmov\t\tr14, r12\n"

														"\n\t." (car fvar-rec) "_gcd_for_acc:"
														"\n\tmov\t\trdx, 0"
														"\n\tmov\t\trax, r15 ; first - acc numerator"
														"\n\tmov\t\trbx, r14 ; second - acc denominator"
														"\n\tcmp\t\trax, 0"
														"\n\tjge\t\t." (car fvar-rec) "_numerator_no_neg"
														"\n\tneg\t\trax"
														"\n\t." (car fvar-rec) "_numerator_no_neg:"
														"\n\tcmp\t\trbx, 0"
														"\n\tjge\t\t." (car fvar-rec) "_denominator_no_neg"
														"\n\tneg\t\trbx"
														"\n\t." (car fvar-rec) "_denominator_no_neg:"
														"\n\tcmp\t\trax, rbx"
														"\n\tjge\t\t." (car fvar-rec) "_gcd_loop\n"

														"\n\txchg\t\trax, rbx\n"
														
														"\n\t." (car fvar-rec) "_gcd_loop:"
														"\n\tcmp\t\trbx, 0"
														"\n\tje\t\t." (car fvar-rec) "_gcd_loop_end"
														"\n\tmov\t\trdx, 0"
														"\n\tdiv\t\trbx"
														"\n\tmov\t\trax, rbx"
														"\n\tmov\t\trbx, rdx"
														"\n\tjmp\t\t." (car fvar-rec) "_gcd_loop"
														"\n\t." (car fvar-rec) "_gcd_loop_end:\n"

														"\n\t; at this point rax holds the gcd, divide r15 and r14 by the gcd"
														"\n\tmov\t\tr10, rax ; save gcd"
														"\n\tmov\t\trdx, 0"
														"\n\tmov\t\trax, r15"
														"\n\tidiv\tr10"
														"\n\tmov\t\tr15, rax"
														"\n\tmov\t\trdx, 0"
														"\n\tmov\t\trax, r14"
														"\n\tidiv\tr10"
														"\n\tmov\t\tr14, rax"

														"\n\tinc\t\tr9"
														"\n\tjmp\t\t." (car fvar-rec) "_greater_loop\n\n"


														"\n\t." (car fvar-rec) "_greater_order_is_true:"
														"\n\tmov\t\trax, sobTrue"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_greater_order_is_false:"
														"\n\tmov\t\trax, sobFalse"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"
														; actual body


														"\n\t." (car fvar-rec) "_not_a_number:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_number_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tpush\tr8"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_number_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_number_string1:"
														"\n\t\tdb \"Exception in >: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_number_string2:"
														"\n\t\tdb \" is not a number\", 10, 0\n\n"


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;=================== > ===================\n"))))
					((eq? (cadr fvar-rec) '+)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;=================== + ==================="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"

														
														; actual body
														"\n\tcmp\t\tqword [rbp + 5*8], 0"
														"\n\tjne\t\t." (car fvar-rec) "_not_zero_args"
														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr8, 0"
														"\n\tshl\t\tr8, 4"
														"\n\tor\t\tr8, T_INTEGER"
														"\n\tmov\t\t[rax], r8"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_not_zero_args:"
														"\n\tmov\t\tr9, 0 ; r9 is a counter for the number of arguments\n\n"

														
														; checking the types of the arguments
														"\n\t." (car fvar-rec) "_arg_type_check_loop:"
														"\n\tcmp\t\tr9, qword [rbp + 5*8]"
														"\n\tje\t\t." (car fvar-rec) "_addition\n"

														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tTYPE\t\tr8"
														"\n\tcmp\t\tr8, T_INTEGER"
														"\n\tje\t\t." (car fvar-rec) "_inc_counter\n"

														"\n\tcmp\t\tr8, T_FRACTION"
														"\n\tje\t\t." (car fvar-rec) "_inc_counter\n"

														"\n\tjmp\t\t." (car fvar-rec) "_not_a_number\n"

														"\n\t." (car fvar-rec) "_inc_counter:\n"
														"\n\tinc\t\tr9"
														"\n\tjmp\t\t." (car fvar-rec) "_arg_type_check_loop\n\n"


														; doing addition
														"\n\t." (car fvar-rec) "_addition:"
														"\n\tmov\t\tr9, 0 ; r9 is a counter for the number of arguments"
														"\n\tmov\t\tr15, 0 ; r15 is the acc numerator"
														"\n\tmov\t\tr14, 1 ; r14 is the acc denominator"
														"\n\tmov\t\tr13, 0 ; r13 is the curr numerator"
														"\n\tmov\t\tr12, 1 ; r12 is the curr denominator\n\n"


														"\n\t." (car fvar-rec) "_addition_loop:"
														"\n\tcmp\t\tr9, qword [rbp + 5*8]"
														"\n\tje\t\t." (car fvar-rec) "_addition_post_processing\n"

														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tTYPE\tr8"
														"\n\tcmp\t\tr8, T_INTEGER"
														"\n\tje\t\t." (car fvar-rec) "_integer_decompose_to_fraction\n"

														"\n\t; setting the curr numerator"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_UPPER r8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr13, r8"
														"\n\t; setting the curr denominator"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_LOWER r8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr12, r8"
														"\n\tjmp\t\t." (car fvar-rec) "_fraction_extensions\n"

														"\n\t." (car fvar-rec) "_integer_decompose_to_fraction:"
														"\n\t; setting the curr numerator"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr13, r8"
														"\n\t; setting the curr denominator"
														"\n\tmov\t\tr12, 1\n"

														"\n\t." (car fvar-rec) "_fraction_extensions:"
														"\n\tmov\t\tr11, r14 ; saving r14"
														"\n\tmov\t\tr10, r12 ; saving r12"
														"\n\t; applying fraction extension on acc"
														"\n\tmov\t\trax, r14"
														"\n\tmul\t\tr10"
														"\n\tmov\t\tr14, rax"
														"\n\tmov\t\tr8, 0"
														"\n\tcmp\t\tr15, 0"
														"\n\tjge\t\t." (car fvar-rec) "_acc_no_neg1"
														"\n\tneg\t\tr15"
														"\n\tmov\t\tr8, 1"
														"\n\t." (car fvar-rec) "_acc_no_neg1:"
														"\n\tmov\t\trax, r15"
														"\n\tmul\t\tr10"
														"\n\tmov\t\tr15, rax"
														"\n\tcmp\t\tr8, 1"
														"\n\tjne\t\t." (car fvar-rec) "_acc_no_neg2"
														"\n\tneg\t\tr15"
														"\n\t." (car fvar-rec) "_acc_no_neg2:"
														"\n\t; applying fraction extension on curr"
														"\n\tmov\t\trax, r12"
														"\n\tmul\t\tr11"
														"\n\tmov\t\tr12, rax"
														"\n\tmov\t\tr8, 0"
														"\n\tcmp\t\tr13, 0"
														"\n\tjge\t\t." (car fvar-rec) "_curr_no_neg1"
														"\n\tneg\t\tr13"
														"\n\tmov\t\tr8, 1"
														"\n\t." (car fvar-rec) "_curr_no_neg1:"
														"\n\tmov\t\trax, r13"
														"\n\tmul\t\tr11"
														"\n\tmov\t\tr13, rax"
														"\n\tcmp\t\tr8, 1"
														"\n\tjne\t\t." (car fvar-rec) "_curr_no_neg2"
														"\n\tneg\t\tr13"
														"\n\t." (car fvar-rec) "_curr_no_neg2:\n"

														"\n\t; adding numerators"
														"\n\tadd\t\tr15, r13\n"

														"\n\t." (car fvar-rec) "_gcd_for_acc:"
														"\n\tmov\t\trdx, 0"
														"\n\tmov\t\trax, r15 ; first - acc numerator"
														"\n\tmov\t\trbx, r14 ; second - acc denominator"
														"\n\tcmp\t\trax, 0"
														"\n\tjge\t\t." (car fvar-rec) "_numerator_no_neg"
														"\n\tneg\t\trax"
														"\n\t." (car fvar-rec) "_numerator_no_neg:"
														"\n\tcmp\t\trbx, 0"
														"\n\tjge\t\t." (car fvar-rec) "_denominator_no_neg"
														"\n\tneg\t\trbx"
														"\n\t." (car fvar-rec) "_denominator_no_neg:"
														"\n\tcmp\t\trax, rbx"
														"\n\tjge\t\t." (car fvar-rec) "_gcd_loop\n"

														"\n\txchg\t\trax, rbx\n"
														
														"\n\t." (car fvar-rec) "_gcd_loop:"
														"\n\tcmp\t\trbx, 0"
														"\n\tje\t\t." (car fvar-rec) "_gcd_loop_end"
														"\n\tmov\t\trdx, 0"
														"\n\tdiv\t\trbx"
														"\n\tmov\t\trax, rbx"
														"\n\tmov\t\trbx, rdx"
														"\n\tjmp\t\t." (car fvar-rec) "_gcd_loop"
														"\n\t." (car fvar-rec) "_gcd_loop_end:\n"

														"\n\t; rax should contain the gcd by now"
														"\n\tmov\t\tr10, rax ; saving the gcd in r10"
														"\n\tcmp\t\tr10, 1"
														"\n\tje\t\t." (car fvar-rec) "_no_fraction_simplification\n"

														"\n\t; in case the gcd is not 1, perform fraction simplification"
														"\n\t; simplify the numerator"
														"\n\tmov\t\trdx, 0"
														"\n\tmov\t\trax, r15"
														"\n\tdiv\t\tr10"
														"\n\tmov\t\tr15, rax"
														"\n\t; simplify the denominator"
														"\n\tmov\t\trdx, 0"
														"\n\tmov\t\trax, r14"
														"\n\tdiv\t\tr10"
														"\n\tmov\t\tr14, rax\n"														

														"\n\t." (car fvar-rec) "_no_fraction_simplification:"
														"\n\tinc\t\tr9"
														"\n\tjmp\t\t." (car fvar-rec) "_addition_loop\n\n"


														"\n\t." (car fvar-rec) "_addition_post_processing:"
														"\n\tmov\t\tr11, r15"
														"\n\tmov\t\tr10, r14"
														"\n\tcmp\t\tr11, 0"
														"\n\tjge\t\t." (car fvar-rec) "_numerator_no_neg2"
														"\n\tneg\t\tr11"
														"\n\t." (car fvar-rec) "_numerator_no_neg2:"
														"\n\tcmp\t\tr10, 0"
														"\n\tjge\t\t." (car fvar-rec) "_denominator_no_neg2"
														"\n\tneg\t\tr10"
														"\n\t." (car fvar-rec) "_denominator_no_neg2:"
														"\n\tcmp\t\tr11, r10"
														"\n\tjl\t\t." (car fvar-rec) "_generate_fraction_result\n"

														"\n\tmov\t\trdx, 0"
														"\n\tmov\t\trax, r11"
														"\n\tdiv\t\tr10"
														"\n\tcmp\t\trdx, 0"
														"\n\tjne\t\t." (car fvar-rec) "_generate_fraction_result\n\n"


														"\n\t; generate integer result"
														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr8, r15"
														"\n\tshl\t\tr8, 4"
														"\n\tor\t\tr8, T_INTEGER"
														"\n\tmov\t\t[rax], r8"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t; generate fraction result"
														"\n\t." (car fvar-rec) "_generate_fraction_result:"
														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr8, r15"
														"\n\tshl\t\tr8, 4"
														"\n\tor\t\tr8, T_INTEGER"
														"\n\tmov\t\t[rax], r8"
														"\n\tmov\t\tr15, rax"
														"\n\tsub\t\tr15, start_of_data\n"

														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr8, r14"
														"\n\tshl\t\tr8, 4"
														"\n\tor\t\tr8, T_INTEGER"
														"\n\tmov\t\t[rax], r8"
														"\n\tmov\t\tr14, rax"
														"\n\tsub\t\tr14, start_of_data\n"

														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr8, r15"
														"\n\tshl\t\tr8, 30"
														"\n\tor\t\tr8, r14"
														"\n\tshl\t\tr8, 4"
														"\n\tor\t\tr8, T_FRACTION"
														"\n\tmov\t\t[rax], r8"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_not_a_number:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_number_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tpush\tr8"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_number_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_number_string1:"
														"\n\t\tdb \"Exception in +: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_number_string2:"
														"\n\t\tdb \" is not a number\", 10, 0\n\n"
														; actual body


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;=================== + ===================\n"))))
					((eq? (cadr fvar-rec) '/)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;=================== / ==================="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"

														
														"\n\tcmp\t\tqword [rbp + 5*8], 0"
														"\n\tje\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														"\n\tmov\t\tr9, 0 ; r9 is a counter for the number of arguments\n\n"

														
														; checking the types of the arguments
														"\n\t." (car fvar-rec) "_arg_type_check_loop:"
														"\n\tcmp\t\tr9, qword [rbp + 5*8]"
														"\n\tje\t\t." (car fvar-rec) "_division\n"

														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tTYPE\t\tr8"
														"\n\tcmp\t\tr8, T_INTEGER"
														"\n\tje\t\t." (car fvar-rec) "_check_if_zero"
														"\n\tjmp\t\t." (car fvar-rec) "_check_if_fraction\n"

														"\n\t." (car fvar-rec) "_check_if_zero:"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tcmp\t\tr8, 0"
														"\n\tje\t\t." (car fvar-rec) "_undefined_for_zero"
														"\n\tjmp\t\t." (car fvar-rec) "_inc_counter\n"

														"\n\t." (car fvar-rec) "_check_if_fraction:"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tTYPE\t\tr8"
														"\n\tcmp\t\tr8, T_FRACTION"
														"\n\tje\t\t." (car fvar-rec) "_inc_counter\n"

														"\n\tjmp\t\t." (car fvar-rec) "_not_a_number\n"

														"\n\t." (car fvar-rec) "_inc_counter:\n"
														"\n\tinc\t\tr9"
														"\n\tjmp\t\t." (car fvar-rec) "_arg_type_check_loop\n\n"


														; doing division
														"\n\t." (car fvar-rec) "_division:"
														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tjg\t\t." (car fvar-rec) "_At_least_two_args"
														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tTYPE\tr8"
														"\n\tcmp\t\tr8, T_FRACTION"
														"\n\tje\t\t." (car fvar-rec) "_invert_one_fraction_arg"
														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr10, 0"
														"\n\tcmp\tr8, 0"
														"\n\tjge\t\t." (car fvar-rec) "_numerator_no_neg5"
														"\n\tneg\t\tr8"
														"\n\tmov\t\tr10, 1"
														"\n\t." (car fvar-rec) "_numerator_no_neg5:"
														"\n\tmov\t\tr14, r8"
														"\n\tmov\t\tr8, 1"
														"\n\tcmp\t\tr10, 1"
														"\n\tjne\t\t." (car fvar-rec) "_denominator_no_neg5"
														"\n\tneg\t\tr8"
														"\n\t." (car fvar-rec) "_denominator_no_neg5:"
														"\n\tmov\t\tr15, r8"
														"\n\tjmp\t\t." (car fvar-rec) "_division_post_processing"
														"\n\t." (car fvar-rec) "_invert_one_fraction_arg:"
														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_UPPER\tr8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr10, 0"
														"\n\tcmp\tr8, 0"
														"\n\tjge\t\t." (car fvar-rec) "_numerator_no_neg6"
														"\n\tneg\t\tr8"
														"\n\tmov\t\tr10, 1"
														"\n\t." (car fvar-rec) "_numerator_no_neg6:"
														"\n\tmov\t\tr14, r8"
														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_LOWER\tr8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tcmp\t\tr10, 1"
														"\n\tjne\t\t." (car fvar-rec) "_denominator_no_neg6"
														"\n\tneg\t\tr8"
														"\n\t." (car fvar-rec) "_denominator_no_neg6:"
														"\n\tmov\t\tr15, r8"
														"\n\tjmp\t\t." (car fvar-rec) "_division_post_processing\n\n"


														"\n\t." (car fvar-rec) "_At_least_two_args:"
														"\n\tmov\t\tr9, 1 ; r9 is a counter for the number of arguments"


														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tTYPE\tr8"
														"\n\tcmp\t\tr8, T_INTEGER"
														"\n\tje\t\t." (car fvar-rec) "_acc_init_for_integer\n"

														"\n\t; setting the acc numerator"
														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_UPPER r8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr15, r8"
														"\n\t; setting the acc denominator"
														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_LOWER r8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr14, r8"
														"\n\tjmp\t\t." (car fvar-rec) "_division_loop\n"

														"\n\t." (car fvar-rec) "_acc_init_for_integer:"
														"\n\t; setting the curr numerator"
														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr15, r8"
														"\n\t; setting the curr denominator"
														"\n\tmov\t\tr14, 1\n\n"


														"\n\t." (car fvar-rec) "_division_loop:"
														"\n\tcmp\t\tr9, qword [rbp + 5*8]"
														"\n\tje\t\t." (car fvar-rec) "_division_post_processing\n"

														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tTYPE\tr8"
														"\n\tcmp\t\tr8, T_INTEGER"
														"\n\tje\t\t." (car fvar-rec) "_integer_decompose_to_fraction\n"

														"\n\t; setting the curr numerator"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_UPPER r8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr10, 0"
														"\n\tcmp\tr8, 0"
														"\n\tjge\t\t." (car fvar-rec) "_numerator_no_neg3"
														"\n\tneg\t\tr8"
														"\n\tmov\t\tr10, 1"
														"\n\t." (car fvar-rec) "_numerator_no_neg3:"
														"\n\tmov\t\tr12, r8"
														"\n\t; setting the curr denominator"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_LOWER r8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tcmp\tr10, 1"
														"\n\tjne\t\t." (car fvar-rec) "_denominator_no_neg3"
														"\n\tneg\t\tr8"
														"\n\t." (car fvar-rec) "_denominator_no_neg3:"
														"\n\tmov\t\tr13, r8"
														"\n\tjmp\t\t." (car fvar-rec) "_acc_and_curr_division\n"

														"\n\t." (car fvar-rec) "_integer_decompose_to_fraction:"
														"\n\t; setting the curr numerator"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr10, 0"
														"\n\tcmp\tr8, 0"
														"\n\tjge\t\t." (car fvar-rec) "_numerator_no_neg4"
														"\n\tneg\t\tr8"
														"\n\tmov\t\tr10, 1"
														"\n\t." (car fvar-rec) "_numerator_no_neg4:"
														"\n\tmov\t\tr12, r8"
														"\n\t; setting the curr denominator"
														"\n\tmov\t\tr8, 1\n"
														"\n\tcmp\tr10, 1"
														"\n\tjne\t\t." (car fvar-rec) "_denominator_no_neg4"
														"\n\tneg\t\tr8"
														"\n\t." (car fvar-rec) "_denominator_no_neg4:"
														"\n\tmov\t\tr13, r8\n"

														"\n\t." (car fvar-rec) "_acc_and_curr_division:"
														"\n\timul\t\tr15, r13"
														"\n\timul\t\tr14, r12"														

														"\n\t." (car fvar-rec) "_gcd_for_acc:"
														"\n\tmov\t\trdx, 0"
														"\n\tmov\t\trax, r15 ; first - acc numerator"
														"\n\tmov\t\trbx, r14 ; second - acc denominator"
														"\n\tcmp\t\trax, 0"
														"\n\tjge\t\t." (car fvar-rec) "_numerator_no_neg"
														"\n\tneg\t\trax"
														"\n\t." (car fvar-rec) "_numerator_no_neg:"
														"\n\tcmp\t\trbx, 0"
														"\n\tjge\t\t." (car fvar-rec) "_denominator_no_neg"
														"\n\tneg\t\trbx"
														"\n\t." (car fvar-rec) "_denominator_no_neg:"
														"\n\tcmp\t\trax, rbx"
														"\n\tjge\t\t." (car fvar-rec) "_gcd_loop\n"

														"\n\txchg\t\trax, rbx\n"
														
														"\n\t." (car fvar-rec) "_gcd_loop:"
														"\n\tcmp\t\trbx, 0"
														"\n\tje\t\t." (car fvar-rec) "_gcd_loop_end"
														"\n\tmov\t\trdx, 0"
														"\n\tdiv\t\trbx"
														"\n\tmov\t\trax, rbx"
														"\n\tmov\t\trbx, rdx"
														"\n\tjmp\t\t." (car fvar-rec) "_gcd_loop"
														"\n\t." (car fvar-rec) "_gcd_loop_end:\n"

														"\n\t; rax should contain the gcd by now"
														"\n\tmov\t\tr10, rax ; saving the gcd in r10"
														"\n\tcmp\t\tr10, 1"
														"\n\tje\t\t." (car fvar-rec) "_no_fraction_simplification\n"

														"\n\t; in case the gcd is not 1, perform fraction simplification"
														"\n\t; simplify the numerator"
														"\n\tmov\t\trdx, 0"
														"\n\tmov\t\trax, r15"
														"\n\tdiv\t\tr10"
														"\n\tmov\t\tr15, rax"
														"\n\t; simplify the denominator"
														"\n\tmov\t\trdx, 0"
														"\n\tmov\t\trax, r14"
														"\n\tdiv\t\tr10"
														"\n\tmov\t\tr14, rax\n"														

														"\n\t." (car fvar-rec) "_no_fraction_simplification:"
														"\n\tinc\t\tr9"
														"\n\tjmp\t\t." (car fvar-rec) "_division_loop\n\n"


														"\n\t." (car fvar-rec) "_division_post_processing:"
														"\n\tmov\t\tr11, r15"
														"\n\tmov\t\tr10, r14"
														"\n\tcmp\t\tr11, 0"
														"\n\tjge\t\t." (car fvar-rec) "_numerator_no_neg2"
														"\n\tneg\t\tr11"
														"\n\t." (car fvar-rec) "_numerator_no_neg2:"
														"\n\tcmp\t\tr10, 0"
														"\n\tjge\t\t." (car fvar-rec) "_denominator_no_neg2"
														"\n\tneg\t\tr10"
														"\n\t." (car fvar-rec) "_denominator_no_neg2:"
														"\n\tcmp\t\tr11, r10"
														"\n\tjl\t\t." (car fvar-rec) "_generate_fraction_result\n"

														"\n\tmov\t\trdx, 0"
														"\n\tmov\t\trax, r11"
														"\n\tdiv\t\tr10"
														"\n\tcmp\t\trdx, 0"
														"\n\tjne\t\t." (car fvar-rec) "_generate_fraction_result\n\n"


														"\n\t; generate integer result"
														"\n\t." (car fvar-rec) "_generate_integer_result:"
														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr8, r15"
														"\n\tshl\t\tr8, 4"
														"\n\tor\t\tr8, T_INTEGER"
														"\n\tmov\t\t[rax], r8"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t; generate fraction result"
														"\n\t." (car fvar-rec) "_generate_fraction_result:"
														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr8, r15"
														"\n\tshl\t\tr8, 4"
														"\n\tor\t\tr8, T_INTEGER"
														"\n\tmov\t\t[rax], r8"
														"\n\tmov\t\tr15, rax"
														"\n\tsub\t\tr15, start_of_data\n"

														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr8, r14"
														"\n\tshl\t\tr8, 4"
														"\n\tor\t\tr8, T_INTEGER"
														"\n\tmov\t\t[rax], r8"
														"\n\tmov\t\tr14, rax"
														"\n\tsub\t\tr14, start_of_data\n"

														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr8, r15"
														"\n\tshl\t\tr8, 30"
														"\n\tor\t\tr8, r14"
														"\n\tshl\t\tr8, 4"
														"\n\tor\t\tr8, T_FRACTION"
														"\n\tmov\t\t[rax], r8"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_undefined_for_zero:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_undefined_for_zero_string"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; current ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_undefined_for_zero_string:"
														"\n\t\tdb \"Exception in /: undefined for 0\", 10, 0\n\n"


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_not_a_number:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_number_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tpush\tr8"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_number_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_number_string1:"
														"\n\t\tdb \"Exception in /: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_number_string2:"
														"\n\t\tdb \" is not a number\", 10, 0\n\n"
														; actual body


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;=================== / ===================\n"))))
					((eq? (cadr fvar-rec) '*)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;=================== * ==================="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"

														
														; actual body
														"\n\tcmp\t\tqword [rbp + 5*8], 0"
														"\n\tjne\t\t." (car fvar-rec) "_not_zero_args"
														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr8, 1"
														"\n\tshl\t\tr8, 4"
														"\n\tor\t\tr8, T_INTEGER"
														"\n\tmov\t\t[rax], r8"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_not_zero_args:"
														"\n\tmov\t\tr9, 0 ; r9 is a counter for the number of arguments\n\n"

														
														; checking the types of the arguments
														"\n\t." (car fvar-rec) "_arg_type_check_loop:"
														"\n\tcmp\t\tr9, qword [rbp + 5*8]"
														"\n\tje\t\t." (car fvar-rec) "_multiplication\n"

														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tTYPE\t\tr8"
														"\n\tcmp\t\tr8, T_INTEGER"
														"\n\tje\t\t." (car fvar-rec) "_check_if_zero"
														"\n\tjmp\t\t." (car fvar-rec) "_check_if_fraction\n"

														"\n\t." (car fvar-rec) "_check_if_zero:"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tcmp\t\tr8, 0"
														"\n\tjne\t\t." (car fvar-rec) "_inc_counter\n"

														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr8, 0"
														"\n\tshl\t\tr8, 4"
														"\n\tor\t\tr8, T_INTEGER"
														"\n\tmov\t\t[rax], r8"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n"

														"\n\t." (car fvar-rec) "_check_if_fraction:"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tTYPE\t\tr8"
														"\n\tcmp\t\tr8, T_FRACTION"
														"\n\tje\t\t." (car fvar-rec) "_inc_counter\n"

														"\n\tjmp\t\t." (car fvar-rec) "_not_a_number\n"

														"\n\t." (car fvar-rec) "_inc_counter:\n"
														"\n\tinc\t\tr9"
														"\n\tjmp\t\t." (car fvar-rec) "_arg_type_check_loop\n\n"


														; doing multiplication
														"\n\t." (car fvar-rec) "_multiplication:"
														"\n\tmov\t\tr9, 0 ; r9 is a counter for the number of arguments"
														"\n\tmov\t\tr15, 1 ; r15 is the acc numerator"
														"\n\tmov\t\tr14, 1 ; r14 is the acc denominator"
														"\n\tmov\t\tr13, 1 ; r13 is the curr numerator"
														"\n\tmov\t\tr12, 1 ; r12 is the curr denominator\n\n"


														"\n\t." (car fvar-rec) "_multiplication_loop:"
														"\n\tcmp\t\tr9, qword [rbp + 5*8]"
														"\n\tje\t\t." (car fvar-rec) "_multiplication_post_processing\n"

														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tTYPE\tr8"
														"\n\tcmp\t\tr8, T_INTEGER"
														"\n\tje\t\t." (car fvar-rec) "_integer_decompose_to_fraction\n"

														"\n\t; setting the curr numerator"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_UPPER r8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr13, r8"
														"\n\t; setting the curr denominator"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_LOWER r8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr12, r8"
														"\n\tjmp\t\t." (car fvar-rec) "_acc_and_curr_multiplication\n"

														"\n\t." (car fvar-rec) "_integer_decompose_to_fraction:"
														"\n\t; setting the curr numerator"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr13, r8"
														"\n\t; setting the curr denominator"
														"\n\tmov\t\tr12, 1\n"

														"\n\t." (car fvar-rec) "_acc_and_curr_multiplication:"
														"\n\timul\t\tr15, r13"
														"\n\timul\t\tr14, r12"														

														"\n\t." (car fvar-rec) "_gcd_for_acc:"
														"\n\tmov\t\trdx, 0"
														"\n\tmov\t\trax, r15 ; first - acc numerator"
														"\n\tmov\t\trbx, r14 ; second - acc denominator"
														"\n\tcmp\t\trax, 0"
														"\n\tjge\t\t." (car fvar-rec) "_numerator_no_neg"
														"\n\tneg\t\trax"
														"\n\t." (car fvar-rec) "_numerator_no_neg:"
														"\n\tcmp\t\trbx, 0"
														"\n\tjge\t\t." (car fvar-rec) "_denominator_no_neg"
														"\n\tneg\t\trbx"
														"\n\t." (car fvar-rec) "_denominator_no_neg:"
														"\n\tcmp\t\trax, rbx"
														"\n\tjge\t\t." (car fvar-rec) "_gcd_loop\n"

														"\n\txchg\t\trax, rbx\n"
														
														"\n\t." (car fvar-rec) "_gcd_loop:"
														"\n\tcmp\t\trbx, 0"
														"\n\tje\t\t." (car fvar-rec) "_gcd_loop_end"
														"\n\tmov\t\trdx, 0"
														"\n\tdiv\t\trbx"
														"\n\tmov\t\trax, rbx"
														"\n\tmov\t\trbx, rdx"
														"\n\tjmp\t\t." (car fvar-rec) "_gcd_loop"
														"\n\t." (car fvar-rec) "_gcd_loop_end:\n"

														"\n\t; rax should contain the gcd by now"
														"\n\tmov\t\tr10, rax ; saving the gcd in r10"
														"\n\tcmp\t\tr10, 1"
														"\n\tje\t\t." (car fvar-rec) "_no_fraction_simplification\n"

														"\n\t; in case the gcd is not 1, perform fraction simplification"
														"\n\t; simplify the numerator"
														"\n\tmov\t\trdx, 0"
														"\n\tmov\t\trax, r15"
														"\n\tdiv\t\tr10"
														"\n\tmov\t\tr15, rax"
														"\n\t; simplify the denominator"
														"\n\tmov\t\trdx, 0"
														"\n\tmov\t\trax, r14"
														"\n\tdiv\t\tr10"
														"\n\tmov\t\tr14, rax\n"														

														"\n\t." (car fvar-rec) "_no_fraction_simplification:"
														"\n\tinc\t\tr9"
														"\n\tjmp\t\t." (car fvar-rec) "_multiplication_loop\n\n"


														"\n\t." (car fvar-rec) "_multiplication_post_processing:"
														"\n\tmov\t\tr11, r15"
														"\n\tmov\t\tr10, r14"
														"\n\tcmp\t\tr11, 0"
														"\n\tjge\t\t." (car fvar-rec) "_numerator_no_neg2"
														"\n\tneg\t\tr11"
														"\n\t." (car fvar-rec) "_numerator_no_neg2:"
														"\n\tcmp\t\tr10, 0"
														"\n\tjge\t\t." (car fvar-rec) "_denominator_no_neg2"
														"\n\tneg\t\tr10"
														"\n\t." (car fvar-rec) "_denominator_no_neg2:"
														"\n\tcmp\t\tr11, r10"
														"\n\tjl\t\t." (car fvar-rec) "_generate_fraction_result\n"

														"\n\tmov\t\trdx, 0"
														"\n\tmov\t\trax, r11"
														"\n\tdiv\t\tr10"
														"\n\tcmp\t\trdx, 0"
														"\n\tjne\t\t." (car fvar-rec) "_generate_fraction_result\n\n"


														"\n\t; generate integer result"
														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr8, r15"
														"\n\tshl\t\tr8, 4"
														"\n\tor\t\tr8, T_INTEGER"
														"\n\tmov\t\t[rax], r8"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t; generate fraction result"
														"\n\t." (car fvar-rec) "_generate_fraction_result:"
														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr8, r15"
														"\n\tshl\t\tr8, 4"
														"\n\tor\t\tr8, T_INTEGER"
														"\n\tmov\t\t[rax], r8"
														"\n\tmov\t\tr15, rax"
														"\n\tsub\t\tr15, start_of_data\n"

														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr8, r14"
														"\n\tshl\t\tr8, 4"
														"\n\tor\t\tr8, T_INTEGER"
														"\n\tmov\t\t[rax], r8"
														"\n\tmov\t\tr14, rax"
														"\n\tsub\t\tr14, start_of_data\n"

														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr8, r15"
														"\n\tshl\t\tr8, 30"
														"\n\tor\t\tr8, r14"
														"\n\tshl\t\tr8, 4"
														"\n\tor\t\tr8, T_FRACTION"
														"\n\tmov\t\t[rax], r8"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_not_a_number:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_number_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tpush\tr8"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_number_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_number_string1:"
														"\n\t\tdb \"Exception in *: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_number_string2:"
														"\n\t\tdb \" is not a number\", 10, 0\n\n"
														; actual body


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;=================== * ===================\n"))))
					((eq? (cadr fvar-rec) '-)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;=================== - ==================="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 0"
														"\n\tje\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														"\n\tmov\t\tr9, 0 ; r9 is a counter for the number of arguments\n\n"

														
														; checking the types of the arguments
														"\n\t." (car fvar-rec) "_arg_type_check_loop:"
														"\n\tcmp\t\tr9, qword [rbp + 5*8]"
														"\n\tje\t\t." (car fvar-rec) "_subtraction\n"

														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tTYPE\t\tr8"
														"\n\tcmp\t\tr8, T_INTEGER"
														"\n\tje\t\t." (car fvar-rec) "_inc_counter\n"

														"\n\tcmp\t\tr8, T_FRACTION"
														"\n\tje\t\t." (car fvar-rec) "_inc_counter\n"

														"\n\tjmp\t\t." (car fvar-rec) "_not_a_number\n"

														"\n\t." (car fvar-rec) "_inc_counter:\n"
														"\n\tinc\t\tr9"
														"\n\tjmp\t\t." (car fvar-rec) "_arg_type_check_loop\n\n"


														; doing subtraction
														"\n\t." (car fvar-rec) "_subtraction:"
														"\n\tmov\t\tr9, 1 ; r9 is a counter for the number of arguments\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tjg\t\t." (car fvar-rec) "_At_least_two_args"
														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tTYPE\tr8"
														"\n\tcmp\t\tr8, T_FRACTION"
														"\n\tje\t\t." (car fvar-rec) "_negate_one_fraction_arg\n"

														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tneg\t\tr8"
														"\n\tshl\t\tr8, 4"
														"\n\tor\t\tr8, T_INTEGER"
														"\n\tmov\t\t[rax], r8"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n"

														"\n\t." (car fvar-rec) "_negate_one_fraction_arg:"
														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_UPPER r8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tneg\t\tr8"
														"\n\tshl\t\tr8, TYPE_BITS"
														"\n\tor\t\tr8, T_INTEGER"
														"\n\tmov\t\tqword [rax], r8"
														"\n\tmov\t\tr15, rax"
														"\n\tsub\t\tr15, start_of_data"
														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_LOWER r8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tmov\t\tqword [rax], r8"
														"\n\tmov\t\tr14, rax"
														"\n\tsub\t\tr14, start_of_data"
														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr8, r15"
														"\n\tshl\t\tr8, 30"
														"\n\tor\t\tr8, r14"
														"\n\tshl\t\tr8, TYPE_BITS"
														"\n\tor\t\tr8, T_FRACTION"
														"\n\tmov\t\tqword [rax], r8"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t; initializing the accumulator to the first number"
														"\n\t." (car fvar-rec) "_At_least_two_args:"
														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tTYPE\tr8"
														"\n\tcmp\t\tr8, T_INTEGER"
														"\n\tje\t\t." (car fvar-rec) "_acc_init_for_integer\n"

														"\n\t; setting the acc numerator"
														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_UPPER r8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr15, r8"
														"\n\t; setting the acc denominator"
														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_LOWER r8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr14, r8"
														"\n\tjmp\t\t." (car fvar-rec) "_subtraction_loop\n"

														"\n\t." (car fvar-rec) "_acc_init_for_integer:"
														"\n\t; setting the curr numerator"
														"\n\tmov\t\tr8, qword [rbp + 6*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr15, r8"
														"\n\t; setting the curr denominator"
														"\n\tmov\t\tr14, 1\n\n"


														"\n\t." (car fvar-rec) "_subtraction_loop:"
														"\n\tcmp\t\tr9, qword [rbp + 5*8]"
														"\n\tje\t\t." (car fvar-rec) "_subtraction_post_processing\n"

														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tTYPE\tr8"
														"\n\tcmp\t\tr8, T_INTEGER"
														"\n\tje\t\t." (car fvar-rec) "_integer_decompose_to_fraction\n"

														"\n\t; setting the curr numerator"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_UPPER r8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr13, r8"
														"\n\t; setting the curr denominator"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA_LOWER r8"
														"\n\tadd\t\tr8, start_of_data"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr12, r8"
														"\n\tjmp\t\t." (car fvar-rec) "_fraction_extensions\n"

														"\n\t." (car fvar-rec) "_integer_decompose_to_fraction:"
														"\n\t; setting the curr numerator"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tDATA\tr8"
														"\n\tmov\t\tr13, r8"
														"\n\t; setting the curr denominator"
														"\n\tmov\t\tr12, 1\n"

														"\n\t." (car fvar-rec) "_fraction_extensions:"
														"\n\tmov\t\tr11, r14 ; saving r14"
														"\n\tmov\t\tr10, r12 ; saving r12"
														"\n\t; applying fraction extension on acc"
														"\n\tmov\t\trax, r14"
														"\n\tmul\t\tr10"
														"\n\tmov\t\tr14, rax"
														"\n\tmov\t\tr8, 0"
														"\n\tcmp\t\tr15, 0"
														"\n\tjge\t\t." (car fvar-rec) "_acc_no_neg1"
														"\n\tneg\t\tr15"
														"\n\tmov\t\tr8, 1"
														"\n\t." (car fvar-rec) "_acc_no_neg1:"
														"\n\tmov\t\trax, r15"
														"\n\tmul\t\tr10"
														"\n\tmov\t\tr15, rax"
														"\n\tcmp\t\tr8, 1"
														"\n\tjne\t\t." (car fvar-rec) "_acc_no_neg2"
														"\n\tneg\t\tr15"
														"\n\t." (car fvar-rec) "_acc_no_neg2:"
														"\n\t; applying fraction extension on curr"
														"\n\tmov\t\trax, r12"
														"\n\tmul\t\tr11"
														"\n\tmov\t\tr12, rax"
														"\n\tmov\t\tr8, 0"
														"\n\tcmp\t\tr13, 0"
														"\n\tjge\t\t." (car fvar-rec) "_curr_no_neg1"
														"\n\tneg\t\tr13"
														"\n\tmov\t\tr8, 1"
														"\n\t." (car fvar-rec) "_curr_no_neg1:"
														"\n\tmov\t\trax, r13"
														"\n\tmul\t\tr11"
														"\n\tmov\t\tr13, rax"
														"\n\tcmp\t\tr8, 1"
														"\n\tjne\t\t." (car fvar-rec) "_curr_no_neg2"
														"\n\tneg\t\tr13"
														"\n\t." (car fvar-rec) "_curr_no_neg2:\n"

														"\n\t; subtracting numerators"
														"\n\tsub\t\tr15, r13\n"

														"\n\t." (car fvar-rec) "_gcd_for_acc:"
														"\n\tmov\t\trdx, 0"
														"\n\tmov\t\trax, r15 ; first - acc numerator"
														"\n\tmov\t\trbx, r14 ; second - acc denominator"
														"\n\tcmp\t\trax, 0"
														"\n\tjge\t\t." (car fvar-rec) "_numerator_no_neg"
														"\n\tneg\t\trax"
														"\n\t." (car fvar-rec) "_numerator_no_neg:"
														"\n\tcmp\t\trbx, 0"
														"\n\tjge\t\t." (car fvar-rec) "_denominator_no_neg"
														"\n\tneg\t\trbx"
														"\n\t." (car fvar-rec) "_denominator_no_neg:"
														"\n\tcmp\t\trax, rbx"
														"\n\tjge\t\t." (car fvar-rec) "_gcd_loop\n"

														"\n\txchg\t\trax, rbx\n"
														
														"\n\t." (car fvar-rec) "_gcd_loop:"
														"\n\tcmp\t\trbx, 0"
														"\n\tje\t\t." (car fvar-rec) "_gcd_loop_end"
														"\n\tmov\t\trdx, 0"
														"\n\tdiv\t\trbx"
														"\n\tmov\t\trax, rbx"
														"\n\tmov\t\trbx, rdx"
														"\n\tjmp\t\t." (car fvar-rec) "_gcd_loop"
														"\n\t." (car fvar-rec) "_gcd_loop_end:\n"

														"\n\t; rax should contain the gcd by now"
														"\n\tmov\t\tr10, rax ; saving the gcd in r10"
														"\n\tcmp\t\tr10, 1"
														"\n\tje\t\t." (car fvar-rec) "_no_fraction_simplification\n"

														"\n\t; in case the gcd is not 1, perform fraction simplification"
														"\n\t; simplify the numerator"
														"\n\tmov\t\trdx, 0"
														"\n\tmov\t\trax, r15"
														"\n\tdiv\t\tr10"
														"\n\tmov\t\tr15, rax"
														"\n\t; simplify the denominator"
														"\n\tmov\t\trdx, 0"
														"\n\tmov\t\trax, r14"
														"\n\tdiv\t\tr10"
														"\n\tmov\t\tr14, rax\n"														

														"\n\t." (car fvar-rec) "_no_fraction_simplification:"
														"\n\tinc\t\tr9"
														"\n\tjmp\t\t." (car fvar-rec) "_subtraction_loop\n\n"


														"\n\t." (car fvar-rec) "_subtraction_post_processing:"
														"\n\tmov\t\tr11, r15"
														"\n\tmov\t\tr10, r14"
														"\n\tcmp\t\tr11, 0"
														"\n\tjge\t\t." (car fvar-rec) "_numerator_no_neg2"
														"\n\tneg\t\tr11"
														"\n\t." (car fvar-rec) "_numerator_no_neg2:"
														"\n\tcmp\t\tr10, 0"
														"\n\tjge\t\t." (car fvar-rec) "_denominator_no_neg2"
														"\n\tneg\t\tr10"
														"\n\t." (car fvar-rec) "_denominator_no_neg2:"
														"\n\tcmp\t\tr11, r10"
														"\n\tjl\t\t." (car fvar-rec) "_generate_fraction_result\n"

														"\n\tmov\t\trdx, 0"
														"\n\tmov\t\trax, r11"
														"\n\tdiv\t\tr10"
														"\n\tcmp\t\trdx, 0"
														"\n\tjne\t\t." (car fvar-rec) "_generate_fraction_result\n\n"


														"\n\t; generate integer result"
														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr8, r15"
														"\n\tshl\t\tr8, 4"
														"\n\tor\t\tr8, T_INTEGER"
														"\n\tmov\t\t[rax], r8"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t; generate fraction result"
														"\n\t." (car fvar-rec) "_generate_fraction_result:"
														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr8, r15"
														"\n\tshl\t\tr8, 4"
														"\n\tor\t\tr8, T_INTEGER"
														"\n\tmov\t\t[rax], r8"
														"\n\tmov\t\tr15, rax"
														"\n\tsub\t\tr15, start_of_data\n"

														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr8, r14"
														"\n\tshl\t\tr8, 4"
														"\n\tor\t\tr8, T_INTEGER"
														"\n\tmov\t\t[rax], r8"
														"\n\tmov\t\tr14, rax"
														"\n\tsub\t\tr14, start_of_data\n"

														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr8, r15"
														"\n\tshl\t\tr8, 30"
														"\n\tor\t\tr8, r14"
														"\n\tshl\t\tr8, 4"
														"\n\tor\t\tr8, T_FRACTION"
														"\n\tmov\t\t[rax], r8"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_not_a_number:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_number_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\tr8, qword [rbp + (6 + r9)*8]"
														"\n\tmov\t\tr8, [r8]"
														"\n\tpush\tr8"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_number_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_number_string1:"
														"\n\t\tdb \"Exception in -: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_number_string2:"
														"\n\t\tdb \" is not a number\", 10, 0\n\n"
														; actual body


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;=================== - ===================\n"))))
					((eq? (cadr fvar-rec) 'boolean?)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;================ boolean? ==============="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]" 
														"\n\tmov\t\trbx, r13"
														"\n\tTYPE\trbx"
														"\n\tcmp\t\trbx, T_BOOL"
														"\n\tjne\t\t.not_a_boolean\n\n"


														; case 1: it's a boolean:
														"\n\tmov\t\trax, sobTrue"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														; case 2: not boolean:														
														"\n\t.not_a_boolean:"
														"\n\tmov\t\trax, sobFalse"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"
														; actual body


														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;================ boolean? ===============\n"))))
					((eq? (cadr fvar-rec) 'car)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;================== car =================="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tmov\t\trbx, r13"
														"\n\tTYPE\trbx"
														"\n\tcmp\t\trbx, T_PAIR"
														"\n\tjne\t\t." (car fvar-rec) "_not_a_pair\n\n"


														"\n\tDATA_UPPER r13"
														"\n\tadd\t\tr13, start_of_data"
														"\n\tmov\t\trax, r13"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_not_a_pair:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_pair_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_pair_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_pair_string1:"
														"\n\t\tdb \"Exception in car: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_pair_string2:"
														"\n\t\tdb \" is not a pair\", 10, 0\n\n"
														; actual body


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;================== car ==================\n"))))
					((eq? (cadr fvar-rec) 'cdr)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;================== cdr =================="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tmov\t\trbx, r13"
														"\n\tTYPE\trbx"
														"\n\tcmp\t\trbx, T_PAIR"
														"\n\tjne\t\t." (car fvar-rec) "_not_a_pair\n\n"


														"\n\tDATA_LOWER r13"
														"\n\tadd\t\tr13, start_of_data"
														"\n\tmov\t\trax, r13"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_not_a_pair:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_pair_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_pair_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_pair_string1:"
														"\n\t\tdb \"Exception in cdr: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_pair_string2:"
														"\n\t\tdb \" is not a pair\", 10, 0\n\n"
														; actual body


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;================== cdr ==================\n"))))
					((eq? (cadr fvar-rec) 'char->integer)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;============= char->integer ============="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"

														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														"\n\tmov\t\trax, [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tmov\t\trbx, r13"
														"\n\tTYPE\trbx"
														"\n\tcmp rbx, T_CHAR"
														"\n\tjne ." (car fvar-rec) "_not_a_char\n\n"


														"\n\tand\t\tr13, 0xFFFFFFFFFFFFFFF0"
														"\n\tor\t\tr13, T_INTEGER"
														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\t[rax], r13"
														"\n\tjmp\t\t ." (car fvar-rec) "_body_end\n\n"

														
														"\n\t." (car fvar-rec) "_not_a_char:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_char_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_char_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_char_string1:"
														"\n\t\tdb \"Exception in char->integer: \", 0"
														"\n\t." (car fvar-rec) "_not_a_char_string2:"
														"\n\t\tdb \" is not a character\", 10, 0\n\n"
														; actual body


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;============= char->integer =============\n"))))
					((eq? (cadr fvar-rec) 'char?)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;================= char? ================="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														"\n\tmov\t\trax, qword[rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tmov\t\trbx, r13"
														"\n\tTYPE\trbx"
														"\n\tcmp\t\trbx, T_CHAR"
														"\n\tjne .not_a_char\n\n"


														; case 1: is a char:
														"\n\tmov\t\trax, sobTrue"
														"\n\tjmp ." (car fvar-rec) "_body_end\n\n"


														; case 2: not a char:
														"\n\t.not_a_char:"
														"\n\tmov\t\trax, sobFalse"
														"\n\tjmp ." (car fvar-rec) "_body_end\n\n"
														; actual body


														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;================= char? =================\n"))))
					((eq? (cadr fvar-rec) 'cons)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;================= cons =================="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 2"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body

														; first implementation
														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc" ; allocate memory for pair in rax
														"\n\tmov\t\tr12, [rbp + 6*8]" ; arg 1 (car)
														"\n\tmov\t\tr13, [rbp + 7*8]" ; arg 2 (cdr)
														"\n\tsub\t\tr12, start_of_data"
														"\n\tshl\t\tr12, 30"
														"\n\tsub\t\tr13, start_of_data"
														"\n\tor\t\tr12, r13"
														"\n\tshl\t\tr12, 4"
														"\n\tor\t\tr12, T_PAIR ; at this stage r12 holds the object of the new pair"
														"\n\tmov\t\t[rax], r12"
														"\n\tjmp ." (car fvar-rec) "_body_end\n\n"
														; first implementation
														; actual body

														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;================= cons ==================\n"))))
					((eq? (cadr fvar-rec) 'denominator)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;============== denominator =============="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														"\n\tmov\t\trbx, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rbx]"
														"\n\tTYPE\t\tr13"
														"\n\tcmp\t\tr13, T_INTEGER"
														"\n\tje\t\t." (car fvar-rec) "_rational_check_ok\n\n"


														"\n\tcmp\t\tr13, T_FRACTION"
														"\n\tje\t\t." (car fvar-rec) "_rational_check_ok\n\n"


														"\n\tjmp\t\t." (car fvar-rec) "_not_a_rational\n\n"


														; rational check ok
														"\n\t." (car fvar-rec) "_rational_check_ok:"
														"\n\tmov\t\tr13, [rbx]"
														"\n\tTYPE\tr13"
														"\n\tcmp\t\tr13, T_INTEGER"
														"\n\tje\t\t." (car fvar-rec) "_denominator_of_an_integer\n\n"


														"\n\tmov\t\tr13, [rbx]"
														"\n\tDATA_LOWER r13"
														"\n\tadd\t\tr13, start_of_data"
														"\n\tmov\t\trax, r13"
														"\n\tjne\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_denominator_of_an_integer:"
														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr13, 1"
														"\n\tshl\t\tr13, 4"
														"\n\tor\t\tr13, T_INTEGER"
														"\n\tmov\t\t[rax], r13"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"
														; rational check ok


														"\n\t." (car fvar-rec) "_not_a_rational:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_rational_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + (6 + r15)*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_rational_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_rational_string1:"
														"\n\t\tdb \"Exception in denominator: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_rational_string2:"
														"\n\t\tdb \" is not a rational number\", 10, 0\n\n"
														; actual body


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;============== denominator ==============\n"))))
					((eq? (cadr fvar-rec) 'eq?)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;================== eq? =================="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 2"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														"\n\tmov\t\tr14, qword [rbp + 6*8] ; r 14 holds the first argument (its pointer)"
														"\n\tmov\t\tr15, qword [rbp + 7*8] ; r 15 holds the second argument (its pointer)\n\n"


														"\n\t; first off, compare addresses. if equal => #t"
														"\n\tcmp\t\tr14, r15"
														"\n\tje\t\t." (car fvar-rec) "_true\n\n"


														"\n\t; compare the types. if they are not equal => #f"
														"\n\tmov\t\tr14, qword [r14] ; r14 holds the argument itself"
														"\n\tmov\t\tr15, qword [r15] ; r15 holds the argument itself"
														"\n\tmov\t\tr12, r14"
														"\n\tmov\t\tr13, r15"
														"\n\tTYPE\tr12"
														"\n\tTYPE\tr13"
														"\n\tcmp\t\tr12, r13"
														"\n\tjne\t\t." (car fvar-rec) "_false\n\n"


														"\n\t; in this case we have different addresses - check the data itself"
														"\n\t; here r11 holds the type of the arguments (up to this point it is concluded that types are equal)"
														"\n\tmov\t\tr11, r13"
														"\n\tcmp\t\tr11, T_INTEGER"
														"\n\tjne\t\t." (car fvar-rec) "_not_integers\n\n"


														"\n\t; the sobs are integers - compare their DATA"
														"\n\tmov\t\tr12, r14"
														"\n\tmov\t\tr13, r15"
														"\n\tDATA\tr12"
														"\n\tDATA\tr13"
														"\n\tcmp\t\tr12, r13"
														"\n\tje\t\t." (car fvar-rec) "_true"
														"\n\tjmp\t\t." (car fvar-rec) "_false\n\n"


														"\n\t; the sobs are not integers"
														"\n\t." (car fvar-rec) "_not_integers:"
														"\n\tcmp\t\tr11, T_FRACTION"
														"\n\tjne\t\t." (car fvar-rec) "_not_fractions\n\n"


														"\n\t; the sobs are fractions - compare their components DATA (assumption - they are simplified fractions)"
														"\n\t; compare numerators"
														"\n\tmov\t\tr12, r14"
														"\n\tmov\t\tr13, r15"
														"\n\tNUMERATOR r12"
														"\n\tNUMERATOR r13"
														"\n\tDATA\t r12"
														"\n\tDATA\t r13"
														"\n\tcmp\t\tr12, r13"
														"\n\tjne\t\t." (car fvar-rec) "_false"
														"\n\t; numerators were ok. compare denominators"
														"\n\tmov\t\tr12, r14"
														"\n\tmov\t\tr13, r15"
														"\n\tDENOMINATOR r12"
														"\n\tDENOMINATOR r13"
														"\n\tDATA\t r12"
														"\n\tDATA\t r13"
														"\n\tcmp\t\tr12, r13"
														"\n\tje\t\t." (car fvar-rec) "_true"
														"\n\tjmp\t\t." (car fvar-rec) "_false\n\n"


														"\n\t; the sobs are not fractions"
														"\n\t." (car fvar-rec) "_not_fractions:"
														"\n\tcmp\t\tr11, T_BOOL"
														"\n\tjne\t\t." (car fvar-rec) "_not_booleans\n\n"


														"\n\t; the sobs are booleans - compare their DATA"
														"\n\tmov\t\tr12, r14"
														"\n\tmov\t\tr13, r15"
														"\n\tDATA\t r12"
														"\n\tDATA\t r13"
														"\n\tcmp\t\tr12, r13"
														"\n\tje\t\t." (car fvar-rec) "_true"
														"\n\tjmp\t\t." (car fvar-rec) "_false\n\n"


														"\n\t; the sobs are not booleans"
														"\n\t." (car fvar-rec) "_not_booleans:"
														"\n\tcmp\t\tr11, T_CHAR"
														"\n\tjne\t\t." (car fvar-rec) "_not_chars\n\n"


														"\n\t; the sobs are chars - compare their DATA"
														"\n\tmov\t\tr12, r14"
														"\n\tmov\t\tr13, r15"
														"\n\tDATA\tr12"
														"\n\tDATA\tr13"
														"\n\tcmp\t\tr12, r13"
														"\n\tje\t\t." (car fvar-rec) "_true"
														"\n\tjmp\t\t." (car fvar-rec) "_false\n\n"


														"\n\t; the sobs are not chars"
														"\n\t." (car fvar-rec) "_not_chars:"
														"\n\tcmp\t\tr11, T_STRING"
														"\n\tjne\t\t." (car fvar-rec) "_not_strings\n\n"


														"\n\t; the sobs are strings - compare their DATA"
														"\n\t." (car fvar-rec) "_strings_compare: ; this label is used to jump here from other cases that require string compare"
														"\n\t; first compare their lengths - its faster"
														"\n\tmov\t\tr12, r14"
														"\n\tmov\t\tr13, r15"
														"\n\tSTRING_LENGTH r12"
														"\n\tSTRING_LENGTH r13"
														"\n\tcmp\t\tr12, r13"
														"\n\tjne\t\t." (car fvar-rec) "_false"
														"\n\t; if lengths are equal, loop over the strings and compare"
														"\n\tmov\t\tr9, r12 ; save the strings length into r9"
														"\n\tmov\t\tr8, 0"
														"\n\tmov\t\tr12, r14"
														"\n\tmov\t\tr13, r15"
														"\n\tSTRING_ELEMENTS r12"
														"\n\tSTRING_ELEMENTS r13"
														"\n\t." (car fvar-rec) "_string_compare_loop:"
														"\n\tcmp\t\tr8, r9"
														"\n\tje\t\t." (car fvar-rec) "_string_compare_loop_end"
														"\n\txor\t\trax, rax"
														"\n\txor\t\trcx, rcx"
														"\n\tmov\t\tal, byte [r12 + r8]"
														"\n\tmov\t\tcl, byte [r13 + r8]"
														"\n\tcmp\t\tal, cl"
														"\n\tjne\t\t." (car fvar-rec) "_false"
														"\n\tinc\t\tr8"
														"\n\tjmp\t\t." (car fvar-rec) "_string_compare_loop"
														"\n\t." (car fvar-rec) "_string_compare_loop_end:"
														"\n\tjmp\t\t." (car fvar-rec) "_true\n\n"


														"\n\t; the sobs are not strings"
														"\n\t." (car fvar-rec) "_not_strings:"
														"\n\tcmp\t\tr11, T_SYMBOL"
														"\n\tjne\t\t." (car fvar-rec) "_not_symbols\n\n"


														"\n\t; the sobs are symbols - compare the pointers to the strings"
														"\n\tcmp\t\tr14, r15 ; from the design of the symbol structure, we can compare the sob itself to determine string pointer equivalence"
														"\n\tje\t\t." (car fvar-rec) "_true"
														"\n\t; the symmbols point to 2 different string addresses - in this case check their strings"
														"\n\tDATA\tr14"
														"\n\tadd\t\tr14, start_of_data"
														"\n\tmov\t\tr14, qword [r14]"
														"\n\tDATA\tr15"
														"\n\tadd\t\tr15, start_of_data"
														"\n\tmov\t\tr15, qword [r15]"
														"\n\tjmp\t\t." (car fvar-rec) "_strings_compare"


														"\n\t; the sobs are not symbols"
														"\n\t." (car fvar-rec) "_not_symbols:"
														"\n\tcmp\t\tr11, T_CLOSURE"
														"\n\tjne\t\t." (car fvar-rec) "_not_closures\n\n"


														"\n\t; sobs are closures - compare their CLOSURE_ENV and CLOSURE_CODE pointers"
														"\n\t; compare environments"
														"\n\tmov\t\tr12, r14"
														"\n\tmov\t\tr13, r15"
														"\n\tCLOSURE_ENV r12"
														"\n\tCLOSURE_ENV r13"
														"\n\tcmp\t\tr12, r13"
														"\n\tjne\t\t." (car fvar-rec) "_false"
														"\n\t; environments were ok. compare codes"
														"\n\tmov\t\tr12, r14"
														"\n\tmov\t\tr13, r15"
														"\n\tCLOSURE_CODE r12"
														"\n\tCLOSURE_CODE r13"
														"\n\tcmp\t\tr12, r13"
														"\n\tje\t\t." (car fvar-rec) "_true"
														"\n\tjmp\t\t." (car fvar-rec) "_false\n\n"


														"\n\t; the sobs are not closures"
														"\n\t." (car fvar-rec) "_not_closures:"
														"\n\tcmp\t\tr11, T_PAIR"
														"\n\tjne\t\t." (car fvar-rec) "_not_pairs\n\n"


														;"\n\t; the sobs are pairs - recursive checks for their cars and cdrs"
														;"\n\t; compare cars"
														;"\n\tmov\t\tr12, r14"
														;"\n\tmov\t\tr13, r15"
														;"\n\tDATA_UPPER r12"
														;"\n\tadd\t\tr12, start_of_data"
														;"\n\tDATA_UPPER r13"
														;"\n\tadd\t\tr13, start_of_data"
														;"\n\t; r12, r13 now hold the addresses of the cars - we need to push those as arguments to eq?"
														;"\n\t; first save all registers that are being used"
														;"\n\tpush\tr15"
														;"\n\tpush\tr14"
														;"\n\tpush\tr13"
														;"\n\tpush\tr12"
														;"\n\tpush\tr11"
														;"\n\tpush\trdx"
														;"\n\tpush\trcx"
														;"\n\tpush\trbx"
														;"\n\tpush\trax"
														;"\n\t; calling sequence"
														;"\n\tmov\t\trax, sobNil"
														;"\n\tpush\trax ; pushing the empty list, as all calls do"
														;"\n\tpush\tr13 ; pushing the car of the second arg"
														;"\n\tpush\tr12 ; pushing the car of the first arg"
														;"\n\tpush\t2 ; argument count"
														;"\n\tpush\t0 ; eq?'s environment - is always zero"
														;"\n\tmov\t\trcx, qword [rbp + 3*8]"
														;"\n\tpush\trcx ; pushing current ast end label"
														;"\n\tmov\t\trcx, qword [rbp + 2*8]"
														;"\n\tpush\trcx ; pushing current ast starting base pointer"
														;"\n\tmov\t\trbx, qword [eq?] ; for exceptions purposes"
														;"\n\tcall\t" (car fvar-rec) "_body"
														;"\n\tadd\t\trsp, 7*8"
														;"\n\tmov\t\tr9, rax"
														;"\n\tpop\t\trax"
														;"\n\tpop\t\trbx"
														;"\n\tpop\t\trcx"
														;"\n\tpop\t\trdx"
														;"\n\tpop\t\tr11"
														;"\n\tpop\t\tr12"
														;"\n\tpop\t\tr13"
														;"\n\tpop\t\tr14"
														;"\n\tpop\t\tr15"
														;"\n\tcmp\t\tr9, sobFalse"
														;"\n\tje\t\t." (car fvar-rec) "_false"
														;"\n\t; compare cdrs"
														;"\n\tmov\t\tr12, r14"
														;"\n\tmov\t\tr13, r15"
														;"\n\tDATA_LOWER r12"
														;"\n\tadd\t\tr12, start_of_data"
														;"\n\tDATA_LOWER r13"
														;"\n\tadd\t\tr13, start_of_data"
														;"\n\t; r12, r13 now hold the addresses of the cdrs - we need to push those as arguments to eq?"
														;"\n\t; first save all registers that are being used"
														;"\n\tpush\tr15"
														;"\n\tpush\tr14"
														;"\n\tpush\tr13"
														;"\n\tpush\tr12"
														;"\n\tpush\tr11"
														;"\n\tpush\trdx"
														;"\n\tpush\trcx"
														;"\n\tpush\trbx"
														;"\n\tpush\trax"
														;"\n\t; calling sequence"
														;"\n\tmov\t\trax, sobNil"
														;"\n\tpush\trax ; pushing the empty list, as all calls do"
														;"\n\tpush\tr13 ; pushing the cdr of the second arg"
														;"\n\tpush\tr12 ; pushing the cdr of the first arg"
														;"\n\tpush\t2 ; argument count"
														;"\n\tpush\t0 ; eq?'s environment - is always zero"
														;"\n\tmov\t\trcx, qword [rbp + 3*8]"
														;"\n\tpush\trcx ; pushing current ast end label"
														;"\n\tmov\t\trcx, qword [rbp + 2*8]"
														;"\n\tpush\trcx ; pushing current ast starting base pointer"
														;"\n\tmov\t\trbx, qword [eq?] ; for exceptions purposes"
														;"\n\tcall\t" (car fvar-rec) "_body"
														;"\n\tadd\t\trsp, 7*8"
														;"\n\tmov\t\tr9, rax"
														;"\n\tpop\t\trax"
														;"\n\tpop\t\trbx"
														;"\n\tpop\t\trcx"
														;"\n\tpop\t\trdx"
														;"\n\tpop\t\tr11"
														;"\n\tpop\t\tr12"
														;"\n\tpop\t\tr13"
														;"\n\tpop\t\tr14"
														;"\n\tpop\t\tr15"
														;"\n\tcmp\t\tr9, sobTrue"
														;"\n\tje\t\t." (car fvar-rec) "_true"
														;"\n\tjmp\t\t." (car fvar-rec) "_false"


														"\n\t; the sobs are not pairs"
														"\n\t." (car fvar-rec) "_not_pairs:"
														"\n\tcmp\t\tr11, T_VECTOR"
														"\n\tjne\t\t." (car fvar-rec) "_not_vectors\n\n"


														;"\n\t; the sobs are vectors - go over their elements and recuresively compare them"
														;"\n\t; first compare their lengths - its faster"
														;"\n\tmov\t\tr12, r14"
														;"\n\tmov\t\tr13, r15"
														;"\n\tVECTOR_LENGTH r12"
														;"\n\tVECTOR_LENGTH r13"
														;"\n\tcmp\t\tr12, r13"
														;"\n\tjne\t\t." (car fvar-rec) "_false"
														;"\n\t; if lengths are equal, loop over the elements of the vectors and compare recursively"
														;"\n\tmov\t\tr9, r12 ; save the vectors length into r9"
														;"\n\tmov\t\tr8, 0"
														;"\n\tmov\t\tr12, r14"
														;"\n\tmov\t\tr13, r15"
														;"\n\tVECTOR_ELEMENTS r12"
														;"\n\tVECTOR_ELEMENTS r13"
														;"\n\t." (car fvar-rec) "_vector_compare_loop:"
														;"\n\tcmp\t\tr8, r9"
														;"\n\tje\t\t." (car fvar-rec) "_vector_compare_loop_end"
														;"\n\tmov\t\tr14, qword [r12 + r8*8] ; r14 holds the first argument (will be pushed second)"
														;"\n\tmov\t\tr15, qword [r13 + r8*8] ; r15 holds the second argument (will be pushed first)"
														;"\n\t; first save all registers that are being used"
														;"\n\tpush\tr15"
														;"\n\tpush\tr14"
														;"\n\tpush\tr13"
														;"\n\tpush\tr12"
														;"\n\tpush\tr11"
														;"\n\tpush\tr9"
														;"\n\tpush\tr8"
														;"\n\tpush\trdx"
														;"\n\tpush\trcx"
														;"\n\tpush\trbx"
														;"\n\tpush\trax"
														;"\n\t; calling sequence"
														;"\n\tmov\t\trax, sobNil"
														;"\n\tpush\trax ; pushing the empty list, as all calls do"
														;"\n\tpush\tr15 ; pushing the address of the second arg"
														;"\n\tpush\tr14 ; pushing the address of the first arg"
														;"\n\tpush\t2 ; argument count"
														;"\n\tpush\t0 ; eq?'s environment - is always zero"
														;"\n\tmov\t\trcx, qword [rbp + 3*8]"
														;"\n\tpush\trcx ; pushing current ast end label"
														;"\n\tmov\t\trcx, qword [rbp + 2*8]"
														;"\n\tpush\trcx ; pushing current ast starting base pointer"
														;"\n\tmov\t\trbx, qword [eq?] ; for exceptions purposes"
														;"\n\tcall\t" (car fvar-rec) "_body"
														;"\n\tadd\t\trsp, 7*8"
														;"\n\tmov\t\tr9, rax"
														;"\n\tpop\t\trax"
														;"\n\tpop\t\trbx"
														;"\n\tpop\t\trcx"
														;"\n\tpop\t\trdx"
														;"\n\tpop\t\tr8"
														;"\n\tpop\t\tr9"
														;"\n\tpop\t\tr11"
														;"\n\tpop\t\tr12"
														;"\n\tpop\t\tr13"
														;"\n\tpop\t\tr14"
														;"\n\tpop\t\tr15"
														;"\n\tcmp\t\tr9, sobFalse"
														;"\n\tje\t\t." (car fvar-rec) "_false"
														;"\n\tinc\t\tr8"
														;"\n\tjmp\t\t." (car fvar-rec) "_vector_compare_loop"
														;"\n\t." (car fvar-rec) "_vector_compare_loop_end:"
														;"\n\tjmp\t\t." (car fvar-rec) "_true\n\n"


														"\n\t; the sobs are not vectors - continue to false answer"
														"\n\t." (car fvar-rec) "_not_vectors:\n\n"


														"\n\t." (car fvar-rec) "_false:"
														"\n\tmov\t\trax, sobFalse"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_true:"
														"\n\tmov\t\trax, sobTrue"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"
														; actual body


														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;================== eq? ==================\n"))))
					((eq? (cadr fvar-rec) 'integer?)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;=============== integer? ================"
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tmov\t\trbx, r13"
														"\n\tTYPE\trbx"
														"\n\tcmp\t\trbx, T_INTEGER"
														"\n\tje\t\t." (car fvar-rec) "_true\n\n"


														"\n\tmov\t\trax, sobFalse"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_true:"
														"\n\tmov\t\trax, sobTrue"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"
														; actual body


														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;=============== integer? ================\n"))))
					((eq? (cadr fvar-rec) 'integer->char)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;============= integer->char ============="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"

														; actual body
														"\n\tmov\t\trax, [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tmov\t\trbx, r13"
														"\n\tTYPE\trbx"
														"\n\tcmp rbx, T_INTEGER"
														"\n\tjne ." (car fvar-rec) "_not_an_integer"

														"\n\tand\t\tr13, 0xFFFFFFFFFFFFFFF0"
														"\n\tor\t\tr13, T_CHAR"
														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\t[rax], r13"
														"\n\tjmp ." (car fvar-rec) "_body_end"


														"\n\t." (car fvar-rec) "_not_an_integer:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_an_integer_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_an_integer_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_an_integer_string1:"
														"\n\t\tdb \"Exception in integer->char: \", 0 "
														"\n\t." (car fvar-rec) "_not_an_integer_string2:"
														"\n\t\tdb \" is not a valid unicode scalar value\", 10, 0\n\n"
														; actual body


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;============= integer->char =============\n"))))
					;((eq? (cadr fvar-rec) 'list)
					;	(begin
					;		(set! primitives-string (string-append
					;									primitives-string
					;									"\n\t;================= list =================="
					;									"\n\tmov\t\trbx, 0"
					;									"\n\tmov\t\trdi, 16"
					;									"\n\tcall\tmalloc"
					;									"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
					;									"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


					;									"\n\t" (car fvar-rec) "_body:"
					;									"\n\tpush\trbp"
					;									"\n\tmov\t\trbp, rsp\n\n"


					;									; actual body
					;									; actual body


					;									"\n\t." (car fvar-rec) "_body_end:"
					;									"\n\tmov\t\trsp, rbp"
					;									"\n\tpop\t\trbp"
					;									"\n\tret\n\n"


					;									"\n\t" (car fvar-rec) "_handle_end:"
					;									"\n\tmov\t\trax, [rax]"
					;									"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
					;									"\n\t;================= list ==================\n"))))
					((eq? (cadr fvar-rec) 'make-string)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;============== make-string =============="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tje\t\t." (car fvar-rec) "_correct_arg_count\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 2"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														"\n\t." (car fvar-rec) "_correct_arg_count:\n\n"


														; actual body
														"\n\tmov\t\trbx, qword [rbp + 6*8] ; rbx holds a pointer to the string length"
														"\n\tmov\t\tr13, [rbx]"
														"\n\tTYPE\tr13"
														"\n\tcmp\t\tr13, T_INTEGER"
														"\n\tjne\t\t." (car fvar-rec) "_not_a_nonnegative_fixnum\n\n"


														"\n\tmov\t\tr13, [rbx]"
														"\n\tDATA\tr13"
														"\n\tcmp\t\tr13, 0"
														"\n\tjl\t\t." (car fvar-rec) "_not_a_nonnegative_fixnum\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tje\t\t." (car fvar-rec) "_unspecified_string_chars\n\n"


														"\n\tmov\t\trbx, qword [rbp + 7*8] ; rbx holds a pointer to the char"
														"\n\tmov\t\tr13, [rbx]"
														"\n\tTYPE\tr13"
														"\n\tcmp\t\tr13, T_CHAR"
														"\n\tjne\t\t." (car fvar-rec) "_not_a_character\n\n"


														; Put code for "fffff" here
														; Setting r12 to the char of the second argument
														"\n\tmov\t\tr12, qword [rbp + 7*8]"
														"\n\tmov\t\tr12, [r12]"
														"\n\tDATA\tr12"
														"\n\tand\t\tr12, 0xff\n\n"


														; Initializing the first byte of the string
														"\n\tmov\t\trbx, qword [rbp + 6*8] ; rbx holds a pointer to the string length"
														"\n\tmov\t\tr13, [rbx]"
														"\n\tDATA\tr13"
														"\n\tadd\t\tr13, 8"
														"\n\tmov\t\trdi, r13"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr13, [rbx]"
														"\n\tDATA\tr13 ; r13 holds the vector length in bytes"
														"\n\tmov\t\tqword [rax], r13"
														"\n\tshl\t\tqword [rax], 30"
														"\n\tlea\t\tr13, [rax + 1*8]"
														"\n\tsub\t\tr13, start_of_data"
														"\n\tor\t\tqword [rax], r13"
														"\n\tshl\t\tqword [rax], TYPE_BITS"
														"\n\tor\t\tqword [rax], T_STRING\n\n"


														; Loop over the string length and put in its elements the char from the second argument.
														"\n\tmov\t\tr15, [rbx]"
														"\n\tDATA\tr15 ; r15 holds the vector length"
														"\n\tmov\t\tr14, 0 ; r14 will be the loop counter\n\n"


														"\n\t." (car fvar-rec) "_put_arg_char_in_string_elements_loop:"
														"\n\tcmp\t\tr14, r15"
														"\n\tje\t\t." (car fvar-rec) "_put_arg_char_in_string_elements_loop_end\n"

														"\n\tmov\t\tqword [rax + 1*8 + r14], r12"
														"\n\tinc\t\tr14"
														"\n\tjmp\t\t." (car fvar-rec) "_put_arg_char_in_string_elements_loop\n"

														"\n\t." (car fvar-rec) "_put_arg_char_in_string_elements_loop_end:"
														"\n\tjmp ." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_unspecified_string_chars:"
														; Put code for "\x0;\x0;\x0;\x0;\x0;" here
														; Putting char #\x0; into r12
														"\n\tmov\t\tr12, 0 ; r12 now holds a pointer to a runtime char F\n\n"


														; Initializing the first byte of the string
														"\n\tmov\t\trbx, qword [rbp + 6*8] ; rbx holds a pointer to the string length"
														"\n\tmov\t\tr13, [rbx]"
														"\n\tDATA\tr13"
														"\n\tadd\t\tr13, 8"
														"\n\tmov\t\trdi, r13"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr13, [rbx]"
														"\n\tDATA\tr13 ; r13 holds the vector length in bytes"
														"\n\tmov\t\tqword [rax], r13"
														"\n\tshl\t\tqword [rax], 30"
														"\n\tlea\t\tr13, [rax + 1*8]"
														"\n\tsub\t\tr13, start_of_data"
														"\n\tor\t\tqword [rax], r13"
														"\n\tshl\t\tqword [rax], TYPE_BITS"
														"\n\tor\t\tqword [rax], T_STRING\n\n"


														; Loop over the vector length and initialize its elements to zero.
														"\n\tmov\t\tr15, [rbx]"
														"\n\tDATA\tr15 ; r15 holds the vector length"
														"\n\tmov\t\tr14, 0 ; r14 will be the loop counter\n\n"


														"\n\t." (car fvar-rec) "_init_chars_to_nul_char_loop:"
														"\n\tcmp\t\tr14, r15"
														"\n\tje\t\t." (car fvar-rec) "_init_chars_to_nul_char_loop_end\n"

														"\n\tmov\t\tqword [rax + 1*8 + r14], r12"
														"\n\tinc\t\tr14"
														"\n\tjmp\t\t." (car fvar-rec) "_init_chars_to_nul_char_loop\n"

														"\n\t." (car fvar-rec) "_init_chars_to_nul_char_loop_end:"
														"\n\tjmp ." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_not_a_character:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_character_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_character_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_character_string1:"
														"\n\t\tdb \"Exception in make-string: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_character_string2:"
														"\n\t\tdb \" is not a character\", 10, 0\n\n"


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_not_a_nonnegative_fixnum:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_nonnegative_fixnum_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_nonnegative_fixnum_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_nonnegative_fixnum_string1:"
														"\n\t\tdb \"Exception in make-string: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_nonnegative_fixnum_string2:"
														"\n\t\tdb \" is not a nonnegative fixnum\", 10, 0\n\n"
														; actual body


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;============== make-string ==============\n"))))
					((eq? (cadr fvar-rec) 'make-vector)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;============== make-vector =============="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tje\t\t." (car fvar-rec) "_correct_arg_count\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 2"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														"\n\t." (car fvar-rec) "_correct_arg_count:\n\n"


														; actual body
														"\n\tmov\t\trbx, qword [rbp + 6*8] ; rbx holds a pointer to the vector length"
														"\n\tmov\t\tr13, [rbx]"
														"\n\tTYPE\tr13"
														"\n\tcmp\t\tr13, T_INTEGER"
														"\n\tjne\t\t." (car fvar-rec) "_not_a_nonnegative_fixnum\n\n"


														"\n\tmov\t\tr13, [rbx]"
														"\n\tDATA\tr13"
														"\n\tcmp\t\tr13, 0"
														"\n\tjl\t\t." (car fvar-rec) "_not_a_nonnegative_fixnum\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tje\t\t." (car fvar-rec) "_unspecified_vector_elements\n\n"


														; Put code for #(4 4 4 4 4) here
														; Setting r12 to the second argument
														"\n\tmov\t\tr12, qword [rbp + 7*8]\n\n"


														; Initializing the first qword of the vector
														"\n\tmov\t\tr13, [rbx]"
														"\n\tDATA\tr13"
														"\n\tinc\t\tr13"
														"\n\tshl\t\tr13, 3"
														"\n\tmov\t\trdi, r13"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr13, [rbx]"
														"\n\tDATA\tr13 ; r13 holds the vector length in qwords"
														"\n\tmov\t\tqword [rax], r13"
														"\n\tshl\t\tqword [rax], 30"
														"\n\tlea\t\tr13, [rax + 1*8]"
														"\n\tsub\t\tr13, start_of_data"
														"\n\tor\t\tqword [rax], r13"
														"\n\tshl\t\tqword [rax], TYPE_BITS"
														"\n\tor\t\tqword [rax], T_VECTOR\n\n"


														; Loop over the vector length and put in its elements the address of the second argument.
														"\n\tmov\t\tr15, [rbx]"
														"\n\tDATA\tr15 ; r15 holds the vector length"
														"\n\tmov\t\tr14, 0 ; r14 will be the loop counter\n\n"


														"\n\t." (car fvar-rec) "_put_second_arg_address_in_vector_elements_loop:"
														"\n\tcmp\t\tr14, r15"
														"\n\tje\t\t." (car fvar-rec) "_put_second_arg_address_in_vector_elements_loop_end\n"

														"\n\tmov\t\tqword [rax + 1*8 + r14*8], r12"
														"\n\tinc\t\tr14"
														"\n\tjmp\t\t." (car fvar-rec) "_put_second_arg_address_in_vector_elements_loop\n"

														"\n\t." (car fvar-rec) "_put_second_arg_address_in_vector_elements_loop_end:"
														"\n\tjmp ." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_unspecified_vector_elements:"
														; Put code for #(0 0 0 0 0) here
														; Initializing the runtime constant zero and putting its address into r12
														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tqword [rax], 0"
														"\n\tshl\t\tqword [rax], TYPE_BITS"
														"\n\tor\t\tqword [rax], T_INTEGER"
														"\n\tmov\t\tr12, rax ; r12 now holds a pointer to a runtime constant zero\n\n"


														; Initializing the first qword of the vector
														"\n\tmov\t\tr13, [rbx]"
														"\n\tDATA\tr13"
														"\n\tinc\t\tr13"
														"\n\tshl\t\tr13, 3"
														"\n\tmov\t\trdi, r13"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr13, [rbx]"
														"\n\tDATA\tr13 ; r13 holds the vector length in qwords"
														"\n\tmov\t\tqword [rax], r13"
														"\n\tshl\t\tqword [rax], 30"
														"\n\tlea\t\tr13, [rax + 1*8]"
														"\n\tsub\t\tr13, start_of_data"
														"\n\tor\t\tqword [rax], r13"
														"\n\tshl\t\tqword [rax], TYPE_BITS"
														"\n\tor\t\tqword [rax], T_VECTOR\n\n"


														; Loop over the vector length and initialize its elements to zero.
														"\n\tmov\t\tr15, [rbx]"
														"\n\tDATA\tr15 ; r15 holds the vector length"
														"\n\tmov\t\tr14, 0 ; r14 will be the loop counter\n\n"


														"\n\t." (car fvar-rec) "_init_elements_to_zero_loop:"
														"\n\tcmp\t\tr14, r15"
														"\n\tje\t\t." (car fvar-rec) "_init_elements_to_zero_loop_end\n"

														"\n\tmov\t\tqword [rax + 1*8 + r14*8], r12"
														"\n\tinc\t\tr14"
														"\n\tjmp\t\t." (car fvar-rec) "_init_elements_to_zero_loop\n"

														"\n\t." (car fvar-rec) "_init_elements_to_zero_loop_end:"
														"\n\tjmp ." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_not_a_nonnegative_fixnum:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_nonnegative_fixnum_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_nonnegative_fixnum_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_nonnegative_fixnum_string1:"
														"\n\t\tdb \"Exception in make-vector: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_nonnegative_fixnum_string2:"
														"\n\t\tdb \" is not a nonnegative fixnum\", 10, 0\n\n"
														; actual body


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;============== make-vector ==============\n"))))
					;((eq? (cadr fvar-rec) 'map)
					;	(begin
					;		(set! primitives-string (string-append
					;									primitives-string
					;									"\n\t;================== map =================="
					;									"\n\tmov\t\trbx, 0"
					;									"\n\tmov\t\trdi, 16"
					;									"\n\tcall\tmalloc"
					;									"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
					;									"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


					;									"\n\t" (car fvar-rec) "_body:"
					;									"\n\tpush\trbp"
					;									"\n\tmov\t\trbp, rsp\n\n"


					;									; actual body
					;									; actual body


					;									"\n\t." (car fvar-rec) "_body_end:"
					;									"\n\tmov\t\trsp, rbp"
					;									"\n\tpop\t\trbp"
					;									"\n\tret\n\n"


					;									"\n\t" (car fvar-rec) "_handle_end:"
					;									"\n\tmov\t\trax, [rax]"
					;									"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
					;									"\n\t;================== map ==================\n"))))
					((eq? (cadr fvar-rec) 'not)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;================== not =================="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"

														; actual body
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tcmp\t\tr13, qword [sobFalse]"
														"\n\tjne ." (car fvar-rec) "_not_false"

														; case 1: was false, so we move sobTrue to rax
														"\n\tmov\t\trax, sobTrue"
														"\n\tjmp ." (car fvar-rec) "_body_end"
														
														; case 2: wasn't false, so we move sobFalse to rax
														"\n\t." (car fvar-rec) "_not_false:"
														"\n\tmov\t\trax, sobFalse"
														"\n\tjmp ." (car fvar-rec) "_body_end"
														; actual body


														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;================== not ==================\n"))))
					((eq? (cadr fvar-rec) 'null?)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;================= null? ================="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tmov\t\trbx, r13"
														"\n\tTYPE\trbx"
														"\n\tcmp\t\trbx, T_NIL"
														"\n\tjne\t\t.not_a_nil\n\n"


														; case 1: is nil (null)
														"\n\tmov\t\trax, sobTrue"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														; case 2: not a nil
														"\n\t.not_a_nil:"
														"\n\tmov\t\trax, sobFalse"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"
														; actual body


														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;================= null? =================\n"))))
					((eq? (cadr fvar-rec) 'number?)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;================ number? ================"
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tmov\t\trbx, r13"
														"\n\tTYPE\trbx"
														"\n\tcmp\t\trbx, T_INTEGER"
														"\n\tje\t\t." (car fvar-rec) "_true\n\n"


														"\n\tcmp\t\trbx, T_FRACTION"
														"\n\tje\t\t." (car fvar-rec) "_true\n\n"


														"\n\tmov\t\trax, sobFalse"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_true:\n"
														"\n\tmov\t\trax, sobTrue\n"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"
														; actual body


														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;================ number? ================\n"))))
					((eq? (cadr fvar-rec) 'numerator)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;=============== numerator ==============="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														"\n\tmov\t\trbx, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rbx]"
														"\n\tTYPE\t\tr13"
														"\n\tcmp\t\tr13, T_INTEGER"
														"\n\tje\t\t." (car fvar-rec) "_rational_check_ok\n\n"


														"\n\tcmp\t\tr13, T_FRACTION"
														"\n\tje\t\t." (car fvar-rec) "_rational_check_ok\n\n"


														"\n\tjmp\t\t." (car fvar-rec) "_not_a_rational\n\n"


														; rational check ok
														"\n\t." (car fvar-rec) "_rational_check_ok:"
														"\n\tmov\t\tr13, [rbx]"
														"\n\tTYPE\tr13"
														"\n\tcmp\t\tr13, T_INTEGER"
														"\n\tje\t\t." (car fvar-rec) "_numerator_of_an_integer\n\n"


														"\n\tmov\t\tr13, [rbx]"
														"\n\tDATA_UPPER r13"
														"\n\tadd\t\tr13, start_of_data"
														"\n\tmov\t\trax, r13"
														"\n\tjne\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_numerator_of_an_integer:"
														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr13, [rbx]"
														"\n\tmov\t\t[rax], r13"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"
														; rational check ok


														"\n\t." (car fvar-rec) "_not_a_rational:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_rational_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + (6 + r15)*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_rational_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_rational_string1:"
														"\n\t\tdb \"Exception in numerator: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_rational_string2:"
														"\n\t\tdb \" is not a rational number\", 10, 0\n\n"
														; actual body


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;=============== numerator ===============\n"))))
					((eq? (cadr fvar-rec) 'pair?)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;================= Pair? ================="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														"\n\tmov\t\trax, [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tmov\t\trbx, r13"
														"\n\tTYPE\trbx"
														"\n\tcmp\t\trbx, T_PAIR"
														"\n\tjne\t\t.not_a_pair\n\n"


														; case 1: is a pair 
														"\n\tmov\t\trax, sobTrue"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														; case 2: not a pair
														"\n\t.not_a_pair:"
														"\n\tmov\t\trax, sobFalse"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"
														; actual body


														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;================= Pair? =================\n"))))
					((eq? (cadr fvar-rec) 'procedure?)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;=============== procedure? =============="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														"\n\tmov\t\t rax, [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tmov\t\trbx, r13"
														"\n\tTYPE\trbx"
														"\n\tcmp\t\trbx, T_CLOSURE"
														"\n\tjne\t\t.not_a_procedure\n\n"


														; case 1: is a procedure 
														"\n\tmov\t\trax, sobTrue"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														; case 2: not a procedure
														"\n\t.not_a_procedure:"
														"\n\tmov\t\trax, sobFalse"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"
														; actual body


														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;=============== procedure? ==============\n"))))
					((eq? (cadr fvar-rec) 'rational?)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;=============== rational? ==============="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tmov\t\trbx, r13"
														"\n\tTYPE\trbx"
														"\n\tcmp\t\trbx, T_INTEGER"
														"\n\tje\t\t." (car fvar-rec) "_true\n\n"


														"\n\tcmp\t\trbx, T_FRACTION"
														"\n\tje\t\t." (car fvar-rec) "_true\n\n"


														"\n\tmov\t\trax, sobFalse"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_true:\n"
														"\n\tmov\t\trax, sobTrue\n"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"
														; actual body


														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;=============== rational? ===============\n"))))
					((eq? (cadr fvar-rec) 'remainder)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;=============== remainder ==============="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 2"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														"\n\tmov\t\tr15, 0"
														"\n\tmov\t\trbx, qword [rbp + (6 + r15)*8]"
														"\n\tmov\t\trbx, [rbx]"
														"\n\tTYPE\trbx"
														"\n\tcmp\t\trbx, T_INTEGER"
														"\n\tjne\t\t." (car fvar-rec) "_not_an_integer\n\n"


														"\n\tinc\t\tr15"
														"\n\tmov\t\trbx, qword [rbp + (6 + r15)*8]"
														"\n\tmov\t\trbx, [rbx]"
														"\n\tTYPE\trbx"
														"\n\tcmp\t\trbx, T_INTEGER"
														"\n\tjne\t\t." (car fvar-rec) "_not_an_integer\n\n"


														"\n\tmov\t\tr15, 1"
														"\n\tmov\t\trbx, qword [rbp + (6 + r15)*8]"
														"\n\tmov\t\trbx, [rbx]"
														"\n\tDATA\trbx"
														"\n\tcmp\t\trbx, 0"
														"\n\tje\t\t." (car fvar-rec) "_undefined_for_zero\n\n\n"



														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\trax, [rax]"
														"\n\tDATA\trax"
														"\n\tmov\t\trcx, qword [rbp + 7*8]"
														"\n\tmov\t\trcx, [rcx]"
														"\n\tDATA\trcx"
														"\n\tcmp\t\trax, 0"
														"\n\tjl\t\t." (car fvar-rec) "_negative_devidend\n\n"


														"\n\tcmp\t\trcx, 0"
														"\n\tjl\t\t." (car fvar-rec) "_positive_devidend_negative_divisor\n\n"


														"\n\tmov\t\trdx, 0"
														"\n\tdiv\t\trcx"
														"\n\tmov\t\trax, rdx"
														"\n\tshl\t\trax, 4"
														"\n\tor\t\trax, T_INTEGER"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_positive_devidend_negative_divisor:"
														"\n\tneg\t\trcx"
														"\n\tmov\t\trdx, 0"
														"\n\tdiv\t\trcx"
														"\n\tmov\t\trax, rdx"
														"\n\tshl\t\trax, 4"
														"\n\tor\t\trax, T_INTEGER"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_negative_devidend:"
														"\n\tneg\t\trax"
														"\n\tcmp\t\trcx, 0"
														"\n\tjl\t\t." (car fvar-rec) "_negative_devidend_negative_divisor\n\n"


														"\n\tmov\t\trdx, 0"
														"\n\tdiv\t\trcx"
														"\n\tmov\t\trax, rdx"
														"\n\tneg\t\trax"
														"\n\tshl\t\trax, 4"
														"\n\tor\t\trax, T_INTEGER"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_negative_devidend_negative_divisor:"
														"\n\tneg\t\trcx"
														"\n\tmov\t\trdx, 0"
														"\n\tdiv\t\trcx"
														"\n\tmov\t\trax, rdx"
														"\n\tneg\t\trax"
														"\n\tshl\t\trax, 4"
														"\n\tor\t\trax, T_INTEGER"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n\n"



														"\n\t." (car fvar-rec) "_undefined_for_zero:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_undefined_for_zero_string"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_undefined_for_zero_string:"
														"\n\t\tdb \"Exception in remainder: undefined for 0\", 10, 0\n\n"


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_not_an_integer:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_an_integer_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + (6 + r15)*8]"
														"\n\tmov\t\trax, [rax]"
														"\n\tpush\trax"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_an_integer_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_an_integer_string1:"
														"\n\t\tdb \"Exception in remainder: \", 0 "
														"\n\t." (car fvar-rec) "_not_an_integer_string2:"
														"\n\t\tdb \" is not an integer\", 10, 0\n\n"
														; actual body


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:" ;; at this point we have the number struct. we need to get its address in the const table or to create his new allocation.
														"\n\tmov\t\tr15, 0"
														"\n\tmov\t\tr14, constant_table\n\n"


														"\n\t." (car fvar-rec) "_result_address_lookup_in_const_table_loop:"
														"\n\tcmp\t\tr15, " (number->string (length const-table))
														"\n\tje\t\t." (car fvar-rec) "_result_address_lookup_in_const_table_loop_allocate_new_memory\n"

														"\n\tmov\t\trbx, qword [r14 + r15*8]"
														"\n\tcmp\t\trbx, rax"
														"\n\tjne\t\t." (car fvar-rec) "_result_address_lookup_in_const_table_loop_inc_index\n"

														"\n\tlea\t\trax, [r14 + r15*8]"
														"\n\tjmp\t\t." (car fvar-rec) "_result_address_lookup_in_const_table_loop_end\n"

														"\n\t." (car fvar-rec) "_result_address_lookup_in_const_table_loop_inc_index:"
														"\n\tinc\t\tr15"
														"\n\tjmp\t\t." (car fvar-rec) "_result_address_lookup_in_const_table_loop\n"

														"\n\t." (car fvar-rec) "_result_address_lookup_in_const_table_loop_allocate_new_memory:"
														"\n\tmov\t\tr13, rax"
														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tqword [rax], r13"

														"\n\t." (car fvar-rec) "_result_address_lookup_in_const_table_loop_end:\n\n"


														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;=============== remainder ===============\n"))))
					((eq? (cadr fvar-rec) 'set-car!)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;=============== set-car! ================"
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 2"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														"\n\tmov\t\trax, [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tmov\t\trbx, r13"
														"\n\tTYPE\t\trbx"
														"\n\tcmp\t\trbx, T_PAIR"
														"\n\tjne ."(car fvar-rec) "_not_a_pair\n\n"

														; step 1: clear current car
														"\n\tshl\t\tr13, 30"
														"\n\tshr\t\tr13, 30"
														; step 2: set up the new car
														"\n\tmov\t\tr12, [rbp + 7*8]" ;the item to set to the car of r13
														"\n\tsub\t\tr12, start_of_data"
														"\n\tshl\t\tr12, 34" ; to the spot of the cdr
														"\n\tor\t\tr13,r12" 
														"\n\tmov\t\t[rax], r13"
														"\n\tmov\t\trax, sobVoid"
														"\n\tjmp ." (car fvar-rec) "_body_end\n\n"


														"\n\n\t." (car fvar-rec) "_not_a_pair:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_pair_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_pair_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_pair_string1:"
														"\n\t\tdb \"Exception in set-car!: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_pair_string2:"
														"\n\t\tdb \" is not a pair\", 10, 0\n\n"
														; actual body


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;=============== set-car! ================\n"))))
					((eq? (cadr fvar-rec) 'set-cdr!)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;=============== set-cdr! ================"
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 2"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														"\n\tmov\t\trax, [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tmov\t\trbx, r13"
														"\n\tTYPE\t\trbx"
														"\n\tcmp\t\trbx, T_PAIR"
														"\n\tjne ."(car fvar-rec) "_not_a_pair\n\n"

														; step 1: clear current car
														"\n\tshr\t\tr13, 34"
														"\n\tshl\t\tr13, 34"
														"\n\tor\t\tr13, T_PAIR"
														; step 2: set up the new car
														"\n\tmov\t\tr12, [rbp + 7*8]" ;the item to set to the car of r13
														"\n\tsub\t\tr12, start_of_data"
														"\n\tshl\t\tr12, 4" ; to the spot of the cdr
														"\n\tor\t\tr13,r12" 
														"\n\tmov\t\t[rax], r13"
														"\n\tmov\t\trax, sobVoid"
														"\n\tjmp ." (car fvar-rec) "_body_end\n\n"


														"\n\n\t." (car fvar-rec) "_not_a_pair:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_pair_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_pair_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_pair_string1:"
														"\n\t\tdb \"Exception in set-cdr!: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_pair_string2:"
														"\n\t\tdb \" is not a pair\", 10, 0\n\n"
														; actual body


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n"

														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;=============== set-cdr! ================\n"))))
					((eq? (cadr fvar-rec) 'string-length)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;============= string-length ============="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														"\n\tmov\t\tr8, qword[rbp + 6*8] ; r8 holds a pointer to the string"
														"\n\tmov\t\tr12, [r8]"
														"\n\tTYPE\tr12"
														"\n\tcmp\t\tr12, T_STRING"
														"\n\tjne\t\t." (car fvar-rec) "_not_a_string\n\n"


														"\n\tmov\t\tr12, [r8]"
														"\n\tSTRING_LENGTH r12"
														"\n\tshl\t\tr12, TYPE_BITS"
														"\n\tor\t\tr12, T_INTEGER"
														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tqword [rax], r12"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_not_a_string:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_string_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_string_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_string_string1:"
														"\n\t\tdb \"Exception in string-length: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_string_string2:"
														"\n\t\tdb \" is not a string\", 10, 0\n\n"
														; actual body


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;============= string-length =============\n"))))
					((eq? (cadr fvar-rec) 'string-ref)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;=============== string-ref =============="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 2"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														; get the 2 arguments into registers
														"\n\tmov\t\tr8, qword [rbp + 6*8] ; r8 holds pointer to the string"
														"\n\tmov\t\tr9, qword [rbp + 7*8] ; r9 holds pointer to the number\n\n"


														; check if the string argument is actually a string
														"\n\tmov\t\tr12, [r8]"
														"\n\tTYPE\tr12"
														"\n\tcmp\t\tr12, T_STRING"
														"\n\tjne\t\t." (car fvar-rec) "_not_a_string\n\n"


														; check that the ref is actually a number
														"\n\tmov\t\tr13, [r9]"
														"\n\tTYPE\tr13"
														"\n\tcmp\t\tr13, T_INTEGER"
														"\n\tjne\t\t." (car fvar-rec) "_not_a_valid_index\n\n"


														; check that the ref is in bounds (0 - (n-1))
														"\n\tmov\t\tr13, [r9]"
														"\n\tDATA\tr13 ; r13 holds the ref"
														"\n\tmov\t\tr12, [r8]"
														"\n\tSTRING_LENGTH r12 ; r12 holds the string length"
														"\n\tcmp\t\tr13, 0"
														"\n\tjl\t\t." (car fvar-rec) "_not_a_valid_index\n\n"


														"\n\tdec\t\tr12"
														"\n\tcmp\t\tr13, r12"
														"\n\tjg\t\t." (car fvar-rec) "_not_a_valid_index\n\n"


														"\n\tmov\t\tr12, [r8]"
														"\n\tSTRING_ELEMENTS r12"
														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tbl, byte [r12 + r13]"
														"\n\tshl\t\trbx, TYPE_BITS"
														"\n\tand\t\trbx, 0xff0"
														"\n\tor\t\trbx, T_CHAR"
														"\n\tmov\t\t[rax], rbx"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_not_a_valid_index:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_valid_index_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + 7*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_valid_index_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob_if_not_void"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_valid_index_string1:"
														"\n\t\tdb \"Exception in string-ref: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_valid_index_string2:"
														"\n\t\tdb \" is not a valid index for \", 0\n\n"


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_not_a_string:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_string_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_string_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_string_string1:"
														"\n\t\tdb \"Exception in string-ref: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_string_string2:"
														"\n\t\tdb \" is not a string\", 10, 0\n\n"
														; actual body


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;=============== string-ref ==============\n"))))
					((eq? (cadr fvar-rec) 'string-set!)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;=============== string-set! ============="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 3"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														; get the 3 arguments into registers
														"\n\tmov\t\tr8, qword [rbp + 6*8] ; r8 holds pointer to the string"
														"\n\tmov\t\tr9, qword [rbp + 7*8] ; r9 holds pointer to the index"
														"\n\tmov\t\tr10, qword [rbp + 8*8] ; r10 holds pointer to the char\n\n"


														; check if the string argument is actually a string
														"\n\tmov\t\tr12, [r8]"
														"\n\tTYPE\tr12"
														"\n\tcmp\t\tr12, T_STRING"
														"\n\tjne\t\t." (car fvar-rec) "_not_a_string\n\n"


														; check that the ref is actually a number
														"\n\tmov\t\tr13, [r9]"
														"\n\tTYPE\tr13"
														"\n\tcmp\t\tr13, T_INTEGER"
														"\n\tjne\t\t." (car fvar-rec) "_not_a_valid_index\n\n"


														; check that the ref is in bounds (0 - (n-1))
														"\n\tmov\t\tr13, [r9]"
														"\n\tDATA\tr13 ; r13 holds the ref"
														"\n\tmov\t\tr12, [r8]"
														"\n\tVECTOR_LENGTH r12 ; r12 holds the string length"
														"\n\tcmp\t\tr13, 0"
														"\n\tjl\t\t." (car fvar-rec) "_not_a_valid_index\n\n"


														"\n\tdec\t\tr12"
														"\n\tcmp\t\tr13, r12"
														"\n\tjg\t\t." (car fvar-rec) "_not_a_valid_index\n\n"


														; check that the char is actually a char
														"\n\tmov\t\tr13, [r10]"
														"\n\tTYPE\tr13"
														"\n\tcmp\t\tr13, T_CHAR"
														"\n\tjne\t\t." (car fvar-rec) "_not_a_character\n\n"

														"\n\t.test:"
														;"\n\tmov\t\trax, qword [r8 + 1*8 + r13*8]"
														"\n\tmov\t\tr12, [r8]"
														"\n\tSTRING_ELEMENTS r12"
														;"\n\tsub\t\tr15, start_of_data"
														"\n\tmov\t\tr10, [r10]"
														"\n\tDATA\tr10"
														"\n\tmov\t\trcx, r10"
														"\n\tand\t\trcx, 0xff"
														"\n\tmov\t\tr13, [r9]"
														"\n\tDATA\tr13 ; r13 holds the ref"
														"\n\tmov\t\tbyte [r12 + r13], cl"
														"\n\tmov\t\trax, sobVoid"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_not_a_character:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_character_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + 8*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_character_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_character_string1:"
														"\n\t\tdb \"Exception in string-set!: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_character_string2:"
														"\n\t\tdb \" is not a character\", 10, 0\n\n"


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_not_a_valid_index:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_valid_index_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + 7*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_valid_index_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob_if_not_void"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_valid_index_string1:"
														"\n\t\tdb \"Exception in string-set!: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_valid_index_string2:"
														"\n\t\tdb \" is not a valid index for \", 0\n\n"


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_not_a_string:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_string_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_string_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_string_string1:"
														"\n\t\tdb \"Exception in string-set!: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_string_string2:"
														"\n\t\tdb \" is not a string\", 10, 0\n\n"
														; actual body


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;=============== string-set! =============\n"))))
					((eq? (cadr fvar-rec) 'string->symbol)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;============= string->symbol ============"
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														"\n\tmov\t\trax, [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tmov\t\trbx, r13"
														"\n\tTYPE\trbx"
														"\n\tcmp rbx, T_STRING"
														"\n\tjne ." (car fvar-rec) "_not_a_string\n\n"

														
														"\n\tmov\t\tr15, symbol_table ; r15 holds pointer to the first node in the symbol table - could be a non empty pair, or the empty list\n\n"


														"\n\t." (car fvar-rec) "_symbol_table_traverse_loop:"
														"\n\tmov\t\tr14, qword [r15]"
														"\n\tcmp\t\tr14, SOB_NIL"
														"\n\tje\t\t." (car fvar-rec) "_symbol_table_traverse_loop_end\n"

														"\n\tDATA_UPPER r14"
														"\n\tadd\t\tr14, start_of_data"
														"\n\tmov\t\tr14, qword [r14] ; r14 now holds the symbol object in this iteration"
														"\n\tDATA\tr14"
														"\n\tadd\t\tr14, start_of_data ; r14 now has the string address for this symbol"
														"\n\tcmp\t\tr14, qword [rbp + 6*8] ; comparing symbol string address and argument string address"
														"\n\tjne\t\t." (car fvar-rec) "_next_node\n"

														"\n\t ; match found. return the current symbol"
														"\n\tmov\t\tr14, qword [r15]"
														"\n\tDATA_UPPER r14"
														"\n\tadd\t\tr14, start_of_data"
														"\n\tmov\t\trax, r14"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n"

														"\n\t." (car fvar-rec) "_next_node:"
														"\n\tmov\t\tr15, qword [r15]"
														"\n\tDATA_LOWER r15"
														"\n\tadd\t\tr15, start_of_data"
														"\n\tjmp\t\t." (car fvar-rec) "_symbol_table_traverse_loop\n"

														"\n\t." (car fvar-rec) "_symbol_table_traverse_loop_end:\n\n"


														"\n\t ; no symbol corresponding to the string found in the symbol table - allocate new symbol"
														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tr13, qword [rbp + 6*8]"
														"\n\tsub\t\tr13, start_of_data"
														"\n\tshl\t\tr13, TYPE_BITS"
														"\n\tor\t\tr13, T_SYMBOL"
														"\n\tmov\t\tqword [rax], r13 ; rax now points to the symbol object that we need to return."
														"\n\tmov\t\tr14, rax ; save this pointer because we need to return it."
														"\n\t; allocate memory for the next node."
														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tqword [rax], SOB_NIL"
														"\n\tmov\t\tr13, r14"
														"\n\tsub\t\tr13, start_of_data"
														"\n\tshl\t\tr13, 30"
														"\n\tsub\t\trax, start_of_data"
														"\n\tor\t\tr13, rax"
														"\n\tshl\t\tr13, TYPE_BITS"
														"\n\tor\t\tr13, T_PAIR"
														"\n\tmov\t\tqword [r15], r13"
														"\n\tmov\t\trax, r14"
														"\n\tjmp ." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_not_a_string:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_string_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_string_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_string_string1:"
														"\n\t\tdb \"Exception in string->symbol: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_string_string2:"
														"\n\t\tdb \" is not a string\", 10, 0\n\n"
														; actual body


														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;============= string->symbol ============\n"))))
					((eq? (cadr fvar-rec) 'string?)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;================ string? ================"
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														"\n\tmov\t\t rax, [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tmov\t\trbx, r13"
														"\n\tTYPE\trbx"
														"\n\tcmp\t\trbx, T_STRING"
														"\n\tjne\t\t.not_a_string\n\n"


														; case 1: is a string
														"\n\tmov\t\trax, sobTrue"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														; case 2: not a string
														"\n\t.not_a_string:"
														"\n\tmov\t\trax, sobFalse"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"
														; actual body


														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;================ string? ================\n"))))
					((eq? (cadr fvar-rec) 'symbol?)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;================ symbol? ================"
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														"\n\tmov\t\trax, [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tmov\t\trbx, r13"
														"\n\tTYPE\trbx"
														"\n\tcmp\t\trbx, T_SYMBOL"
														"\n\tjne\t\t.not_a_symbol\n\n"


														; case 1: is a symbol 
														"\n\tmov\t\trax, sobTrue"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														; case 2: not a symbol
														"\n\t.not_a_symbol:"
														"\n\tmov\t\trax, sobFalse"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"
														; actual body


														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;================ symbol? ================\n"))))
					((eq? (cadr fvar-rec) 'symbol->string)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;============ symbol->string ============="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														"\n\tmov\t\trax, [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tmov\t\trbx, r13"
														"\n\tTYPE\trbx"
														"\n\tcmp rbx, T_SYMBOL"
														"\n\tjne ." (car fvar-rec) "_not_a_symbol\n\n"

														
														"\n\tDATA\tr13"
														"\n\tadd\t\tr13, start_of_data"
														"\n\tmov\t\trax, r13"
														"\n\tjmp ." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_not_a_symbol:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_symbol_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_symbol_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_symbol_string1:"
														"\n\t\tdb \"Exception in symbol->string: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_symbol_string2:"
														"\n\t\tdb \" is not a symbol\", 10, 0\n\n"
														; actual body


														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;============ symbol->string =============\n"))))
					((eq? (cadr fvar-rec) 'vector)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;================ vector ================="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														; actual body
														"\n\t; vector header initialization"
														"\n\tmov\t\tr13, qword [rbp + 5*8] ; r13 holds the number of the parameters (the length of the vector in bytes)"
														"\n\tmov\t\trdi, r13"
														"\n\tshl\t\trdi, 3"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tqword [rax], r13"
														"\n\tshl\t\tqword [rax], 30"
														"\n\tlea\t\tr13, [rax + 1*8]"
														"\n\tsub\t\tr13, start_of_data"
														"\n\tor\t\tqword [rax], r13"
														"\n\tshl\t\tqword [rax], TYPE_BITS"
														"\n\tor\t\tqword [rax], T_VECTOR\n\n"


														; Loop over the vector length and put in its elements the address of the elements in the stack.
														"\n\t; loop over the vector length and put in its elements the address of the elements in the stack."
														"\n\tmov\t\tr13, qword [rbp + 5*8] ; r13 holds the number of the parameters (the length of the vector in bytes)"
														"\n\tmov\t\tr14, 0 ; r14 will be the loop counter\n\n"


														"\n\t." (car fvar-rec) "_put_args_addresses_in_vector_elements_loop:"
														"\n\tcmp\t\tr14, r13"
														"\n\tje\t\t." (car fvar-rec) "_put_second_arg_address_in_vector_elements_loop_end\n"

														"\n\tmov\t\tr12, qword [rbp + 6*8 + r14*8]"
														"\n\tmov\t\tqword [rax + 1*8 + r14*8], r12"
														"\n\tinc\t\tr14"
														"\n\tjmp\t\t." (car fvar-rec) "_put_args_addresses_in_vector_elements_loop\n"

														"\n\t." (car fvar-rec) "_put_second_arg_address_in_vector_elements_loop_end:"
														"\n\tjmp ." (car fvar-rec) "_body_end\n\n"
														; actual body


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;================ vector =================\n"))))
					((eq? (cadr fvar-rec) 'vector-length)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;============= vector-length ============="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														"\n\tmov\t\tr8, qword[rbp + 6*8] ; r8 holds a pointer to the vector"
														"\n\tmov\t\tr12, [r8]"
														"\n\tTYPE\tr12"
														"\n\tcmp\t\tr12, T_VECTOR"
														"\n\tjne\t\t." (car fvar-rec) "_not_a_vector\n\n"


														"\n\tmov\t\tr12, [r8]"
														"\n\tVECTOR_LENGTH r12"
														"\n\tshl\t\tr12, TYPE_BITS"
														"\n\tor\t\tr12, T_INTEGER"
														"\n\tmov\t\trdi, 8"
														"\n\tcall\tmalloc"
														"\n\tmov\t\tqword [rax], r12"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_not_a_vector:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_vector_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_vector_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_vector_string1:"
														"\n\t\tdb \"Exception in vector-length: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_vector_string2:"
														"\n\t\tdb \" is not a vector\", 10, 0\n\n"
														; actual body


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;============= vector-length =============\n"))))
					((eq? (cadr fvar-rec) 'vector-ref)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;============== vector-ref ==============="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 2"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														; get the 2 arguments into registers
														"\n\tmov\t\tr8, qword [rbp + 6*8] ; r8 holds pointer to the vector"
														"\n\tmov\t\tr9, qword [rbp + 7*8] ; r9 holds pointer to the number\n\n"


														; check if the vector argument is actually a vector
														"\n\tmov\t\tr12, [r8]"
														"\n\tTYPE\tr12"
														"\n\tcmp\t\tr12, T_VECTOR"
														"\n\tjne\t\t." (car fvar-rec) "_not_a_vector\n\n"


														; check that the ref is actually a number
														"\n\tmov\t\tr13, [r9]"
														"\n\tTYPE\tr13"
														"\n\tcmp\t\tr13, T_INTEGER"
														"\n\tjne\t\t." (car fvar-rec) "_not_a_valid_index\n\n"


														; check that the ref is in bounds (0 - (n-1))
														"\n\tmov\t\tr13, [r9]"
														"\n\tDATA\tr13 ; r13 holds the ref"
														"\n\tmov\t\tr12, [r8]"
														"\n\tVECTOR_LENGTH r12 ; r12 holds the vector length"
														"\n\tcmp\t\tr13, 0"
														"\n\tjl\t\t." (car fvar-rec) "_not_a_valid_index\n\n"


														"\n\tdec\t\tr12"
														"\n\tcmp\t\tr13, r12"
														"\n\tjg\t\t." (car fvar-rec) "_not_a_valid_index\n\n"


														;"\n\tmov\t\trax, qword [r8 + 1*8 + r13*8]"
														"\n\tmov\t\tr12, [r8]"
														"\n\tVECTOR_ELEMENTS r12"
														;"\n\tsub\t\tr15, start_of_data"
														"\n\tmov\t\trax, qword [r12 + r13*8]"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_not_a_valid_index:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_valid_index_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + 7*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_valid_index_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob_if_not_void"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_valid_index_string1:"
														"\n\t\tdb \"Exception in vector-ref: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_valid_index_string2:"
														"\n\t\tdb \" is not a valid index for \", 0\n\n"


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_not_a_vector:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_vector_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_vector_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_vector_string1:"
														"\n\t\tdb \"Exception in vector-ref: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_vector_string2:"
														"\n\t\tdb \" is not a vector\", 10, 0\n\n"
														; actual body


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;============== vector-ref ===============\n"))))
					((eq? (cadr fvar-rec) 'vector-set!)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;============== vector-set! =============="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 3"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														; get the 3 arguments into registers
														"\n\tmov\t\tr8, qword [rbp + 6*8] ; r8 holds pointer to the vector"
														"\n\tmov\t\tr9, qword [rbp + 7*8] ; r9 holds pointer to the number"
														"\n\tmov\t\tr10, qword [rbp + 8*8] ; r10 holds pointer to the object\n\n"


														; check if the vector argument is actually a vector
														"\n\tmov\t\tr12, [r8]"
														"\n\tTYPE\tr12"
														"\n\tcmp\t\tr12, T_VECTOR"
														"\n\tjne\t\t." (car fvar-rec) "_not_a_vector\n\n"


														; check that the ref is actually a number
														"\n\tmov\t\tr13, [r9]"
														"\n\tTYPE\tr13"
														"\n\tcmp\t\tr13, T_INTEGER"
														"\n\tjne\t\t." (car fvar-rec) "_not_a_valid_index\n\n"


														; check that the ref is in bounds (0 - (n-1))
														"\n\tmov\t\tr13, [r9]"
														"\n\tDATA\tr13 ; r13 holds the ref"
														"\n\tmov\t\tr12, [r8]"
														"\n\tVECTOR_LENGTH r12 ; r12 holds the vector length"
														"\n\tcmp\t\tr13, 0"
														"\n\tjl\t\t." (car fvar-rec) "_not_a_valid_index\n\n"


														"\n\tdec\t\tr12"
														"\n\tcmp\t\tr13, r12"
														"\n\tjg\t\t." (car fvar-rec) "_not_a_valid_index\n\n"


														;"\n\tmov\t\trax, qword [r8 + 1*8 + r13*8]"
														"\n\tmov\t\tr12, [r8]"
														"\n\tVECTOR_ELEMENTS r12"
														;"\n\tsub\t\tr15, start_of_data"
														"\n\tmov\t\tqword [r12 + r13*8], r10"
														"\n\tmov\t\trax, sobVoid"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_not_a_valid_index:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_valid_index_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + 7*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_valid_index_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob_if_not_void"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_valid_index_string1:"
														"\n\t\tdb \"Exception in vector-set!: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_valid_index_string2:"
														"\n\t\tdb \" is not a valid index for \", 0\n\n"


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_not_a_vector:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_vector_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_vector_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_vector_string1:"
														"\n\t\tdb \"Exception in vector-set!: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_vector_string2:"
														"\n\t\tdb \" is not a vector\", 10, 0\n\n"
														; actual body


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;============== vector-set! ==============\n"))))
					((eq? (cadr fvar-rec) 'vector?)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;================ vector? ================"
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"


														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"


														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"


														; actual body
														"\n\tmov\t\trax, [rbp + 6*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tmov\t\trbx, r13"
														"\n\tTYPE\trbx"
														"\n\tcmp\t\trbx, T_VECTOR"
														"\n\tjne\t\t.not_a_vector\n\n"


														; case 1: is a vector 
														"\n\tmov\t\trax, sobTrue"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														; case 2: not a vector
														"\n\t.not_a_vector:"
														"\n\tmov\t\trax, sobFalse"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"
														; actual body


														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"


														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"


														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;================ vector? ================\n"))))
					((eq? (cadr fvar-rec) 'zero?)
						(begin
							(set! primitives-string (string-append
														primitives-string
														"\n\t;================= zero? ================="
														"\n\tmov\t\trbx, 0"
														"\n\tmov\t\trdi, 16"
														"\n\tcall\tmalloc"
														"\n\tMAKE_LITERAL_CLOSURE rax, rbx, " (car fvar-rec) "_body"
														"\n\tjmp\t\t" (car fvar-rec) "_handle_end\n\n"
					

														"\n\t" (car fvar-rec) "_body:"
														"\n\tpush\trbp"
														"\n\tmov\t\trbp, rsp\n\n"
					

														"\n\tcmp\t\tqword [rbp + 5*8], 1"
														"\n\tjne\t\t." (car fvar-rec) "_incorrect_arg_count\n\n"
					

														; actual body
														"\n\tmov\t\trbx, qword [rbp + 6*8]"
														"\n\tmov\t\tr13, [rbx]"
														"\n\tTYPE\t\tr13"
														"\n\tcmp\t\tr13, T_INTEGER"
														"\n\tje\t\t." (car fvar-rec) "_number_check_ok\n\n"


														"\n\tcmp\t\tr13, T_FRACTION"
														"\n\tje\t\t." (car fvar-rec) "_number_check_ok\n\n"


														"\n\tjmp\t\t." (car fvar-rec) "_not_a_number\n\n"


														"\n\t." (car fvar-rec) "_number_check_ok:"
														"\n\tmov\t\tr13, [rbx]"
														"\n\tDATA\tr13"
														"\n\tcmp\t\tr13, 0"
														"\n\tjne\t\t." (car fvar-rec) "_not_zero\n\n"


														"\n\tmov\t\trax, sobTrue"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_not_zero:"
														"\n\tmov\t\trax, sobFalse"
														"\n\tjmp\t\t." (car fvar-rec) "_body_end\n\n"


														"\n\t." (car fvar-rec) "_not_a_number:"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_number_string1"
														"\n\tcall\tprintf"
														"\n\tmov\t\trax, qword [rbp + (6 + r15)*8]"
														"\n\tmov\t\tr13, [rax]"
														"\n\tpush\tr13"
														"\n\tcall\twrite_sob"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trax, 0"
														"\n\tmov\t\trdi, ." (car fvar-rec) "_not_a_number_string2"
														"\n\tcall\tprintf"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n"

														"\n\tsection .data"
														"\n\t." (car fvar-rec) "_not_a_number_string1:"
														"\n\t\tdb \"Exception in zero?: \", 0 "
														"\n\t." (car fvar-rec) "_not_a_number_string2:"
														"\n\t\tdb \" is not a number\", 10, 0\n\n"
														; actual body


														"\n\tsection .text"
														"\n\t." (car fvar-rec) "_incorrect_arg_count:"
														"\n\tpush\trbx"
														"\n\tcall\tincorrect_number_of_arguments"
														"\n\tadd\t\trsp, 1*8"
														"\n\tmov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
														"\n\tjmp\t\trcx\n\n"
					

														"\n\t." (car fvar-rec) "_body_end:"
														"\n\tmov\t\trsp, rbp"
														"\n\tpop\t\trbp"
														"\n\tret\n\n"
					

														"\n\t" (car fvar-rec) "_handle_end:"
														;"\n\tmov\t\trax, [rax]"
														"\n\tmov\t\tqword [" (car fvar-rec) "], rax"
														"\n\t;================= zero? =================\n"))))
					(else
						(begin
							(set! primitives-string (string-append primitives-string ""))))))
			global-var-tab)))


;; ========================================================================================================================================
;; ========================================================================================================================================
;; ====================================================== PRIMITIVE FUNCTIONS WRITING =====================================================
;; ========================================================================================================================================
;; ========================================================================================================================================





;; ========================================================================================================================================
;; ========================================================================================================================================
;; =========================================================== Code Generation ============================================================
;; ========================================================================================================================================
;; ========================================================================================================================================

;; Description: Main code - gen function. Takes the list of asts and generates a code in assembly for every ast.
(define code-gen
	(lambda (asts)
		(for-each
			code-gen-for-single-ast-with-output
			asts)))


;; Description: Code - gen function for a single ast. Generates comment of start of code + code itself + result printout.
(define code-gen-for-single-ast-with-output
	(lambda (ast)
		(begin
			(set! astInd (+ astInd 1))
			(set! code-lambda-depth 0)
			(set! code-ident "")
			(set! code-string (string-append
								code-string
								"\n\t; ================== ast number " (number->string astInd) " start =================="
								"\n\tpush\tast_no_" (number->string astInd) "_end" 
								"\n\tpush\trbp"
								"\n\tpush\t0"
								"\n\tpush\t0"
								"\n\tmov\t\trbp, rsp\n"))

			(code-gen-for-single-ast ast)
			(set! code-string (string-append
								code-string
								"\n\n\t; AST RETURN VALUE PRINT"
								"\n\tmov\t\trax, [rax]"
								"\n\tpush\trax"
								"\n\tcall\twrite_sob_if_not_void"
								"\n\tadd\t\trsp, 1*8"
								"\n\t; AST RETURN VALUE PRINT\n"

								"\n\tast_no_" (number->string astInd) "_end:"
								"\n\tmov\t\trsp, rbp"
								"\n\tadd\t\trsp, 2*8"
								"\n\tpop\t\trbp"
								"\n\tmov\t\trsp, rbp"
								"\n\t; =================== ast number " (number->string astInd) " end ===================\n\n")))))


;; Description: Code - gen for a single ast without starting comment and ending printouts.
(define code-gen-for-single-ast
	(lambda (ast)
		(begin
			(let ((curr-ident code-ident))
				(begin
					(set! code-ident (string-append code-ident "\t\t"))
					(cond
						((eq? (car ast) 'const)
							(begin
								(set! code-constInd (+ code-constInd 1))
								(let ((current-constInd code-constInd))
									(begin
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; const_" (number->string current-constInd) " handling start"))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "mov\t\trax, " (cadr (car (filter (lambda (const) (equal? (cadr ast) (caddr const))) const-table)))))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; const_" (number->string current-constInd) " handling end\n"))))))
						((eq? (car ast) 'if3)
							(begin
								(set! code-ifInd (+ code-ifInd 1))
								(let ((current-ifInd code-ifInd))
									(begin
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; if3_" (number->string current-ifInd) " handling start"))
										(code-gen-for-single-ast (cadr ast)) ; code - gen for test case
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "mov\t\tr13, [rax]"
															"\n\t" curr-ident "cmp\t\tr13, SOB_FALSE"
															"\n\t" curr-ident "je\t\tif_" (number->string current-ifInd) "_else\n"))

										(code-gen-for-single-ast (caddr ast)) ; code - gen for then case
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "jmp\t\tif_" (number->string current-ifInd) "_end\n"

															"\n\t" curr-ident "if_" (number->string current-ifInd) "_else:"))
										(code-gen-for-single-ast (cadddr ast)) ; code - gen for else case
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "if_" (number->string current-ifInd) "_end:"))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; if3_" (number->string current-ifInd) " handling end\n"))))))
						((eq? (car ast) 'or)
							(begin
								(set! code-orInd (+ code-orInd 1))
								(let ((current-orInd code-orInd))
									(begin
										(set! code-string (string-append 
															code-string
															"\n\t" curr-ident "; or_" (number->string current-orInd) " handling start"))
										(for-each
											(lambda (or-sub-exp)
												(code-gen-for-single-ast or-sub-exp)
												(set! code-string (string-append
																	code-string
																	"\n\t" curr-ident "mov\t\tr13, [rax]"
																	"\n\t" curr-ident "cmp\t\tr13, SOB_FALSE"
																	"\n\t" curr-ident "jne\t\tor_" (number->string current-orInd) "_end")))
											(cadr ast))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "or_" (number->string current-orInd) "_end:"))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; or_" (number->string current-orInd) " handling end\n"))))))
						((eq? (car ast) 'seq)
							(begin
								(set! code-seqInd (+ code-seqInd 1))
								(let ((current-seqInd code-seqInd))
									(begin
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; seq_" (number->string current-seqInd) " handling start"))
										(for-each
											(lambda (seq-sub-exp)
												(code-gen-for-single-ast seq-sub-exp))
											(cadr ast))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; seq_" (number->string current-seqInd) " handling end\n"))))))
						((eq? (car ast) 'define)
							(begin
								(set! code-defineInd (+ code-defineInd 1))
								(let ((current-defineInd code-defineInd))
									(begin
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; define_" (number->string current-defineInd) "_" (car (car (filter (lambda (glob-var) (equal? (cadr (cadr ast)) (cadr glob-var))) global-var-table))) " handling start"))
										(code-gen-for-single-ast (caddr ast))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "mov\t\trbx, " (car (car (filter (lambda (glob-var) (equal? (cadr (cadr ast)) (cadr glob-var))) global-var-table)))
															"\n\t" curr-ident "mov\t\tr13, rax"
															"\n\t" curr-ident "mov\t\tqword [rbx], r13"
															"\n\t" curr-ident "mov\t\trax, sobVoid"))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; define_" (number->string current-defineInd) "_" (car (car (filter (lambda (glob-var) (equal? (cadr (cadr ast)) (cadr glob-var))) global-var-table))) " handling end\n"))))))
						((eq? (car ast) 'fvar)
							(begin
								(set! code-fvarInd (+ code-fvarInd 1))
								(let ((current-fvarInd code-fvarInd))
									(begin
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; fvar_" (number->string current-fvarInd) "_" (car (car (filter (lambda (glob-var) (equal? (cadr ast) (cadr glob-var))) global-var-table))) " handling start"))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "mov\t\trax, " (car (car (filter (lambda (glob-var) (equal? (cadr ast) (cadr glob-var))) global-var-table)))
															"\n\t" curr-ident "cmp\t\tqword [rax], T_UNDEFINED"
															"\n\t" curr-ident "jne\t\tfvar_" (number->string current-fvarInd) "_" (car (car (filter (lambda (glob-var) (equal? (cadr ast) (cadr glob-var))) global-var-table))) "_end\n"

															"\n\t" curr-ident "mov\t\trax, qword [rax]"
															"\n\t" curr-ident "push\trax"
															"\n\t" curr-ident "call\tvariable_is_not_bound"
															"\n\t" curr-ident "add\t\trsp, 1*8"
															"\n\t" curr-ident "mov\t\trcx, qword [rbp + 3*8] ; current ast end label"
															"\n\t" curr-ident "jmp\t\trcx\n"

															"\n\t" curr-ident "fvar_" (number->string current-fvarInd) "_" (car (car (filter (lambda (glob-var) (equal? (cadr ast) (cadr glob-var))) global-var-table))) "_end:"
															"\n\t" curr-ident "mov\t\trax, [rax]"))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; fvar_" (number->string current-fvarInd) "_" (car (car (filter (lambda (glob-var) (equal? (cadr ast) (cadr glob-var))) global-var-table))) " handling end\n"))))))
						((eq? (car ast) 'lambda-simple)
							(begin
								(set! code-lambda-simpleInd (+ code-lambda-simpleInd 1))
								(let ((current-lambda-simpleInd code-lambda-simpleInd))
									(begin
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; lambda-simple_" (number->string current-lambda-simpleInd) " at depth: " (number->string code-lambda-depth) " handling start"))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; lambda-simple_" (number->string current-lambda-simpleInd) " environment building start"
															"\n\t" curr-ident "lambda_simple_" (number->string current-lambda-simpleInd) "_env_build:"
															(if (= 0 code-lambda-depth)
																(string-append
																	"\n\t" curr-ident "mov\t\trbx, 0")
																(string-append
																	"\n\t" curr-ident "xor\t\trax, rax"
													 				"\n\t" curr-ident "mov\t\trdi, qword [rbp + 5*8] ; get the number of params (n)" 
													 				"\n\t" curr-ident "call\tmalloc"
													 				"\n\t" curr-ident "mov\t\trdx, rax ; rdx hold a pointer to store the params"
													 				"\n\t" curr-ident "push\trdx"
													 				"\n\t" curr-ident "xor\t\trax, rax"
													 				"\n\t" curr-ident "mov\t\trdi, " (number->string (* 8 (+ code-lambda-depth 1)))
													 				"\n\t" curr-ident "call\tmalloc"
													 				"\n\t" curr-ident "mov\t\trbx, rax ; rbx hold a pointer to store the new environment"
													 				"\n\t" curr-ident "push\trbx"
													 				"\n\t" curr-ident "pop\t\trbx ; pointer to the new env (m)"
													 				"\n\t" curr-ident "pop\t\trdx ; pointer to the params array (n)\n"
													 				
													 				"\n\t" curr-ident "mov\t\trcx, 0"
													 				"\n\t" curr-ident "lambda_simple_" (number->string current-lambda-simpleInd) "_copy_args_loop:"
														 				"\n\t\t" curr-ident "cmp\t\trcx, qword [rbp + 5*8] ; check if we are done copying the args in the lambda"
														 				"\n\t\t" curr-ident "je\t\tlambda_simple_" (number->string current-lambda-simpleInd) "_copy_args_loop_end"
														 				"\n\t\t" curr-ident "mov\t\tr9, qword [rbp + 8*(6 + rcx)]"
														 				"\n\t\t" curr-ident "mov\t\t[rdx + rcx*8], r9"
														 				"\n\t\t" curr-ident "inc\t\trcx"
														 				"\n\t\t" curr-ident "jmp\t\tlambda_simple_" (number->string current-lambda-simpleInd) "_copy_args_loop"
														 			"\n\t" curr-ident "lambda_simple_" (number->string current-lambda-simpleInd) "_copy_args_loop_end:\n"

													 				"\n\t" curr-ident "mov\t\t[rbx], rdx"
													 				"\n\t" curr-ident "mov\t\tr10, 0"
													 				"\n\t" curr-ident "mov\t\tr15, 1"
													 				"\n\t" curr-ident "lambda_simple_" (number->string current-lambda-simpleInd) "_copy_environment_loop:"
														 				"\n\t\t" curr-ident "cmp\t\tr10, " (number->string (- code-lambda-depth 1))
														 				"\n\t\t" curr-ident "je\t\tlambda_simple_" (number->string current-lambda-simpleInd) "_copy_environment_loop_end"
														 				"\n\t\t" curr-ident "mov\t\tr12, [rbp + 8*4] ; move to r12 the old env address"
														 				"\n\t\t" curr-ident "mov\t\tr12, [r12 + 8*r10] ; move to r12 the current (m) of the loop"
														 				"\n\t\t" curr-ident "mov\t\t[rbx + 8*r15], r12"
														 				"\n\t\t" curr-ident "inc\t\tr10"
														 				"\n\t\t" curr-ident "inc\t\tr15"
														 				"\n\t\t" curr-ident "jmp\t\tlambda_simple_" (number->string current-lambda-simpleInd) "_copy_environment_loop"
														 			"\n\t" curr-ident "lambda_simple_" (number->string current-lambda-simpleInd) "_copy_environment_loop_end:"
														 			"\n\t" curr-ident "mov\t\tqword [rbx + 8*r15], 0"))
															"\n\t" curr-ident "; lambda-simple_" (number->string current-lambda-simpleInd) " environment building end\n"

															"\n\t" curr-ident "; lambda-simple_" (number->string current-lambda-simpleInd) " closure building start"
															"\n\t" curr-ident "mov\t\trdi, 16"
															"\n\t" curr-ident "call\tmalloc ; rax now holds a pointer to the target closure"
															"\n\t" curr-ident "MAKE_LITERAL_CLOSURE rax, rbx, lambda_simple_" (number->string current-lambda-simpleInd) "_body"
							 								"\n\t" curr-ident "jmp\t\tlambda_simple_" (number->string current-lambda-simpleInd) "_handle_end"
							 								"\n\t" curr-ident "; lambda-simple_" (number->string current-lambda-simpleInd) " closure building end\n"

							 								"\n\t" curr-ident "; lambda-simple_" (number->string current-lambda-simpleInd) " body start"
							 								"\n\t" curr-ident "lambda_simple_" (number->string current-lambda-simpleInd) "_body:"
											 				"\n\t" curr-ident "push\trbp"
											 				"\n\t" curr-ident "mov\t\trbp, rsp\n"

											 				"\n\t" curr-ident "cmp\t\tqword [rbp + 5*8], " (number->string (length (cadr ast)))
											 				"\n\t" curr-ident "jne\t\tlambda_simple_incorrect_arg_count_" (number->string current-lambda-simpleInd) "\n" ))

										(set! code-lambda-depth (+ code-lambda-depth 1))
										(code-gen-for-single-ast (caddr ast)) ;; code-gen for the body
										(set! code-lambda-depth (- code-lambda-depth 1))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "jmp\t\tlambda_simple_" (number->string current-lambda-simpleInd) "_body_end\n"
															
															"\n\t" curr-ident "lambda_simple_incorrect_arg_count_" (number->string current-lambda-simpleInd) ":"
															"\n\t" curr-ident "push\trbx"
															"\n\t" curr-ident "call\tincorrect_number_of_arguments"
															"\n\t" curr-ident "add\t\trsp, 1*8"
															"\n\t" curr-ident "mov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
															"\n\t" curr-ident "jmp\t\trcx\n"

															"\n\t" curr-ident "lambda_simple_" (number->string current-lambda-simpleInd) "_body_end:"
															"\n\t" curr-ident "mov\t\trsp, rbp"
															"\n\t" curr-ident "pop\t\trbp"
															"\n\t" curr-ident "ret"
															"\n\t" curr-ident "; lambda-simple_" (number->string current-lambda-simpleInd) " body end\n"

															"\n\t" curr-ident "lambda_simple_" (number->string current-lambda-simpleInd) "_handle_end:"))
										(set! code-string (string-append 
															code-string
															"\n\t" curr-ident "; lambda-simple_" (number->string current-lambda-simpleInd) " at depth: " (number->string code-lambda-depth) " handling end\n"))))))
						((eq? (car ast) 'lambda-opt)
							(begin
								(set! code-lambda-optInd (+ code-lambda-optInd 1))
								(let ((current-lambda-optInd code-lambda-optInd))
									(begin
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; lambda_opt_" (number->string current-lambda-optInd) " at depth: " (number->string code-lambda-depth) " handling start"))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; lambda_opt_" (number->string current-lambda-optInd) " environment building start"
															"\n\t" curr-ident "lambda_opt_" (number->string current-lambda-optInd) "_env_build:"
															(if (= 0 code-lambda-depth)
																(string-append
																	"\n\t" curr-ident "mov\t\trbx, 0")
																(string-append
																	"\n\t" curr-ident "xor\t\trax, rax"
													 				"\n\t" curr-ident "mov\t\trdi, qword [rbp + 5*8] ; get the number of params (n)" 
													 				"\n\t" curr-ident "call\tmalloc"
													 				"\n\t" curr-ident "mov\t\trdx, rax ; rdx hold a pointer to store the params"
													 				"\n\t" curr-ident "push\trdx"
													 				"\n\t" curr-ident "xor\t\trax, rax"
													 				"\n\t" curr-ident "mov\t\trdi, " (number->string (* 8 (+ code-lambda-depth 1)))
													 				"\n\t" curr-ident "call\tmalloc"
													 				"\n\t" curr-ident "mov\t\trbx, rax ; rbx hold a pointer to store the new environment"
													 				"\n\t" curr-ident "push\trbx"
													 				"\n\t" curr-ident "pop\t\trbx ; pointer to the new env (m)"
													 				"\n\t" curr-ident "pop\t\trdx ; pointer to the params array (n)\n"
													 				
													 				"\n\t" curr-ident "mov\t\trcx, 0"
													 				"\n\t" curr-ident "lambda_opt_" (number->string current-lambda-optInd) "_copy_args_loop:"
														 				"\n\t\t" curr-ident "cmp\t\trcx, qword [rbp + 5*8] ; check if we are done copying the args in the lambda"
														 				"\n\t\t" curr-ident "je\t\tlambda_opt_" (number->string current-lambda-optInd) "_copy_args_loop_end"
														 				"\n\t\t" curr-ident "mov\t\tr9, qword [rbp + 8*(6 + rcx)]"
														 				"\n\t\t" curr-ident "mov\t\t[rdx + rcx*8], r9"
														 				"\n\t\t" curr-ident "inc\t\trcx"
														 				"\n\t\t" curr-ident "jmp\t\tlambda_opt_" (number->string current-lambda-optInd) "_copy_args_loop"
														 			"\n\t" curr-ident "lambda_opt_" (number->string current-lambda-optInd) "_copy_args_loop_end:\n"

													 				"\n\t" curr-ident "mov\t\t[rbx], rdx"
													 				"\n\t" curr-ident "mov\t\tr10, 0"
													 				"\n\t" curr-ident "mov\t\tr15, 1"
													 				"\n\t" curr-ident "lambda_opt_" (number->string current-lambda-optInd) "_copy_environment_loop:"
														 				"\n\t\t" curr-ident "cmp\t\tr10, " (number->string (- code-lambda-depth 1))
														 				"\n\t\t" curr-ident "je\t\tlambda_opt_" (number->string current-lambda-optInd) "_copy_environment_loop_end"
														 				"\n\t\t" curr-ident "mov\t\tr12, [rbp + 8*4] ; move to r12 the old env address"
														 				"\n\t\t" curr-ident "mov\t\tr12, [r12 + 8*r10] ; move to r12 the current (m) of the loop"
														 				"\n\t\t" curr-ident "mov\t\t[rbx + 8*r15], r12"
														 				"\n\t\t" curr-ident "inc\t\tr10"
														 				"\n\t\t" curr-ident "inc\t\tr15"
														 				"\n\t\t" curr-ident "jmp\t\tlambda_opt_" (number->string current-lambda-optInd) "_copy_environment_loop"
														 			"\n\t" curr-ident "lambda_opt_" (number->string current-lambda-optInd) "_copy_environment_loop_end:"
														 			"\n\t" curr-ident "mov\t\tqword [rbx + 8*r15], 0"))
															"\n\t" curr-ident "; lambda-opt_" (number->string current-lambda-optInd) " environment building end\n"

															"\n\t" curr-ident "; lambda-opt_" (number->string current-lambda-optInd) " closure building start"
															"\n\t" curr-ident "mov\t\trdi, 16"
															"\n\t" curr-ident "call\tmalloc ; rax now holds a pointer to the target closure"
															"\n\t" curr-ident "MAKE_LITERAL_CLOSURE rax, rbx, lambda_opt_" (number->string current-lambda-optInd) "_body"
							 								"\n\t" curr-ident "jmp\t\tlambda_opt_" (number->string current-lambda-optInd) "_handle_end"
							 								"\n\t" curr-ident "; lambda-opt_" (number->string current-lambda-optInd) " closure building end\n"

							 								"\n\t" curr-ident "; lambda-opt_" (number->string current-lambda-optInd) " body start"
							 								"\n\t" curr-ident "lambda_opt_" (number->string current-lambda-optInd) "_body:"
											 				"\n\t" curr-ident "push\trbp"
											 				"\n\t" curr-ident "mov\t\trbp, rsp\n"

											 				"\n\t" curr-ident "cmp\t\tqword [rbp + 5*8], " (number->string (length (cadr ast)))
											 				"\n\t" curr-ident "jl\t\tlambda_opt_incorrect_arg_count_" (number->string current-lambda-optInd) "\n" 
											 				
											 				"\n\t" curr-ident "; optional arguments assembly to list start"
											 				"\n\t" curr-ident "mov\t\tr15, " (number->string (- (length (cadr ast)) 1)) " ; r15 holds the position of the last mandatory argument"
											 				"\n\t" curr-ident "mov\t\tr14, qword [rbp + 5*8]"
											 				"\n\t" curr-ident "dec\t\tr14 ; r14 holds the position of the last optional argument"
											 				"\n\t" curr-ident "mov\t\tr13, sobNil ; r13 will be workhorse for holding the list. currently holding '()\n\n"


											 				"\n\t" curr-ident "lambda_opt_" (number->string current-lambda-optInd) "_optional_args_assembly_to_list_loop:"
											 				"\n\t" curr-ident "cmp\t\tr14, r15"
											 				"\n\t" curr-ident "je\t\tlambda_opt_" (number->string current-lambda-optInd) "_optional_args_assembly_to_list_loop_end\n"

											 				"\n\t" curr-ident "mov\t\trdi, 8"
											 				"\n\t" curr-ident "call\tmalloc"
											 				"\n\t" curr-ident "mov\t\tr8, qword [rbp + 6*8 + r14*8] ; r8 holds the address of the object that is to be the next car"
											 				"\n\t" curr-ident "; make new pair: [rax]/r12, r8, r13"
											 				"\n\t" curr-ident "mov\t\tr12, r8"
											 				"\n\t" curr-ident "sub\t\tr12, start_of_data"
											 				"\n\t" curr-ident "shl\t\tr12, 30"
											 				"\n\t" curr-ident "mov\t\tr9, r13"
											 				"\n\t" curr-ident "sub\t\tr9, start_of_data"
											 				"\n\t" curr-ident "or\t\tr12, r9"
											 				"\n\t" curr-ident "shl\t\tr12, 4"
											 				"\n\t" curr-ident "or\t\tr12, T_PAIR ; at this stage r12 holds the object of the new list"
											 				"\n\t" curr-ident "mov\t\t qword [rax], r12"
											 				"\n\t" curr-ident "mov\t\tr13, rax"
											 				"\n\t" curr-ident "dec\t\tr14"
											 				"\n\t" curr-ident "jmp\t\tlambda_opt_" (number->string current-lambda-optInd) "_optional_args_assembly_to_list_loop\n"

											 				"\n\t" curr-ident "lambda_opt_" (number->string current-lambda-optInd) "_optional_args_assembly_to_list_loop_end:"
											 				"\n\t" curr-ident "mov\t\tqword [rbp + 6*8 + (r15 + 1)*8], r13"
											 				"\n\t" curr-ident "; optional arguments assembly to list end\n\n"))


										(set! code-lambda-depth (+ code-lambda-depth 1))
										(code-gen-for-single-ast (cadddr ast)) ;; code-gen for the body
										(set! code-lambda-depth (- code-lambda-depth 1))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "jmp\t\tlambda_opt_" (number->string current-lambda-optInd) "_body_end\n\n"

															
															"\n\t" curr-ident "lambda_opt_incorrect_arg_count_" (number->string current-lambda-optInd) ":"
															"\n\t" curr-ident "push\trbx"
															"\n\t" curr-ident "call\tincorrect_number_of_arguments"
															"\n\t" curr-ident "add\t\trsp, 1*8"
															"\n\t" curr-ident "mov\t\trcx, qword [rbp + 3*8] ; curent ast end label"
															"\n\t" curr-ident "jmp\t\trcx\n\n"


															"\n\t" curr-ident "lambda_opt_" (number->string current-lambda-optInd) "_body_end:"
															"\n\t" curr-ident "mov\t\trsp, rbp"
															"\n\t" curr-ident "pop\t\trbp"
															"\n\t" curr-ident "ret"
															"\n\t" curr-ident "; lambda-opt_" (number->string current-lambda-optInd) " body end\n\n"


															"\n\t" curr-ident "lambda_opt_" (number->string current-lambda-optInd) "_handle_end:"))
										(set! code-string (string-append 
															code-string
															"\n\t" curr-ident "; lambda-opt_" (number->string current-lambda-optInd) " at depth: " (number->string code-lambda-depth) " handling end\n"))))))
						((or (eq? (car ast) 'applic) (eq? (car ast) 'tc-applic)) ; TODO: get rid of this once tc-applic is done.
						;((eq? (car ast) 'applic)								 ; TODO: get back to this once tc-applic is done.
							(begin
								(set! code-applicInd (+ code-applicInd 1))
								(let ((current-code-applicInd code-applicInd))
									(begin
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; applic_" (number->string current-code-applicInd) " handling start"
															"\n\t" curr-ident "; applic_" (number->string current-code-applicInd) " args pushing start"
															"\n\t" curr-ident "mov\t\trax, sobNil"
															"\n\t" curr-ident "push\trax ; pushing nil right before the last argument to support lambda-opt\n"))
										(for-each
											(lambda (arg)
												(begin
													(code-gen-for-single-ast arg)
													(set! code-string (string-append
																		code-string
																		"\t" curr-ident "push\trax\n"))))
											(reverse (caddr ast)))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "push\t" (number->string (length (caddr ast))) " ; pushing argument count"
															"\n\t" curr-ident "; applic_" (number->string current-code-applicInd) " args pushing end\n"))

										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; applic_" (number->string current-code-applicInd) " operator evaluation start"))
										(code-gen-for-single-ast (cadr ast)) ;; code-gen for the operator
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; rax now has the operator address"
															"\n\t" curr-ident "; applic_" (number->string current-code-applicInd) " operator evaluation end\n"))

										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; applic_" (number->string current-code-applicInd) " calling start"
															"\n\t" curr-ident "mov\t\tr13, [rax]"
															"\n\t" curr-ident "mov\t\trcx, r13"
															"\n\t" curr-ident "TYPE\trcx"
															"\n\t" curr-ident "cmp\t\trcx, T_CLOSURE"
															"\n\t" curr-ident "jne\t\tnot_a_closure_" (number->string current-code-applicInd) "\n\n"


															"\n\t" curr-ident "mov\t\trcx, r13"
															"\n\t" curr-ident "CLOSURE_ENV rcx"
															"\n\t" curr-ident "push\trcx ; pushing environment"
															"\n\t" curr-ident "mov\t\trcx, qword [rbp + 3*8]"
															"\n\t" curr-ident "push\trcx ; pushing current ast end label"
															"\n\t" curr-ident "mov\t\trcx, qword [rbp + 2*8]"
															"\n\t" curr-ident "push\trcx ; pushing current ast starting base pointer"
															"\n\t" curr-ident "mov\t\trbx, r13"
															"\n\t" curr-ident "CLOSURE_CODE r13"
															"\n\t" curr-ident "call\tr13"
															"\n\t" curr-ident "add\t\trsp, 2*8"
															"\n\t" curr-ident "mov\t\tr13, qword [rsp + 1*8] ; putting into r13 the arg count"
															"\n\t" curr-ident "shl\t\tr13, 3"
															"\n\t" curr-ident "add\t\tr13, 3*8"
															"\n\t" curr-ident "add\t\trsp, r13"
															"\n\t" curr-ident "jmp\t\tapplic_" (number->string current-code-applicInd) "_finish\n\n"

															
															"\n\t" curr-ident "not_a_closure_" (number->string current-code-applicInd) ":"
															"\n\t" curr-ident "push\tr13"
															"\n\t" curr-ident "call\tattempt_to_apply_non_procedure"
															"\n\t" curr-ident "add\t\trsp, 1*8"
															"\n\t" curr-ident "mov\t\trcx, qword [rbp + 3*8] ; curent ast end label (TOP of the ast)"
															"\n\t" curr-ident "jmp\t\trcx\n\n"


															"\n\t" curr-ident "applic_" (number->string current-code-applicInd) "_finish:"
															"\n\t" curr-ident "; applic_" (number->string current-code-applicInd) " calling end\n\n"))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; applic_" (number->string current-code-applicInd) " handling end\n\n"))))))
						((eq? (car ast) 'pvar)
							(begin
								(set! code-pvarInd (+ code-pvarInd 1))
								(let ((current-pvarInd code-pvarInd))
									(begin
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; pvar_" (number->string current-pvarInd) " handling start"))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "mov\t\trax, qword [rbp + (6+" (number->string (caddr ast)) ")*8]"))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; pvar_" (number->string current-pvarInd) " handling end\n"))))))
						((eq? (car ast) 'bvar)
							(begin
								(set! code-bvarInd (+ code-bvarInd 1))
								(let ((current-bvarInd code-bvarInd))
									(begin
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; bvar_" (number->string current-bvarInd) " handling start"))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "mov\t\trax, qword [rbp + 4*8]"
															"\n\t" curr-ident "mov\t\trax, qword [rax + "(number->string (caddr ast)) "*8]"
															"\n\t" curr-ident "mov\t\trax, qword [rax + "(number->string (cadddr ast)) "*8]"))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; bvar_" (number->string current-bvarInd) " handling end\n"))))))
						((eq? (car ast) 'set)
							(begin
								;(let (())
								;	(begin
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; set start"))
										(code-gen-for-single-ast (caddr ast))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "mov\t\tr13, rax"))
										(if (eq? (car (cadr ast)) 'fvar)
											(set! code-string (string-append
															code-string
															"\n\t" curr-ident "mov\t\trax, " (car (car (filter (lambda (glob-var) (equal? (cadr (cadr ast)) (cadr glob-var))) global-var-table)))))
											(code-gen-for-single-ast (cadr ast)))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "mov\t\tqword [rax], r13"
															"\n\t" curr-ident "mov\t\trax, sobVoid"))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; set end"))))
						((eq? (car ast) 'box)
							(begin
								;(let (())
								;	(begin
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; box start"))
										(code-gen-for-single-ast (cadr ast))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "mov\t\tr13, rax"
															"\n\t" curr-ident "mov\t\trdi, 8"
															"\n\t" curr-ident "call\tmalloc"
															"\n\t" curr-ident "mov\t\tqword [rax], r13"))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; box end"))))
						((eq? (car ast) 'box-set)
							(begin
								;(let (())
								;	(begin
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; box-set start"))
										(code-gen-for-single-ast (caddr ast))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "mov\t\tr13, rax"))
										(code-gen-for-single-ast (cadr ast))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "mov\t\tqword [rax], r13"
															"\n\t" curr-ident "mov\t\trax, sobVoid"))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; box-set end"))))
						((eq? (car ast) 'box-get)
							(begin
								;(let (())
								;	(begin
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; box-get start"))
										(code-gen-for-single-ast (cadr ast))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "mov\t\trax, [rax]"))
										(set! code-string (string-append
															code-string
															"\n\t" curr-ident "; box-get end"))))
						(else 
							(set! code-string (string-append code-string ""))))
					(set! code-ident (substring code-ident 0 (- (string-length code-ident) 2)))
					)))))

;; ========================================================================================================================================
;; ========================================================================================================================================
;; =========================================================== Code Generation ============================================================
;; ========================================================================================================================================
;; ========================================================================================================================================





;; ========================================================================================================================================
;; ========================================================================================================================================
;; ======================================================= MAIN COMPILING FUNCTIOON =======================================================
;; ========================================================================================================================================
;; ========================================================================================================================================

;;; Usage: (compile-scheme-file "gopnik.scm" "gopnik.s")
;;; Description: Main compiler function. recives input scheme file, and generates code for output assembly file.
(define compile-scheme-file
	(lambda (source target)
		(begin
			;; Initialization of set! vars
			(reset-utility-set-vars!)
			;; Initialization of set! vars

			;; Pipeline
			(set! asts (pipeline (append (file->list "project/append_list_map.scm") (file->list source))))
			;; Pipeline

			;; Constant table scheme representation construction
			(make-const-list asts)
			(make-const-table const-list)
			;; Constant table scheme representation construction

			;; Global variables table scheme representation construction
			(make-global-var-list asts)
			(make-global-var-table global-var-list)
			;; Global variables table scheme representation construction

			;; Constant table string construction
			(make-const-table-string const-table)
			;; Constant table string construction

			;; Global variables table string representation construction
			(make-global-var-table-string global-var-table)
			;; Global variables table string representation construction

			;; Symbol table init string construction
			(make-symbol-table-init-string const-table)
			;; Symbol table init string construction

			;; Primitive functions string construction
			(make-primitive-functions-string global-var-table)
			;; Primitive functions string construction

			;; The code string construction
			(code-gen asts)
			;; The code string construction

			;; First asembly lines
			(set! complete-assembly-string (string-append complete-assembly-string "%include \"project/scheme.s\"\n\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "section .bss\n\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "section .data\n\n"))
			;; First asembly lines

			;; Constant table assembly lines
			(set! complete-assembly-string (string-append complete-assembly-string "; ============================================================================================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "; =========================================== Constant table start ===========================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "; ============================================================================================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "constant_table:\n\n"))
			(set! complete-assembly-string (string-append complete-assembly-string const-table-string))
			(set! complete-assembly-string (string-append complete-assembly-string "; ============================================================================================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "; ============================================ Constant table end ============================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "; ============================================================================================================\n\n\n"))
			;; Constant table assembly lines

			;; Global vars table assembly lines
			(set! complete-assembly-string (string-append complete-assembly-string "; ============================================================================================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "; ========================================== Global vars table start =========================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "; ============================================================================================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "global_vars_table:\n\n"))
			(set! complete-assembly-string (string-append complete-assembly-string global-var-table-string))
			(set! complete-assembly-string (string-append complete-assembly-string "; ============================================================================================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "; =========================================== Global vars table end ==========================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "; ============================================================================================================\n\n\n"))
			;; Global vars table assembly lines

			;; Symbol table assembly lines
			(set! complete-assembly-string (string-append complete-assembly-string "; ============================================================================================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "; ============================================= Symbol table start ===========================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "; ============================================================================================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "symbol_table:\n\tdq SOB_NIL\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "; ============================================================================================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "; ============================================== Symbol table end ============================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "; ============================================================================================================\n"))
			;; Symbol table assembly lines

			;; The prologue
			(set! complete-assembly-string (string-append complete-assembly-string "\n\nsection .text\n\n\textern exit, printf, scanf, malloc\n\tglobal main"))
			(set! complete-assembly-string (string-append complete-assembly-string "\n\nmain:\n\tpush\trbp\n\tmov\t\trbp, rsp\n\n"))
			;; The prologue

			;; The symbol table init string
			(set! complete-assembly-string (string-append complete-assembly-string "\t; ========================================================================================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "\t; ======================================== Symbol table init start =======================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "\t; ========================================================================================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string symbol-table-init-string))
			(set! complete-assembly-string (string-append complete-assembly-string "\n\t; ========================================================================================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "\t; ========================================= Symbol table init end ========================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "\t; ========================================================================================================\n\n"))
			;; The symbol table init string

			;; The primitive functions string
			(set! complete-assembly-string (string-append complete-assembly-string "\t; ========================================================================================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "\t; ======================================= Primitive functions start ======================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "\t; ========================================================================================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string primitives-string))
			(set! complete-assembly-string (string-append complete-assembly-string "\n\t; ========================================================================================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "\t; ======================================= Primitive functions end ========================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "\t; ========================================================================================================\n\n"))
			;; The primitive functions string

			;; The code gen string
			(set! complete-assembly-string (string-append complete-assembly-string "\t; ========================================================================================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "\t; ============================================== Code start ==============================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "\t; ========================================================================================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string code-string))
			(set! complete-assembly-string (string-append complete-assembly-string "\t; ========================================================================================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "\t; =============================================== Code end ===============================================\n"))
			(set! complete-assembly-string (string-append complete-assembly-string "\t; ========================================================================================================\n"))
			;; The code gen string

			;; The epilogue
			(set! complete-assembly-string (string-append
												complete-assembly-string
												"\n\tmov\t\trsp, rbp"
												"\n\tpop\t\trbp"
												"\n\tret"))
			
			(set! complete-assembly-string (string-append
												complete-assembly-string
												"\n\nsection .text"
												"\nincorrect_number_of_arguments:"
												"\n\tpush\trbp"
												"\n\tmov\t\trbp, rsp"

												"\n\tmov\t\trax, 0"
												"\n\tmov\t\trdi, .incorrect_number_of_arguments_string"
												"\n\tcall\tprintf"

												"\n\tmov\t\trax, qword [rbp + 8 + 1*8]"
												"\n\tpush\trax"
												"\n\tcall\twrite_sob_if_not_void"
												"\n\tadd\t\trsp, 1*8"

												"\n\tleave"
												"\n\tret"

												"\n\nsection .data"
												"\n.incorrect_number_of_arguments_string:"
												"\n\tdb \"Exception: incorrect argument count in call \", 0"))
			(set! complete-assembly-string (string-append
												complete-assembly-string
												"\n\nsection .text"
												"\nattempt_to_apply_non_procedure:"
												"\n\tpush\trbp"
												"\n\tmov\t\trbp, rsp"

												"\n\tmov\t\trax, 0"
												"\n\tmov\t\trdi, .attempt_to_apply_non_procedure_string"
												"\n\tcall\tprintf"

												"\n\tmov\t\trax, qword [rbp + 8 + 1*8]"
												"\n\tpush\trax"
												"\n\tcall\twrite_sob_if_not_void"
												"\n\tadd\t\trsp, 1*8"

												"\n\tleave"
												"\n\tret"

												"\n\nsection .data"
												"\n.attempt_to_apply_non_procedure_string:"
												"\n\tdb \"Exception: attempt to apply non-procedure \", 0"))
			(set! complete-assembly-string (string-append ;; TODO: update this function once we know how to pass the variable name
												complete-assembly-string
												"\n\tsection .text"
												"\n\nvariable_is_not_bound:"
												"\n\tpush\trbp"
												"\n\tmov\t\trbp, rsp"

												"\n\tmov\t\trax, 0"
												"\n\tmov\t\trdi, .variable_is_not_bound_string"
												"\n\tcall\tprintf"

												"\n\tmov\t\trax, qword [rbp + 8 + 1*8]"
												"\n\tpush\trax"
												"\n\tcall\twrite_sob_if_not_void"
												"\n\tadd\t\trsp, 1*8"

												"\n\tleave"
												"\n\tret"

												"\nsection .data"
												"\n.variable_is_not_bound_string:"
												"\n\tdb \"Attempt to use an unbounded variable \", 0"))
			;; The epilogue


			(let ((output-port (open-output-file target 'replace)))
				(fprintf output-port complete-assembly-string)
				(close-output-port output-port)))))


;; ========================================================================================================================================
;; ========================================================================================================================================
;; ======================================================= MAIN COMPILING FUNCTIOON =======================================================
;; ========================================================================================================================================
;; ========================================================================================================================================