;;; Constants
5
-5
1/3
;-2/4
;#t
;"\""
;#\"
;#f
;#\k
;#\x0
;#\x01
;#\newline
;'()
;"av"
;"a"
;"at\nme"
;'(1 . 2)
;'(1 . (2 3))
;'#(1 "at" 2 4 "ag")
;'#(1 "at" 2 4 "ag" #(1 2 69) #(1 2 69))
;'(1 2 3 (4 (5 6 7) 8 9))
;'(4 5 6 . #(4 8 16 (#\a #\b #\c)))
;'(t 5 6 . #(4 8 16 (1 2 3)))
;'a
;'-
;'#()
;'#(1)
;;; Constants


;; primitives tests
;append
;(append) ; ()
;(append 1 2 3) ; Exception. (not conforming to scheme since append is generated via code-gen)
;(append 1 '(2)) ; Exception. (not conforming to scheme since append is generated via code-gen)
;(append '(1 2 3) '(4 5 (6 7) 8 9)) ; (1 2 3 4 5 (6 7) 8 9)
;((lambda (a b c d e f) (append)) 1 2 3 '(2) '(1 2 3) '(4 5 (6 7) 8 9)) ; ()
;((lambda (a b c d e f) (append a b c)) 1 2 3 '(2) '(1 2 3) '(4 5 (6 7) 8 9)) ; Exception. (not conforming to scheme since append is generated via code-gen)
;((lambda (a b c d e f) (append a d)) 1 2 3 '(2) '(1 2 3) '(4 5 (6 7) 8 9)) ; Exception. (not conforming to scheme since append is generated via code-gen)
;((lambda (a b c d e f) (append e f)) 1 2 3 '(2) '(1 2 3) '(4 5 (6 7) 8 9)) ; (1 2 3 4 5 (6 7) 8 9)

;apply ; procedure
;(apply) ; Exception: incorrect argument count in call (apply)
;(apply +) ; Exception: incorrect argument count in call (apply +)
;(apply + 4) ; Exception in apply: 4 is not a proper list
;(apply + '(1 2 3)) ; 6
;(apply + '(4)) ; 4
;(apply 1 '(4)) ; Exception: attempt to apply non-procedure 1
;(apply + 1 '(4)) ; 5
;(apply + 1 2 '(3 4 5)) ; 15
;(apply + 1 2 3 4 '(5 6 7 8 9 10)) ; 55
;((lambda (a b c d e f g) (apply)) + 4 '(1 2 3) '(4) 1 2 '(3 4 5)) ; Exception: incorrect argument count in call (apply)
;((lambda (a b c d e f g) (apply a)) + 4 '(1 2 3) '(4) 1 2 '(3 4 5)) ; Exception: incorrect argument count in call (apply +)
;((lambda (a b c d e f g) (apply a b)) + 4 '(1 2 3) '(4) 1 2 '(3 4 5)) ; Exception in apply: 4 is not a proper list
;((lambda (a b c d e f g) (apply a c)) + 4 '(1 2 3) '(4) 1 2 '(3 4 5)) ; 6
;((lambda (a b c d e f g) (apply a d)) + 4 '(1 2 3) '(4) 1 2 '(3 4 5)) ; 4
;((lambda (a b c d e f g) (apply e d)) + 4 '(1 2 3) '(4) 1 2 '(3 4 5)) ; Exception: attempt to apply non-procedure 1
;((lambda (a b c d e f g) (apply a e d)) + 4 '(1 2 3) '(4) 1 2 '(3 4 5)) ; 5
;((lambda (a b c d e f g) (apply a e f g)) + 4 '(1 2 3) '(4) 1 2 '(3 4 5)) ; 15

;< ; closure
;(<) ; Exception: incorrect argument count in call (<)
;(< 1 2) ; #t
;(< 1 2 3 4) ; #t
;(< 2 2 2 2) ; #f
;(< 1 2 "three" 4) ; Exception in <: "three" is not a number
;((lambda (w x y z) (<)) 1 2 3 4) ; Exception: incorrect argument count in call (<)
;((lambda (w x y z) (< w x)) 1 2 3 4) ; #t
;((lambda (w x y z) (< w x y z)) 1 2 3 4) ; #t
;((lambda (w x y z) (< x x x x)) 1 2 3 4) ; #f
;((lambda (w x y z) (< w x y z)) 1 2 "three" 4) ; Exception in <: "three" is not a number
;(< 6) ; #t
;(< 1/7) ; #t
;(< -4/8) ; #t
;(< -5) ; #t
;(< 1/2 -1/4) ; #f
;(< 1 1/2) ; #f
;(< 1 2 3 4 0 6) ; #f
;(< 1/2 2/2 0/2 4/2) ; #f
;(< 1/2 4/8) ; #f
;(< -2/3 -6/9) ; #f

;= ; closure
;(=) ; Exception: incorrect argument count in call (=)
;(= 1 2) ; #f
;(= 1 2 3 4) ; #f
;(= 2 2 2 2) ; #t
;(= 1 2 "three" 4) ; Exception in =: "three" is not a number
;((lambda (w x y z) (=)) 1 2 3 4) ; Exception: incorrect argument count in call (=)
;((lambda (w x y z) (= w x)) 1 2 3 4) ; #f
;((lambda (w x y z) (= w x y z)) 1 2 3 4) ; #f
;((lambda (w x y z) (= x x x x)) 1 2 3 4) ; #t
;((lambda (w x y z) (= w x y z)) 1 2 "three" 4) ; Exception in =: "three" is not a number
;(= 6) ; #t
;(= 1/7) ; #t
;(= -4/8) ; #t
;(= -5) ; #t
;(= 1/2 -1/4) ; #f
;(= 1 1/2) ; #f
;(= 1 2 3 4 0 6) ; #f
;(= 1/2 2/2 0/2 4/2) ; #f
;(= 1/2 4/8) ; #t
;(= -2/3 -6/9) ; #t

;> ; closure
;(>) ; Exception: incorrect argument count in call (>)
;(> 2 1) ; #t
;(> 4 3 2 1) ; #t
;(> 2 2 2 2) ; #f
;(> 1 2 "three" 4) ; Exception in >: "three" is not a number
;((lambda (w x y z) (>)) 1 2 3 4) ; Exception: incorrect argument count in call (>)
;((lambda (w x y z) (> x w)) 1 2 3 4) ; #t
;((lambda (w x y z) (> z y x w)) 1 2 3 4) ; #t
;((lambda (w x y z) (> x x x x)) 1 2 3 4) ; #f
;((lambda (w x y z) (> w x y z)) 1 2 "three" 4) ; Exception in >: "three" is not a number
;(> 6) ; #t
;(> 1/7) ; #t
;(> -4/8) ; #t
;(> -5) ; #t
;(> 1/2 -1/4) ; #t
;(> 1 1/2) ; #t
;(> 1 2 3 4 0 6) ; #f
;(> 1/2 2/2 0/2 4/2) ; #f
;(> 1/2 4/8) ; #f
;(> -2/3 -6/9) ; #f

;+ ; closure
;(+) ; 0
;(+ 1 2) ; 3
;(+ 1 2 3 4) ; 10
;(+ 1 2 "three" 4) ; exception not a number
;((lambda (w x y z) (+)) 1 2 3 4) ; 0
;((lambda (w x y z) (+ w x)) 1 2 3 4) ; 3
;((lambda (w x y z) (+ w x y z)) 1 2 3 4) ; 10
;((lambda (w x y z) (+ w x y z)) 1 2 "three" 4) ; exception not a number
;(+ 1/2 1/3) ; 5/6
;(+ 1/7) ; 1/7
;(+ -4/8) ; -1/2
;(+ 1/2 -1/4) ; 1/4
;(+ 1 1/2) ; 3/2
;(+ 1/2 2/2 3/2 4/2) ; 5

;/ ; closure
;(/) ; Exception: incorrect argument count in call (/)
;(/ 1 2) ; 1/2 
;(/ 1 2 3 4) ; 1/24
;(/ 1 2 "three" 4) ; Exception in /: "three" is not a number
;((lambda (w x y z) (/)) 1 2 3 4) ; Exception: incorrect argument count in call (/)
;((lambda (w x y z) (/ w x)) 1 2 3 4) ; 1/2
;((lambda (w x y z) (/ w x y z)) 1 2 3 4) ; 1/24
;((lambda (w x y z) (/ w x y z)) 1 2 "three" 4) ; Exception in /: "three" is not a number
;(/ 1/2 1/3) ; 3/2
;(/ 1/7) ; 7
;(/ -4/8) ; -2
;(/ 6) ;1/6
;(/ 1/2 -1/4) ; -2
;(/ 1 1/2) ; 2
;(/ 1/2 2/2 3/2 4/2) ; 1/6
;(/ 1 2 3 4 0 6) ; Exception in /: undefined for 0
;(/ 1/2 2/2 0/2 4/2) ; Exception in /: undefined for 0

;* ; closure
;(*) ; 1
;(* 1 2) ; 2 
;(* 1 2 3 4) ; 24
;(* 1 2 "three" 4) ; Exception in *: "three" is not a number
;((lambda (w x y z) (*)) 1 2 3 4) ; 1
;((lambda (w x y z) (* w x)) 1 2 3 4) ; 2
;((lambda (w x y z) (* w x y z)) 1 2 3 4) ; 24
;((lambda (w x y z) (* w x y z)) 1 2 "three" 4) ; Exception in *: "three" is not a number
;(* 1/2 1/3) ; 1/6
;(* 1/7) ; 1/7
;(* -4/8) ; -1/2
;(* 1/2 -1/4) ; -1/8
;(* 1 1/2) ; 1/2
;(* 1/2 2/2 3/2 4/2) ; 3/2
;(* 1 2 3 4 0 6) ; 0
;(* 1/2 2/2 0/2 4/2) ; 0

;- ; closure
;(-) ; Exception: incorrect argument count in call
;(- 1 2) ; -1
;(- 1) ; -1
;(- 1 2 3 4) ; -8
;(- 1 2 "three" 4) ; Exception in -: "three" is not a number
;((lambda (w x y z) (-)) 1 2 3 4) ; Exception: incorrect argument count in call
;((lambda (w x y z) (- w x)) 1 2 3 4) ; -1
;((lambda (w x y z) (- w x y z)) 1 2 3 4) ; -8
;((lambda (w x y z) (- w x y z)) 1 2 "three" 4) ; Exception in -: "three" is not a number
;(- 1/2 1/3) ; 1/6
;(- 1/7) ; -1/7
;(- -4/8) ; 1/2
;(- 1/2 -1/4) ; 3/4
;(- 1 1/2) ; 1/2
;(- 1/2 2/2 3/2 4/2) ; -4

;boolean? ; closure
;(boolean? #t #f) ; exception incorrect arg count
;(boolean? #f) ; #t
;(boolean? #f) ; #t
;(boolean? 1/3) ; #f
;(boolean? '(1 . 8)) ; #f
;(boolean? ((lambda () #t))) ; #t
;((lambda (v w x y z) (boolean? v w)) #t #f 1/3 '(1 . 8) ((lambda () #t))) ; exception incorrect arg count
;((lambda (v w x y z) (boolean? w)) #t #f 1/3 '(1 . 8) ((lambda () #t))) ; #t
;((lambda (v w x y z) (boolean? w)) #t #f 1/3 '(1 . 8) ((lambda () #t))) ; #t
;((lambda (v w x y z) (boolean? x)) #t #f 1/3 '(1 . 8) ((lambda () #t))) ; #f
;((lambda (v w x y z) (boolean? y)) #t #f 1/3 '(1 . 8) ((lambda () #t))) ; #f
;((lambda (v w x y z) (boolean? z)) #t #f 1/3 '(1 . 8) ((lambda () #t))) ; #t

;car ; closure
;(car '(111 2 3)) ; 111
;(car '(111 2 3)) ; 111
;(car '(111 2 3) 6) ; exception incorrect arg count
;(car 6) ; Exception in car: 6 is not a pair
;(car "king") ; Exception in car: "king" is not a pair
;((lambda (x y z) (car x)) '(111 2 3) 6 "king") ; 111
;((lambda (x y z) (car x)) '(111 2 3) 6 "king") ; 111
;((lambda (x y z) (car x y)) '(111 2 3) 6 "king") ; exception incorrect arg count
;((lambda (x y z) (car x) (x y)) '(111 2 3) 6 "king") ; Exception: attempt to apply non-procedure (111 2 3)
;((lambda (x y z) (car y)) '(111 2 3) 6 "king") ; Exception in car: 6 is not a pair
;((lambda (x y z) (car z)) '(111 2 3) 6 "king") ; Exception in car: "king" is not a pair
;((lambda (x y z) ((lambda (a) (car x)) 5)) '(111 2 3) 6 "king") ; 111
;((lambda (x y z) ((lambda (a) (car x)) 5)) '(111 2 3) 6 "king") ; 111
;((lambda (x y z) ((lambda (a) (car x y)) 5)) '(111 2 3) 6 "king") ; exception incorrect arg count
;((lambda (x y z) ((lambda (a) (car y)) 5)) '(111 2 3) 6 "king") ; Exception in car: 6 is not a pair
;((lambda (x y z) ((lambda (a) (car z)) 5)) '(111 2 3) 6 "king") ; Exception in car: "king" is not a pair

;cdr ; closure
;(cdr '(111 2 3)) ; (2 3)
;(cdr '(111 2 3)) ; (2 3)
;(cdr '(111 2 3) 6) ; exception incorrect arg count
;(cdr 6) ; Exception in cdr: 6 is not a pair
;(cdr "king") ; Exception in cdr: "king" is not a pair
;((lambda (x y z) (cdr x)) '(111 2 3) 6 "king") ; (2 3)
;((lambda (x y z) (cdr x)) '(111 2 3) 6 "king") ; (2 3)
;((lambda (x y z) (cdr x y)) '(111 2 3) 6 "king") ; exception incorrect arg count
;((lambda (x y z) (cdr x) (x y)) '(111 2 3) 6 "king") ; Exception: attempt to apply non-procedure (111 2 3) 
;((lambda (x y z) (cdr y)) '(111 2 3) 6 "king") ; Exception in cdr: 6 is not a pair
;((lambda (x y z) (cdr z)) '(111 2 3) 6 "king") ; Exception in cdr: "king" is not a pair
;((lambda (x y z) ((lambda (a) (cdr x)) 5)) '(111 2 3) 6 "king") ; (2 3)
;((lambda (x y z) ((lambda (a) (cdr x)) 5)) '(111 2 3) 6 "king") ; (2 3)
;((lambda (x y z) ((lambda (a) (cdr x y)) 5)) '(111 2 3) 6 "king") ; exception incorrect arg count
;((lambda (x y z) ((lambda (a) (cdr y)) 5)) '(111 2 3) 6 "king") ; Exception in cdr: 6 is not a pair
;((lambda (x y z) ((lambda (a) (cdr z)) 5)) '(111 2 3) 6 "king") ; Exception in cdr: "king" is not a pair

;char->integer ; closure
;(char->integer 7 13) ; exception incorrect arg count
;(char->integer '#\x) ; 120
;(char->integer '#\x) ; 120
;(char->integer 1/3) ; Exception in char->integer: 1/3 is not a character
;(char->integer '#\y) ; 121
;(char->integer '#\z) ; 122
;(char->integer '#\w) ; 119
;(char->integer '#\a 105) ; exception incorrect arg count
;((lambda (x y z) (char->integer (cdr z))) 10 1/3 '(1 . #\s)) ; 115
;((lambda (x y z) (char->integer (cdr z))) 7 1/3 '(#\s . 8)) ; Exception in char->integer: 8 is not a character
;((lambda (x y z w) (char->integer w)) #\a 1/3 '(1 . 8) #\s) ; 115
;(char->integer '#\newline) ; 10

;char? ; closure
;(char? #\d #\f) ; exception incorrect arg count
;(char? #\f) ; #t
;(char? #\f) ; #t
;(char? 1/3) ; #f
;(char? '(1 . 8)) ; #f
;(char? ((lambda () #\z))) ; #t
;((lambda (v w x y z) (char? v w)) #\d #\f 1/3 '(1 . 8) ((lambda () #\z))) ; exception incorrect arg count
;((lambda (v w x y z) (char? w)) #\d #\f 1/3 '(1 . 8) ((lambda () #\z))) ; #t
;((lambda (v w x y z) (char? w)) #\d #\f 1/3 '(1 . 8) ((lambda () #\z))) ; #t
;((lambda (v w x y z) (char? x)) #\d #\f 1/3 '(1 . 8) ((lambda () #\z))) ; #f
;((lambda (v w x y z) (char? y)) #\d #\f 1/3 '(1 . 8) ((lambda () #\z))) ; #f
;((lambda (v w x y z) (char? z)) #\d #\f 1/3 '(1 . 8) ((lambda () #\z))) ; #t

;cons ; closure
;(cons #\d #\f) ; (#\d . #\f)
;(cons #\d #\f) ; (#\d . #\f)
;(cons #\f) ; exception incorrecct arg count
;(cons 1/3) ; exception incorrecct arg count
;(cons '(1 . 8)) ; exception incorrecct arg count
;(cons ((lambda () #\z))) ; exception incorrecct arg count
;((lambda (v w x y z) (cons v w)) #\d #\f 1/3 '(1 . 8) ((lambda () #\z))) ; (#\d . #\f)
;((lambda (v w x y z) (cons v w)) #\d #\f 1/3 '(1 . 8) ((lambda () #\z))) ; (#\d . #\f)
;((lambda (v w x y z) (cons w z)) #\d #\f 1/3 '(1 . 8) ((lambda () #\z))) ; (#\f . #\z)
;((lambda (v w x y z) (cons x y)) #\d #\f 1/3 '(1 . 8) ((lambda () #\z))) ; (1/3 1 . 8)
;((lambda (v w x y z) (cons y z)) #\d #\f 1/3 '(1 . 8) ((lambda () #\z))) ; ((1 . 8) . #\z)
;((lambda (v w x y z) (cons z z)) #\d #\f 1/3 '(1 . 8) ((lambda () #\z))) ; (#\z . #\z)

;denominator ; closure
;(denominator 2/3 7) ; ecxeption incorrect arg count
;(denominator 2/3) ; 3
;(denominator 2/3) ; 3
;(denominator 7) ; 1
;(denominator '(1 . 8)) ; Exception in denominator: (1 . 8) is not a rational number
;((lambda (x y z) (denominator x y)) 1/3 7 '(1 . 8)) ; ecxeption incorrect arg count
;((lambda (x y z) (denominator x)) 1/3 7 '(1 . 8)) ; 3
;((lambda (x y z) (denominator x)) 1/3 7 '(1 . 8)) ; 3
;((lambda (x y z) (denominator y)) 1/3 7 '(1 . 8)) ; 1
;((lambda (x y z) (denominator z)) 1/3 7 '(1 . 8)) ; Exception in denominator: (1 . 8) is not a rational number

;eq? ; closure
;(eq? '() '()) ; #t - same address
;(eq? #t #t) ; #t - same address
;(eq? #f #f) ; #t - same address
;(eq? #t #f) ; #f - different types
;(eq? 1 1) ; #t - same address
;(eq? 1 2) ; #f - different integer values
;(eq? 1/2 2/2) ; #f - different types
;(eq? 1/2 3/6) ; #t - same address
;(eq? #\a #\a) ; #t - same address
;(eq? #\a #\b) ; #f - different char values
;(eq? "abc" "abc") ; #t - same address
;(eq? "abc" "abcd") ; #f - different string lengths
;(eq? "abcd" "abcf") ; #f - different strings
;(eq? 'a 'a) ; #t - same addresses
;(eq? 'a 'b) ; #f - pointers to different strings
;(eq? car car) ; #t - same addresses
;(eq? car cdr) ; #f - different CLOSURE_CODE's
;(eq? '(1 . 2) '(1 . 2)) ; #t - same addresses
;(eq? '(1 . 2) '(1 . 3)) ; #f - different contents
;(eq? (cons 1 2) (cons 1 2)) ; #f - different addresses
;(define x (cons 1 2))
;(define y x)
;(eq? x y) ; #t - same addresses
;(eq? '#(1 2) '#(1 2)) ; #t - same addresses
;(eq? '#(1 2) '#(1 3)) ; #f - different contents
;(eq? (vector 1 2) (vector 1 2)) ; #f - different addresses
;(define a (cons 1 2))
;(define b a)
;(eq? a b) ; #t - same addresses
;(eq? (car '(a b c)) 'a) ; #t - same type and content
;(eq? '() (cdr '(1))) ; #t
;(define a '(1 2 . 3))
;(define b a)
;(eq? a b) ; #t - same addresses
;(eq? (define t 6) (define g 9)) ; #t - same address of distinct value #<void>

;integer? ; closure
;(integer? 7 1/3) ; ecxeption incorrect arg count
;(integer? 7) ; #t
;(integer? 7) ; #t
;(integer? 1/3) ; #f
;(integer? '(1 . 8)) ; #f
;((lambda (x y z) (integer? x y)) 7 1/3 '(1 . 8)) ; ecxeption incorrect arg count
;((lambda (x y z) (integer? x)) 7 1/3 '(1 . 8)) ; #t
;((lambda (x y z) (integer? x)) 7 1/3 '(1 . 8)) ; #t
;((lambda (x y z) (integer? y)) 7 1/3 '(1 . 8)) ; #f
;((lambda (x y z) (integer? z)) 7 1/3 '(1 . 8)) ; #f

;integer->char ; closure
;(integer->char 7 1/3) ; exception incorrect arg count
;(integer->char 7) ; #\alarm (equal? #\alarm #\x7) returns true
;(integer->char 1/3) ; Exception in integer->char: 1/3 is not a valid unicode scalar value
;(integer->char 80) ; #\P
;(integer->char 80) ; #\P
;(integer->char 95) ; #\_
;(integer->char 100) ; #\d
;(integer->char 102) ; #\f
;(integer->char 105) ; #\i
;(integer->char 1) ; #\x1
;((lambda (x y z) (integer->char (car z))) 7 1/3 '(1 . 8)) ; #\x1
;((lambda (x y z) (integer->char x)) 10 1/3 '(1 . 8)) ; #\newline
;((lambda (x y z) (integer->char (cdr z))) 7 1/3 '(1 . 8)) ; #\backspace ; (equal? #\backspace #\x8) returns true
;((lambda (x y z w) (integer->char w)) 7 1/3 '(1 . 8) 85) ; #U

;list ; closure
;(list) ; ()
;(list 1 2 3 4 "avi") ; (1 2 3 4 "avi")
;(list 1 '(2 (3 . 4) 7) "avi") ; (1 (2 (3 . 4) 7) "avi")
;((lambda (a b c d e f) (list)) 1 2 3 4 "avi" '(2 (3 . 4) 7)) ; () ;
;((lambda (a b c d e f) (list a b c d e)) 1 2 3 4 "avi" '(2 (3 . 4) 7)) ; (1 2 3 4 "avi")
;((lambda (a b c d e f) (list a f e)) 1 2 3 4 "avi" '(2 (3 . 4) 7)) ; (1 (2 (3 . 4) 7) "avi")

;make-string ; closure
;(make-string 5 #\f -7) ; Exception: incorrect argument count in call (make-string 5 #\f -7)
;(make-string 5) ; "\x0;\x0;\x0;\x0;\x0;"
;(make-string 5 5) ; Exception in make-string: 5 is not a character
;(make-string -7) ; Exception in make-string: -7 is not a nonnegative fixnum
;(make-string 5 #\f) ; "fffff"
;(make-string 1/2) ; Exception in make-vector: 1/2 is not a nonnegative fixnum
;((lambda (w x y z) (make-string w x y)) 5 #\f -7 1/2) ; Exception: incorrect argument count in call (make-string 5 #\f -7)
;((lambda (w x y z) (make-string w)) 5 #\f -7 1/2) ; "\x0;\x0;\x0;\x0;\x0;"
;((lambda (w x y z) (make-string w w)) 5 #\f -7 1/2) ; Exception in make-string: 5 is not a character
;((lambda (w x y z) (make-string y)) 5 #\f -7 1/2) ; Exception in make-vector: -7 is not a nonnegative fixnum
;((lambda (w x y z) (make-string w x)) 5 #\f -7 1/2) ; "fffff"
;((lambda (w x y z) (make-string z)) 5 #\f -7 1/2) ; Exception in make-vector: 1/2 is not a nonnegative fixnum

;make-vector ; closure
;(make-vector 5 4 -7) ; ecxeption incorrect arg count
;(make-vector 5) ; #(0 0 0 0 0)
;(make-vector 5) ; #(0 0 0 0 0)
;(make-vector -7) ; Exception in make-vector: -7 is not a nonnegative fixnum
;(make-vector 5 4) ; #(4 4 4 4 4)
;(make-vector 1/2) ; Exception in make-vector: 1/2 is not a nonnegative fixnum
;((lambda (w x y z) (make-vector w x y)) 5 4 -7 1/2) ; ecxeption incorrect arg count
;((lambda (w x y z) (make-vector w)) 5 4 -7 1/2) ; #(0 0 0 0 0)
;((lambda (w x y z) (make-vector w)) 5 4 -7 1/2) ; #(0 0 0 0 0)
;((lambda (w x y z) (make-vector y)) 5 4 -7 1/2) ; Exception in make-vector: -7 is not a nonnegative fixnum
;((lambda (w x y z) (make-vector w x)) 5 4 -7 1/2) ; #(4 4 4 4 4)
;((lambda (w x y z) (make-vector z)) 5 4 -7 1/2) ; Exception in make-vector: 1/2 is not a nonnegative fixnum

;map ; closure
;(map) ; Exception: incorrect argument count in call
;(map cons)  ; Exception. (not conforming to scheme since append is generated via code-gen)
;(map '(1 2 3) '(1 2 3))  ; Exception. (not conforming to scheme since append is generated via code-gen)
;(map cons '(1 2 3) '(1 2 3)) ; ((1 . 1) (2 . 2) (3 . 3))
;(map + '(1 2 3) '(1 2 3)) ; (2 4 6)
;(map + '(1 2 3) '(1 2 3) '(1 2 3)) ; (3 6 9)
;(map remainder '(13 13 -13 -13) '(4 -4 -4 4)) ; (1 1 -1 -1)
;(map boolean? '(1 #t 3 #f)) ; (#f #t #f #t)
;(map not '(#t #t #t)) ; (#f #f #f)
;((lambda (a b c d e f g h i j) (map)) cons '(1 2 3) + remainder '(13 13 -13 -13) '(4 -4 -4 4) boolean? '(1 #t 3 #f) not '(#t #t #t)) ; Exception: incorrect argument count in call
;((lambda (a b c d e f g h i j) (map a)) cons '(1 2 3) + remainder '(13 13 -13 -13) '(4 -4 -4 4) boolean? '(1 #t 3 #f) not '(#t #t #t)) ; Exception. (not conforming to scheme since append is generated via code-gen)
;((lambda (a b c d e f g h i j) (map b b)) cons '(1 2 3) + remainder '(13 13 -13 -13) '(4 -4 -4 4) boolean? '(1 #t 3 #f) not '(#t #t #t)) ; Exception. (not conforming to scheme since append is generated via code-gen)
;((lambda (a b c d e f g h i j) (map a b b)) cons '(1 2 3) + remainder '(13 13 -13 -13) '(4 -4 -4 4) boolean? '(1 #t 3 #f) not '(#t #t #t)) ; ((1 . 1) (2 . 2) (3 . 3))
;((lambda (a b c d e f g h i j) (map c b b)) cons '(1 2 3) + remainder '(13 13 -13 -13) '(4 -4 -4 4) boolean? '(1 #t 3 #f) not '(#t #t #t)) ; (2 4 6)
;((lambda (a b c d e f g h i j) (map d e f)) cons '(1 2 3) + remainder '(13 13 -13 -13) '(4 -4 -4 4) boolean? '(1 #t 3 #f) not '(#t #t #t)) ; (1 1 -1 -1)
;((lambda (a b c d e f g h i j) (map g h)) cons '(1 2 3) + remainder '(13 13 -13 -13) '(4 -4 -4 4) boolean? '(1 #t 3 #f) not '(#t #t #t)) ; (#f #t #f #t)
;((lambda (a b c d e f g h i j) (map i j)) cons '(1 2 3) + remainder '(13 13 -13 -13) '(4 -4 -4 4) boolean? '(1 #t 3 #f) not '(#t #t #t)) ; (#f #f #f)

;not ; closure
;(not 5) ; #f
;(not "super") ; #f
;(not "sukka") ; #f
;(not #f) ; #t
;(not ((lambda (x) (not x)) 5)) ; #t

;null? ; closure
;(null? 69 "snir") ; ecxeption incorrect arg count
;(null? 69) ; #f
;(null? "snir") ; #f
;(null? '()) ; #t
;((lambda (x y z) (null? x y)) 69 "snir" '()) ; ecxeption incorrect arg count
;((lambda (x y z) (null? x)) 69 "snir" '()) ; #f
;((lambda (x y z) (null? y)) 69 "snir" '()) ; #f
;((lambda (x y z) (null? z)) 69 "snir" '()) ; #t

;number? ; closure
;(number? 48 4/8) ; exception incorrect arg count
;(number? 48) ; #t
;(number? 4/8) ; #t
;(number? "shihrur") ; #f
;((lambda (x y z) (number? x y)) 48 4/8 "shihrur") ; exception incorrect arg count
;((lambda (x y z) (number? x)) 48 4/8 "shihrur") ; #t
;((lambda (x y z) (number? y)) 48 4/8 "shihrur") ; #t
;((lambda (x y z) (number? z)) 48 4/8 "shihrur") ; #f

;numerator ; closure
;(numerator 2/3 7) ; ecxeption incorrect arg count
;(numerator 2/3) ; 2
;(numerator 7) ; 7
;(numerator '(1 . 8)) ; Exception in numerator: (1 . 8) is not a rational number
;((lambda (x y z) (numerator x y)) 2/3 7 '(1 . 8)) ; ecxeption incorrect arg count
;((lambda (x y z) (numerator x)) 2/3 7 '(1 . 8)) ; 2
;((lambda (x y z) (numerator y)) 2/3 7 '(1 . 8)) ; 7
;((lambda (x y z) (numerator z)) 2/3 7 '(1 . 8)) ; Exception in numerator: (1 . 8) is not a rational number

;pair? ; closure
;(pair? '(1 . 2) '(3 4 5)) ; exception incorrect arg count
;(pair? '(1 . 2)) ; #t
;(pair? '(3 4 5)) ; #t
;(pair? number?) ; #f
;((lambda (x y z) (pair? x y)) '(1 . 2) '(3 4 5) number?) ; exception incorrect arg count
;((lambda (x y z) (pair? x)) '(1 . 2) '(3 4 5) number?) ; #t
;((lambda (x y z) (pair? y)) '(1 . 2) '(3 4 5) number?) ; #t
;((lambda (x y z) (pair? z)) '(1 . 2) '(3 4 5) number?) ; #f

;procedure? ; closure
;(procedure? '(1 . 2) procedure?) ; exception incorrect arg count
;(procedure? '(1 . 2)) ; #f
;(procedure? procedure?) ; #t
;(procedure? number?) ; #t
;((lambda (x y z) (procedure? x y)) '(1 . 2) procedure? number?) ; exception incorrect arg count
;((lambda (x y z) (procedure? x)) '(1 . 2) procedure? number?) ; #f
;((lambda (x y z) (procedure? y)) '(1 . 2) procedure? number?) ; #t
;((lambda (x y z) (procedure? z)) '(1 . 2) procedure? number?) ; #t

;rational? ; closure
;(rational? 2/3 7) ; exception incorrect arg count
;(rational? 2/3) ; #t
;(rational? 7) ; #t
;(rational? number?) ; #f
;((lambda (x y z) (rational? x y)) 2/3 7 number?) ; exception incorrect arg count
;((lambda (x y z) (rational? x)) 2/3 7 number?) ; #t
;((lambda (x y z) (rational? y)) 2/3 7 number?) ; #t
;((lambda (x y z) (rational? z)) 2/3 7 number?) ; #f

;remainder ; closure
;(remainder 13 4 5) ; exception incorrect arg count
;(remainder 13 4) ; 1
;(remainder -13 4) ; -1
;(remainder -13 -4) ; -1
;(remainder 13 -4) ; 1
;(remainder 13 "a") ; Exception in remainder: "a" is not an integer
;(remainder "b" 4) ; Exception in remainder: "b" is not an integer
;(remainder 13 0) ; Exception in remainder: undefined for 0
;((lambda (x y z) (remainder x y z)) 13 4 5) ; exception incorrect arg count
;((lambda (x y z) (remainder x y)) 13 4 5) ; 1
;((lambda (x y z) (remainder x y)) -13 4 5) ; -1
;((lambda (x y z) (remainder x y)) -13 -4 5) ; -1
;((lambda (x y z) (remainder x y)) 13 -4 5) ; 1
;((lambda (x y z) (remainder x y)) 13 "a" 5) ; Exception in remainder: "a" is not an integer
;((lambda (x y z) (remainder x y)) "b" 4 5) ; Exception in remainder: "b" is not an integer
;((lambda (x y z) (remainder x y)) 13 0 5)  ; Exception in remainder: undefined for 0

;set-car! ; closure
;(define x1 '(1 . 2)) ; nothing
;(define x2 '("sukka" . "blyat")) ; nothing
;(define x3 '("huyassa" . "gopnik")) ; nothing
;(set-car! x1 3) ; nothing
;x1 ; (3 . 2)
;(set-car! x2 "nahui") ; nothing
;(set-car! x1 "blyat") ; nothing
;x1 ; ("blyat" . 2)
;x2 ; ("nahui" . "blyat")
;x3 ; ("huyassa" . "gopnik")

;set-cdr! ; closure
;(define x1 '(1 . 2)); nothing
;(define x2 '("sukka" . "blyat")); nothing
;(define x3 '("huyassa" . "gopnik")); nothing
;(set-cdr! x1 3); nothing
;(set-cdr! x2 "nahui"); nothing
;(set-cdr! x3 "blyat"); nothing
;x1 ; (1 . 3)
;x2 ; ("sukka" . "nahui")
;x3 ; ("huyassa" . "blyat")

;string-length ; closure
;(string-length) ; Exception: incorrect argument count in call (string-length)
;(string-length "avi" "natan") ; Exception: incorrect argument count in call (string-length)
;(string-length 6) ; Exception in string-length: 6 is not a string
;(string-length "avi") ; 3
;((lambda (x y z) (string-length)) "avi" "natan" 6) ; Exception: incorrect argument count in call (string-length)
;((lambda (x y z) (string-length x y)) "avi" "natan" 6) ; Exception: incorrect argument count in call (string-length)
;((lambda (x y z) (string-length z)) "avi" "natan" 6) ; Exception in string-length: 6 is not a string
;((lambda (x y z) (string-length x)) "avi" "natan" 6) ; 3

;string-ref ; closure
;(string-ref) ; Exception: incorrect argument count in call (string-ref)
;(string-ref "avi") ; Exception: incorrect argument count in call (string-ref "avi")
;(string-ref "avi" 1/3) ; Exception in string-ref: 1/3 is not a valid index for "avi"
;(string-ref "avi" 3) ; Exception in string-ref: 3 is not a valid index for "avi"
;(string-ref 3 3) ; Exception in string-ref: 3 is not a string
;(string-ref "avi" 2) ; #\i
;((lambda (w x y z) (string-ref)) "avi" 1/3 3 2) ; Exception: incorrect argument count in call (string-ref)
;((lambda (w x y z) (string-ref w)) "avi" 1/3 3 2) ; Exception: incorrect argument count in call (string-ref "avi")
;((lambda (w x y z) (string-ref w x)) "avi" 1/3 3 2) ; Exception in string-ref: 1/3 is not a valid index for "avi"
;((lambda (w x y z) (string-ref w y)) "avi" 1/3 3 2) ; Exception in string-ref: 3 is not a valid index for "avi"
;((lambda (w x y z) (string-ref y y)) "avi" 1/3 3 2) ; Exception in string-ref: 3 is not a string
;((lambda (w x y z) (string-ref w z)) "avi" 1/3 3 2) ; #\i
;(define a "avi") ; nothing
;a ; "avi"
;(string-ref a 2) ; #\i
;a ; "avi"

;string-set! ; closure
;(define s "avi") ; nothing
;s ; "avi"
;(string-set! s 2) ; Exception: incorrect argument count in call (string-set! s 2)
;(string-set! s 2 5) ; Exception in string-set!: 5 is not a character
;(string-set! s 5 #\e) ; Exception in string-set!: 5 is not a valid index for "avi"
;(string-set! 5 2 #\e) ; Exception in string-set!: 5 is not a string
;(string-set! s 2 #\e) ; nothing
;s ; "ave"
;(define b "avi") ; nothing
;b ; "avi" ; FAILS. TODO: Fix define to support deep copy.
;(define c "yi") ; nothing
;c ; "yi" 
;((lambda (w x y z) (string-set! w x)) c 0 5 #\v) ; Exception: incorrect argument count in call (string-set! w x)
;((lambda (w x y z) (string-set! w x y)) c 0 5 #\v) ; Exception in string-set!: 5 is not a character
;((lambda (w x y z) (string-set! w y z)) c 0 5 #\v) ; Exception in string-set!: 5 is not a valid index for "yi"
;((lambda (w x y z) (string-set! y x z)) c 0 5 #\v); Exception in string-set!: 5 is not a string
;((lambda (w x y z) (string-set! w x z)) c 0 5 #\v); nothing
;c ; "vi"

;string->symbol
;(string->symbol "abc" 1) ; Exception: incorrect argument count in call
;(string->symbol 1) ; Exception in symbol->string: 1 is not a string
;(string->symbol "abc") ; abc
;((lambda (x y) (string->symbol x y)) "abc" 1) ; Exception: incorrect argument count in call
;((lambda (x y) (string->symbol x)) "abc" 1) ; abc
;((lambda (x y) (string->symbol y)) "abc" 1) ; Exception in symbol->string: 1 is not a string

;string? ; closure
;(string? '(1 . 2) procedure?) ; exception incorrect arg count
;(string? '(1 . 2)) ; #f
;(string? "seagulls") ; #t
;(string? number?) ; #f
;((lambda (x y z) (string? x y)) '(1 . 2) "seagulls" number?) ; exception incorrect arg count
;((lambda (x y z) (string? x)) '(1 . 2) "seagulls" number?) ; #f
;((lambda (x y z) (string? y)) '(1 . 2) "seagulls" number?) ; #t
;((lambda (x y z) (string? z)) '(1 . 2) "seagulls" number?) ; #f

;symbol? ; closure
;(symbol?  'a '(1 2 3)) ; Exception: incorrect argument count in call
;(symbol? 'a) ; #t
;(symbol? '(1 2 3)) ; #f
;(symbol? "seagulls") ; #f
;((lambda (x y z) (symbol? x y)) 'a' '(1 2 3) "seagulls") ; Exception: incorrect argument count in call
;((lambda (x y z) (symbol? x)) 'a' '(1 2 3) "seagulls") ; #t
;((lambda (x y z) (symbol? y)) 'a' '(1 2 3) "seagulls") ; #f
;((lambda (x y z) (symbol? z)) 'a' '(1 2 3) "seagulls") ; #f

;symbol->string ; closure
;(symbol->string 'abc 1) ; Exception: incorrect argument count in call
;(symbol->string 1) ; Exception in symbol->string: 1 is not a symbol
;(symbol->string 'abc) ; "abc"
;((lambda (x y) (symbol->string x y)) 'abc 1) ; Exception: incorrect argument count in call
;((lambda (x y) (symbol->string x)) 'abc 1) ; "abc"
;((lambda (x y) (symbol->string y)) 'abc 1) ; Exception in symbol->string: 1 is not a symbol

;vector ; closure
;(vector) ; #()
;(vector 1 2) ; #(1 2)
;((lambda (x y) (vector)) 1 2) ; #()
;((lambda (x y) (vector x y)) 1 2) ; #(1 2)

;vector-length ; closure
;(vector-length '#(10 20 30 40) 5) ; exception incorrect arg count
;(vector-length 5) ; Exception in vector-length: 5 is not a vector
;(vector-length '#(10 20 30 40)) ; 4
;((lambda (x y) (vector-length x y)) '#(10 20 30 40) 5) ; exception incorrect arg count
;((lambda (x y) (vector-length y)) '#(10 20 30 40) 5) ; Exception in vector-length: 5 is not a vector
;((lambda (x y) (vector-length x)) '#(10 20 30 40) 5) ; 4

;vector-ref ; closure
;(vector-ref '#(10 20 30 40)) ; Exception: incorrect argument count in call
;(vector-ref 3) ; Exception: incorrect argument count in call
;(vector-ref '#(10 20 30 40) 3 "f") ; Exception: incorrect argument count in call
;(vector-ref 4 3) ; Exception in vector-ref: 4 is not a vector
;(vector-ref '#(10 20 30 40) "f") ; Exception in vector-ref: "f" is not a valid index for #(10 20 30 40)
;(vector-ref '#(10 20 30 40) 4) ; Exception in vector-ref: 4 is not a valid index for #(10 20 30 40)
;(vector-ref '#(10 20 30 40) 3) ; 40
;((lambda (w x y z) (vector-ref w)) '#(10 20 30 40) 3 4 "f") ; Exception: incorrect argument count in call
;((lambda (w x y z) (vector-ref x)) '#(10 20 30 40) 3 4 "f") ; Exception: incorrect argument count in call
;((lambda (w x y z) (vector-ref w x z)) '#(10 20 30 40) 3 4 "f") ; Exception: incorrect argument count in call
;((lambda (w x y z) (vector-ref y x)) '#(10 20 30 40) 3 4 "f") ; Exception in vector-ref: 4 is not a vector
;((lambda (w x y z) (vector-ref w z)) '#(10 20 30 40) 3 4 "f") ; Exception in vector-ref: "f" is not a valid index for #(10 20 30 40)
;((lambda (w x y z) (vector-ref w y)) '#(10 20 30 40) 3 4 "f") ; Exception in vector-ref: 4 is not a valid index for #(10 20 30 40)
;((lambda (w x y z) (vector-ref w x)) '#(10 20 30 40) 3 4 "f") ; 40
;(define a '#(1 2 3)) ; nothing
;a ; #(1 2 3)
;(vector-ref a 2) ; 3
;a ; #(1 2 3)

;vector-set! ; closure
;(define a '#(1 2 3)) ; nothing
;a ; '#(1 2 3)
;(vector-set! a 2) ; Exception: incorrect argument count in call
;(vector-set! a 3 10) ; Exception in vector-set!: 3 is not a valid index for #(1 2 3)
;(vector-set! a -7 10) ; Exception in vector-set!: -7 is not a valid index for #(1 2 3)
;(vector-set! a 2 10) ; nothing
;a ; '#(1 2 10)
;(define b '(1 2 3)) ; nothing
;b ; '(1 2 3)
;(vector-set! b 2 10) ; Exception in vector-set!: (1 2 3) is not a vector
;(define c '#(1 2 3)) ; nothing
;c ; '#(1 2 3) ; FAILS. TODO: debug define to do deep copy in order to support this
;((lambda (v w x y z) (vector-set! v w)) c 2 3 -7 10) ; Exception: incorrect argument count in call
;((lambda (v w x y z) (vector-set! v x z)) c 2 3 -7 10) ; Exception in vector-set!: 3 is not a valid index for #(1 2 3)
;((lambda (v w x y z) (vector-set! v y z)) c 2 3 -7 10) ; Exception in vector-set!: -7 is not a valid index for #(1 2 3)
;((lambda (v w x y z) (vector-set! v w z)) c 2 3 -7 10); nothing
;c ; '#(1 2 10)

;vector? ; closure
;(vector?  '#(1 2 3) '(1 2 3)) ; exception incorrect arg count
;(vector? '#(1 2 3)) ; #t
;(vector? '(1 2 3)) ; #f
;(vector? "seagulls") ; #f
;((lambda (x y z) (vector? x y)) '#(1 2 3) '(1 2 3) "seagulls") ; exception incorrect arg count
;((lambda (x y z) (vector? x)) '#(1 2 3) '(1 2 3) "seagulls") ; #t
;((lambda (x y z) (vector? y)) '#(1 2 3) '(1 2 3) "seagulls") ; #f
;((lambda (x y z) (vector? z)) '#(1 2 3) '(1 2 3) "seagulls") ; #f

;zero? ; closure
;(zero? 0 7) ; exception incorrect arg count
;(zero? 0) ; #t
;(zero? 7) ; #f
;(zero? "seagulls") ; Exception in zero?: "seagulls" is not a number
;((lambda (x y z) (zero? x y)) 0 7 "seagulls") ; exception incorrect arg count
;((lambda (x y z) (zero? x)) 0 7 "seagulls") ; #t
;((lambda (x y z) (zero? y)) 0 7 "seagulls") ; #f
;((lambda (x y z) (zero? z)) 0 7 "seagulls") ; Exception in zero?: "seagulls" is not a number
;;; primitives tests


;;; if, or, sequence
;(if #f 2 4)	;4
;(if 1 2 3)		;2
;(or 1 2 3 4)	;1
;(if 1 (if 2 (if 3 4) 5)) ; 4
;(or #f (or #f 1 2) 3 4) ; 1
;(or #f #f #f)	;#f
;(or)			;#f
;(begin 1 2 3 4 5 6 7 8 9 10) ;10
;(or #f (if #f 2 '("avi" . "king")) 30) ;'( "avi" . "king")
;;; if, or, sequence


;;; define, fvar
;(define a 9) ; nothing
;(define x1 5) ; nothing
;a ; 9
;b ; Exception: attempt to use undefined
;x1 ; 5
;;; define, fvar


;;; lambda-simple, applic
;(lambda (a) (if #t 4 a)) ; a closure
;(lambda (x) (lambda (a) (if #t 4 a))) ; a closure
;(lambda (y) (lambda (x) (lambda (a) (if #t 4 a)))) ; a closure
;(((lambda (x) (lambda (y) (or x y))) 3) 4) ; 3
;9
;('(4 . 6) 1 2 3) ; Exception: attempt to apply non-procedure (4 . 6)
;8
;(((((lambda (x) (lambda (y) (lambda (z) (lambda (w) (or w x y z))))) 1) 2) 3) 17) ; 17
;(((((lambda (x) (lambda (y) (lambda (z) (lambda (w a b c) (or w x y z a b c))))) 66) 2) 3) #f 200 300 400) ; 66
;(((((lambda (a) (lambda (b c) (lambda (d e f) (lambda (g h i j) (or g a b d))))) #f) 11 12) 21 22 23) #f 32 33 34) ; 11
;(define a (lambda (z) (lambda (w) (or w x y z)))) ; nothing
;(((((lambda (x) (lambda (y) a)) 1)  2) 3) 17) ; 17
;(define b (lambda (x) x)) ; nothing
;b ; closure
;(b 6) ; 6
;(b 5 7 5 5) ; Exception: incorrect argument count in call
;56
;c ; exception attemt to use unbounded
;3
;3
;444
;(r 4 5) ; exception attempt to use unbounded
;444
;;; lambda-simple, applic


;;; lambda-opt
;(lambda (a b . c) (or a b)) ; procedure
;((lambda (a b . c) (or a b)) 49 2 3 4) ; 49
;((lambda (a b . c) (or a b)) 34) ; Exception: incorrect argument count in call
;((lambda (a b . c) a) 49 2 3 4) ; 49
;((lambda (a b . c) b) 49 2 3 4) ; 2
;((lambda (a b . c) c) 49 2 3 4) ; (3 4)
;((lambda (a b . c) c) 49 2) ; ()
;((lambda (a b . c) c) 49) ; Exception: incorrect argument count in call
;((lambda c c) 1 2 3 4) ; (1 2 3 4)
;((lambda (a b . c) ((car c) (car (cdr c)))) 70 71 integer? 69) ; t
;((lambda (a b . c) ((car c) (car (cdr c)))) 70 71 (lambda (x) (+ 10 x)) 39) ; 49
;(((lambda (x y z) (lambda (a b . c) ((lambda (g) (car g)) b))) 55 66 77) 1 '(600 . 700) "h" "g" "y") ; 600
;;; lambda-opt

;;; tc-applic
;((lambda (x y) ((lambda (a) (remainder a y)) 13)) 6 4)
;;; tc-applic

;;; set, box, box-set, box-get
;(((lambda (x y z) (lambda (a b c) (set! y 7) (or x y a c))) 10 20 30) 100 200 300) ; 10
;(define a 7)
;a ; 7
;(set! a 8)
;a ; 8
;(define b 7)
;b ; 7
;;; set, box, box-set, box-get


;;; other tests
;((lambda (a) (+ 5 a)) 4)
;(define adder3
;	; this is a comment in the middle of the function definition!
;	(lambda (x) (+ 3 x)))
;(adder3 4)
;##6-8
;##6-##(adder3 8)
;(* #;(/ 5 0) 8 9)
;(- 2)
;-2
;(+ 1 2)
;(apply cons '(1 2)) ; (1 . 2)
;(apply list '(1 2 3 4)) ; (1 2 3 4)
;(apply vector-ref '(#(10 20 30 40) 3)) ; 40
;((lambda (x) (not (apply list '(3)))) 8) ; #f
;(append '(1 2) '(3 (4))) ; (1 2 3 (4))
;(map list '(1 2 3) '(4 5 6) '(7 8 9) '(10 11 12)) ; ((1 4 7 10) (2 5 8 11) (3 6 9 12))
;(map list (list 1 2 3) (list 4 5 6) (list 7 8 9) (list 10 11 12)) ; ((1 4 7 10) (2 5 8 11) (3 6 9 12))
;(map + '(1 2 3) '(1 2 3) '(1 2 3)) ; (3 6 9)
;(map + '(1 2 3) '(1 2 3)) ; (2 4 6)
;(map list '(1 2 3 4) '(1 2 3 4) '(1 2 3 4) '(1 2 3 4) '(1 2 3 4) '(1 2 3 4) '(1 2 3 4) '(1 2 3 4) '(1 2 3 4)) ; ((1 1 1 1 1 1 1 1 1) (2 2 2 2 2 2 2 2 2) (3 3 3 3 3 3 3 3 3) (4 4 4 4 4 4 4 4 4))
;(map (lambda (x y z) (or x y z)) '(1 #f #f) '(10 20 #f) '(100 200 300)) ; (1 20 300)
;;; other tests