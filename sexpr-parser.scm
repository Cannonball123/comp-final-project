(load "project/pc.scm")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Utility Funcions ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; Useful flatten function using append
;; append takes two or more lists and constructs a new list with all of their elements.
;; append example: example (append '(1 2) '(3 4)) => '(1 2 3 4)
;; General rule of thumb: order matters, make sure that if there are several types and one includes another,
;; such that you want to check the disjunction of both, make sure to check the inclusive before!
;; Example: checking <Fraction> before <Integer>
(define flattenList 
  (lambda (lst) 
    (cond 
      ((null? lst) '())
      ((not (list? lst)) (list lst))
      ((not (list? (car lst)))
        (cons (car lst) (flattenList (cdr lst))))
      (else (append (flattenList (car lst)) (flattenList (cdr lst)))))))


;; A parser for the number zero
(define <Zero>
  (new
    (*parser (char #\0)) *plus
    (*pack (lambda (_) 0))
  done))


;; A parser for comments or whitespaces - for infix and prefix notations
(define <CommentOrWhiteSpace>
  (new
    (*parser (const (lambda (ch) (char<=? ch #\space))))  ; Whitespace


    (*parser (char #\#))                                  ; expression comment - prefix notation
    (*parser (char #\;))                                  ;
    (*delayed (lambda () <sexpr>))						  ;
    (*delayed (lambda () <InfixExpression>))			  ;
    *diff                        						  ;
    (*caten 3)                                            ;

    (*parser (char #\#))                                  ; expression comment - infix notation
    (*parser (char #\;))                                  ;
    (*delayed (lambda () <InfixExpression>))              ;
    (*caten 3)                                            ;


    (*parser (char #\;))                                  ; line comment
    (*parser <any>)                                       ;
    (*parser (char #\newline))                            ;
    (*parser <end-of-input>)                              ;
    (*disj 2)                                             ;
    *diff                                                 ;
    *star                                                 ;
                                                          ;
    (*parser (char #\newline))                            ;
    (*parser <end-of-input>)                              ;
    (*disj 2)                                             ;
                                                          ;
    (*caten 3)                                            ;

    (*disj 4)
    (*pack 
        (lambda (result)
          ;(display "<CommentOrWhiteSpace> result: ")
          ;(display result)
          ;(newline)
          #\space))
  done))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Utility Funcions ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Parser Implementation ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; =========================================================================================================
;; ================================= Parsers defining the <Boolean> parser =================================
;; =========================================================================================================
(define <True>
  (new
      (*parser (char #\#))
      (*parser (char-ci #\t))
      (*caten 2)

      (*pack 
        (lambda (result)
          ;(display "<True> result: ")
          ;(display result)
          ;(newline)
          #t))
    done))


(define <False>
    (new
      (*parser (char #\#))
      (*parser (char-ci #\f))
      (*caten 2)

      (*pack 
        (lambda (result)
          ;(display "<False> result: ")
          ;(display result)
          ;(newline)
          #f))
    done))


(define <Boolean>
    (new
      (*parser <True>)
      (*parser <False>)
      (*disj 2)
      
      (*pack
        (lambda (result)
            ;(display "<Boolean> result: ")
            ;(display result)
            ;(newline)
            result))
   done))
;; =========================================================================================================
;; ================================= Parsers defining the <Boolean> parser =================================
;; =========================================================================================================


;; =========================================================================================================
;; =================================== Parsers defining the <Char> parser ==================================
;; =========================================================================================================
(define <CharPrefix>
  (new
    (*parser (char #\#))
    (*parser (char #\\))
    (*caten 2)
    (*parser (const (lambda (ch) (char<=? ch #\space))))
    *not-followed-by
    (*pack 
        (lambda (result)
          ;(display "<CharPrefix> result: ")
          ;(display result)
          ;(newline)
          result))
  done))


(define <VisibleSimpleChar>
  (new 
      (*parser (const (lambda (ch) (char>? ch #\space))))
      (*parser (diff 
                  (const (lambda (ch) (char>? ch #\space)))
                  (disj (char-ci #\))
                        (char-ci #\})
                        (char-ci #\.))))
      *not-followed-by
      (*pack 
        (lambda (result)
          ;(display "<VisibleSimpleChar> result: ")
          ;(display result)
          ;(newline)
          result))
  done))


(define <NamedChar>
  (new 
      (*parser (word-ci "lambda"))
      (*parser (word-ci "newline"))
      (*parser (word-ci "nul"))
      (*parser (word-ci "page"))
      (*parser (word-ci "return"))
      (*parser (word-ci "space"))
      (*parser (word-ci "tab"))
      (*disj 7)
      (*parser (diff 
                  (const (lambda (ch) (char>? ch #\space)))
                  (disj (char-ci #\))
                        (char-ci #\})
                        (char-ci #\.))))
      *not-followed-by
      (*pack 
        (lambda (result)
          ;(display "<NamedChar> result: ")
          ;(display result)
          ;(newline)
          result))
  done))


(define <HexChar>
  (new
      (*parser (range #\0 #\9))
      (*parser (range #\a #\f))
      (*parser (range #\A #\F))
      (*disj 3)
      (*pack 
        (lambda (result)
          ;(display "<HexChar> result: ")
          ;(display result)
          ;(newline)
          result))
  done))


(define <HexUnicodeChar>
  (new
      (*parser (const (lambda (ch) 
                (or (char=? ch #\x)
                  (char=? ch #\X)))))
      (*parser <HexChar>) *plus
      (*caten 2)
      (*pack 
        (lambda (result)
          ;(display "<HexUnicodeChar> result: ")
          ;(display result)
          ;(newline)
          result))
  done))


(define <Char>
  (new
      (*parser <CharPrefix>)

      (*parser <VisibleSimpleChar>)
      (*parser <NamedChar>)
      (*parser <HexUnicodeChar>)
      (*disj 3)

      (*caten 2)
      (*pack 
        (lambda (result)
          ;(display "<Char> result: ")
          ;(display result)
          ;(newline)
          (let ((flat (flattenList result)))
            ;(display "<Char> flatted result: ")
            ;(display flat)
            ;(newline)
            (if (= 3 (length flat))
                (caddr flat)
                (if (equal? (caddr flat) #\x)
                    (integer->char (string->number (list->string (cdddr flat)) 16))
                    (let ((named (string-downcase (list->string (cddr flat)))))
                      (cond 
                        ((equal? named "lambda") #\x3bb)
                        ((equal? named "newline") #\newline)
                        ((equal? named "nul") #\nul)
                        ((equal? named "page") #\page)
                        ((equal? named "return") #\return)
                        ((equal? named "space") #\space)
                        (else #\tab))))))))
  done))
;; =========================================================================================================
;; =================================== Parsers defining the <Char> parser ==================================
;; =========================================================================================================


;; =========================================================================================================
;; ================================== Parsers defining the <Number> parser =================================
;; =========================================================================================================
(define <Natural> 
  (new 
    (*parser (range #\0 #\9)) *plus

    (*pack 
          (lambda (result)
              ;(display "<Natural> result: ")
              ;(display result)
              ;(newline)
              (string->number (list->string result))))
  done))


(define <Integer> 
  (new 
    (*parser (char #\+))
    (*parser (char #\-))
    (*disj 2)

    (*parser <Natural>)
    (*caten 2)

    (*parser <Natural>)
    (*disj 2)
    
    (*pack 
        (lambda (result)
          ;(display "<Integer> result: ")
          ;(display result)
          ;(newline)
          (cond ((number? result) result)
            ((equal? #\- (car result)) (- (cadr result)))
            (else (cadr result)))))
  done))


(define <Fraction>
  (new
    (*parser <Integer>)
    (*parser (char #\/))
    (*parser <Natural>)
    
    (*parser <Zero>)
    *diff
    (*caten 3)
    
    (*pack
        (lambda (result)
          ;(display "<Fraction> result: ")
          ;(display result)
          ;(newline)
          (/ (car result) (caddr result))))
  done)) 


(define <Number> 
  (new
    (*parser <Fraction>)
    (*parser <Integer>)
    
    (*disj 2)
    (*delayed (lambda () <SymbolChar>))
    *not-followed-by

    (*pack
      (lambda (result)
        ;(display "<Number> result: ")
        ;(display result)
        ;(newline)
        result))
  done))
;; =========================================================================================================
;; ================================== Parsers defining the <Number> parser =================================
;; =========================================================================================================


;; =========================================================================================================
;; ================================== Parsers defining the <String> parser =================================
;; =========================================================================================================
(define <StringLiteralChar> 
  (new 
    (*parser (const (lambda (ch) 
          				(and 
          					(char? ch) 
          					(and 
          						(not (char=? ch #\\))    ;; input rejection of "\\"
                                (not (char=? ch #\"))))  ;; input rejection of "\""
        ))) 
    (*pack 
        (lambda (result)
          ;(display "<StringLiteralChar> result: ")
          ;(display result)
          ;(newline)
          result))
  done))


(define <StringMetaChar> 
  (new 
    (*parser (char #\\))
    (*parser (char-ci #\\)) ;; input: \\\\
    (*caten 2)
    (*pack (lambda (result) #\\))
    
    (*parser (char #\\))
    (*parser (char-ci #\")) ;; input: \\\"
    (*caten 2)
    (*pack (lambda (result) #\"))

    (*parser (char #\\))
    (*parser (char-ci #\t)) ;; input: \\\t
    (*caten 2)
    (*pack (lambda (result) #\tab))

    (*parser (char #\\))
    (*parser (char-ci #\f)) ;; input: \\\f
    (*caten 2)
    (*pack (lambda (result) #\page))

    (*parser (char #\\))
    (*parser (char-ci #\n)) ;; input: \\\n
    (*caten 2)
    (*pack (lambda (result) #\newline))

    (*parser (char #\\))
    (*parser (char-ci #\r)) ;; input: \\\r
    (*caten 2)
    (*pack (lambda (result) #\return))

    (*disj 6)

    (*pack 
        (lambda (result)
          ;(display "<StringMetaChar> result: ")
          ;(display result)
          ;(newline)
          result))
  done))


(define <StringHexChar> 
  (new
    (*parser (char #\\))
    (*parser (char #\x))
    (*caten 2)

    (*parser <HexChar>) *star
    (*parser (char #\;))

    (*caten 3)

    (*pack 
        (lambda (result)
          ;(display "<StringHexChar> result: ")
          ;(display result)
          ;(newline)
          ;(display "<StringHexChar> flat result: ")
          ;(display (flattenList result))
          ;(newline)
          ;(display "<StringHexChar> flat result cddr: ")
          ;(display (cddr (flattenList result)))
          ;(newline)
          ;(display "<StringHexChar> flat result cddr-last: ")
          ;(display (reverse (cdr (reverse (cddr (flattenList result))))))
          ;(newline)
          (integer->char (string->number (list->string (reverse (cdr (reverse (cddr (flattenList result)))))) 16))))
  done))


(define <StringChar> 
  (new
    (*parser <StringLiteralChar>)
    (*parser <StringMetaChar>)
    (*parser <StringHexChar>)
    (*disj 3)

    (*pack 
        (lambda (result)
          ;(display "<StringChar> result: ")
          ;(display result)
          ;(newline)
          result))
  done))


(define <String> 
  (new
    (*parser (char #\"))
    (*parser <StringChar>) *star
    (*parser (char #\"))

    (*caten 3)

    (*pack 
        (lambda (result)
          ;(display "<String> result: ")
          ;(display result)
          ;(newline)
          (list->string (cadr result))))
  done))
;; =========================================================================================================
;; ================================== Parsers defining the <String> parser =================================
;; =========================================================================================================


;; =========================================================================================================
;; ================================== Parsers defining the <Symbol> parser =================================
;; =========================================================================================================
(define <SymbolChar>
  (new
    (*parser 
      (const 
        (lambda (ch) 
          (or 
            (and 
              (char>=? ch #\0)
              (char<=? ch #\9))
            (and
              (char>=? ch #\a)
              (char<=? ch #\z))
            (and
              (char>=? ch #\A)
              (char<=? ch #\Z))
            (char=? ch #\!)
            (char=? ch #\$)
            (char=? ch #\^)
            (char=? ch #\*)
            (char=? ch #\-)
            (char=? ch #\_)
            (char=? ch #\=)
            (char=? ch #\+)
            (char=? ch #\<)
            (char=? ch #\>)
            (char=? ch #\?)
            (char=? ch #\/)))))
    (*pack 
        (lambda (result)
          ;(display "<SymbolChar> result: ")
          ;(display result)
          ;(newline)
          (char-downcase result)))
  done))


(define <Symbol>
  (new
    (*parser (const (lambda (ch) (char<=? ch #\space)))) *star ;; maybe should be removed
    (*parser <SymbolChar>) *plus
    (*parser (const (lambda (ch) (char<=? ch #\space)))) *star ;; maybe should be removed
    (*caten 3)
    (*pack 
        (lambda (result)
          ;(display "<Symbol> result: ")
          ;(display result)
          ;(newline)
          (string->symbol (list->string (cadr result)))))
  done))
;; =========================================================================================================
;; ================================== Parsers defining the <Symbol> parser =================================
;; =========================================================================================================


;; =========================================================================================================
;; ================================ Parsers defining the <ProperList> parser ===============================
;; =========================================================================================================
(define <ProperList>
  (new
    (*parser (char #\())
    (*delayed (lambda () (^<separated-exprs>
                          <sexpr>           			  ;; the expression
                          (star <CommentOrWhiteSpace>)))) ;; the separator to merge
    (*parser (char #\)))
    (*caten 3)											  ;; Non - empty list

   	(*pack-with
      	(lambda (leftBracket result RightBracket)
        	;(display "<ProperList> result1: ")
        	;(display result)
        	;(newline)
      		result))

    (*parser (char #\())
    (*parser (char #\)))
    (*caten 2)											  ;; Empty list

    (*pack
      (lambda (result)
        ;(display "<ProperList> result2: ")
        ;(display result)
        ;(newline)
        (list)))

    (*disj 2)

  done))
;; =========================================================================================================
;; ================================ Parsers defining the <ProperList> parser ===============================
;; =========================================================================================================


;; =========================================================================================================
;; =============================== Parsers defining the <ImproperList> parser ==============================
;; =========================================================================================================
(define <ImproperList> 
  (new
    (*parser (char #\())
    (*delayed (lambda () (^<separated-exprs>
                          <sexpr>           				;; the expression
                          (star <CommentOrWhiteSpace>))))   ;; the separator to merge
    (*parser <CommentOrWhiteSpace>) *star 
    (*parser (char #\.))
    (*parser <CommentOrWhiteSpace>) *star

    (*delayed (lambda () <sexpr>))
    
    (*parser <CommentOrWhiteSpace>) *star
    (*parser (char #\)))

    (*caten 8)

    (*pack-with
      (lambda (elem1 elem2 elem3 elem4 elem5 elem6 elem7 elem8)
        ;(display "<ImproperList> result: ")
        ;(display (list elem1 elem2 elem3 elem4 elem5 elem6 elem7 elem8))
        ;(newline)
        ;(display "<ImproperList> elem1 result: ")
        ;(display elem1)
        ;(newline)
        ;(display "<ImproperList> elem2 result: ")
        ;(display elem2)
        ;(newline)
        ;(display "<ImproperList> elem3 result: ")
        ;(display elem3)
        ;(newline)
        ;(display "<ImproperList> elem4 result: ")
        ;(display elem4)
        ;(newline)
        ;(display "<ImproperList> elem5 result: ")
        ;(display elem5)
        ;(newline)
        ;(display "<ImproperList> elem6 result: ")
        ;(display elem6)
        ;(newline)
        ;(display "<ImproperList> elem7 result: ")
        ;(display elem7)
        ;(newline)
        ;(display "<ImproperList> elem8 result: ")
        ;(display elem8)
        ;(newline)
        ;; REMEMBER THAT: '(1 . (2 3)) = '(1 2 3)
        `(,@elem2 . ,elem6))) 
  done))
;; =========================================================================================================
;; =============================== Parsers defining the <ImproperList> parser ==============================
;; =========================================================================================================


;; =========================================================================================================
;; ================================== Parsers defining the <Vector> parser =================================
;; =========================================================================================================
(define <Vector> 
  (new
    (*parser (char #\#))
    (*parser <ProperList>)
    (*caten 2)

    (*pack
      (lambda (result)
        ;(display "<Vector> result: ")
        ;(display result)
        ;(newline)

        ;(display "<Vector> car of result: ")
        ;(display (car result))
        ;(newline)

        ;(display "<Vector> cdr of result: ")
        ;(display (cdr result))
        ;(newline)

        ;(display "<Vector> cadr of result: ")
        ;(display (cadr result))
        ;(newline)

        (list->vector (car (cdr  result)))))
  done))
;; =========================================================================================================
;; ================================== Parsers defining the <Vector> parser =================================
;; =========================================================================================================


;; =========================================================================================================
;; ================================== Parsers defining the <Quoted> parser =================================
;; =========================================================================================================
(define <Quoted>
  (new
    (*parser (char #\'))
    (*parser <CommentOrWhiteSpace>) *star
    (*delayed (lambda () <sexpr>))
    (*caten 3)
    
    (*pack-with
          (lambda (commentOrWhiteSpace quotePrefix result)
            ;(display "<Quoted> result: ")
            ;(display result)
            ;(newline)
            (list 'quote result)))
  done))
;; =========================================================================================================
;; ================================== Parsers defining the <Quoted> parser =================================
;; =========================================================================================================


;; =========================================================================================================
;; ================================ Parsers defining the <QuasiQuoted> parser ==============================
;; =========================================================================================================
(define <QuasiQuoted>
  (new
    (*parser (char #\`))
    (*parser <CommentOrWhiteSpace>) *star
    (*delayed (lambda () <sexpr>))
    (*caten 3)
    
    (*pack-with
          (lambda (commentOrWhiteSpace quotePrefix result)
            ;(display "<QuasiQuoted> result: ")
            ;(display result)
            ;(newline)
            (list 'quasiquote result)))
  done))
;; =========================================================================================================
;; ================================ Parsers defining the <QuasiQuoted> parser ==============================
;; =========================================================================================================


;; =========================================================================================================
;; ================================= Parsers defining the <Unquoted> parser ================================
;; =========================================================================================================
(define <Unquoted>
  (new
    (*parser (char #\,))
      (*parser <CommentOrWhiteSpace>) *star
      (*parser (char #\@))
      *not-followed-by
    (*delayed (lambda () <sexpr>))
    (*caten 3)
    
    (*pack-with
          (lambda (commentOrWhiteSpace quotePrefix result)
            ;(display "<Unquoted> result: ")
            ;(display result)
            ;(newline)
            (list 'unquote result)))
  done))
;; =========================================================================================================
;; ================================= Parsers defining the <Unquoted> parser ================================
;; =========================================================================================================


;; =========================================================================================================
;; ============================ Parsers defining the <UnquoteAndSpliced> parser ============================
;; =========================================================================================================
(define <UnquoteAndSpliced>
  (new
    (*parser (char #\,))
    (*parser (char #\@))
    (*parser <CommentOrWhiteSpace>) *star
    (*delayed (lambda () <sexpr>))
    (*caten 4)

    (*pack-with
          (lambda (comma strudl commentOrWhiteSpace result)
            ;(display "<UnquoteAndSpliced> result: ")
            ;(display result)
            ;(newline)
            (list 'unquote-splicing result)))
  done))
;; =========================================================================================================
;; ============================ Parsers defining the <UnquoteAndSpliced> parser ============================
;; =========================================================================================================


;; =========================================================================================================
;; ================================= Parsers defining the <CBName> parser ==================================
;; =========================================================================================================
(define <CBNameSyntax1>
  (new
    (*parser (char #\@))
    (*delayed (lambda () <sexpr>))
    (*caten 2)
    (*pack-with
        (lambda (shtrudl resultat)
          ;(display "<CBNameSyntax1> result: ")
          ;(display resultat)
          ;(newline)
          resultat))
    done))


(define <CBNameSyntax2>
  (new
    (*parser (char #\{))
    (*delayed (lambda () <sexpr>))
    (*parser (char #\}))
    (*caten 3)
    (*pack-with
        (lambda (sograyimRishonot totzaa sograyimAhronot)
          ;(display "<CBNameSyntax2> result: ")
          ;(display totzaa)
          ;(newline)
          totzaa))
    done))


(define <CBName>
  (new
    (*parser <CBNameSyntax1>)
    (*parser <CBNameSyntax2>)
    (*disj 2)
    (*pack
        (lambda (result)
          ;(display "<CBName> result: ")
          ;(display result)
          ;(newline)
          `(cbname ,result)))
  done))
;; =========================================================================================================
;; ================================= Parsers defining the <CBName> parser ==================================
;; =========================================================================================================


;; =========================================================================================================
;; ============================= Parsers defining the <InfixExtension> parser ==============================
;; =========================================================================================================

;; ============================== Parsers defining the <PowerSymbol> parser ================================
(define <PowerSymbol> 
  (new 
    (*parser (char #\^))

    (*parser (char #\*))
    (*parser (char #\*))
    (*caten 2)

    (*disj 2)

    (*pack
      (lambda (result)
        ;(display "<PowerSymbol> result: ")
        ;(display result)
        ;(newline)
        #\^))
  done))
;; ============================== Parsers defining the <PowerSymbol> parser ================================

;; ======================= Parsers defining the <InfixPrefixExtensionPrefix> parser ========================
(define <InfixPrefixExtensionPrefix>
  (new
    (*parser (char #\#))
    (*parser (char #\#))
    (*caten 2)

    (*parser (char #\#))
    (*parser (char #\%))
    (*caten 2)

    (*disj 2)

    (*pack
      (lambda (tulos) ;; finnish for result
        ;(display "<InfixPrefixExtensionPrefix> result: ")
        ;(display tulos)
        ;(newline)
        tulos))
  done))
;; ======================= Parsers defining the <InfixPrefixExtensionPrefix> parser ========================

;; ============================== Parsers defining the <InfixNumber> parser ================================
(define <InfixNumber>
  (new 
    (*parser <CommentOrWhiteSpace>) *star
    
    (*parser <Fraction>)
    (*parser <Integer>)
    (*disj 2)

    (*parser <CommentOrWhiteSpace>) *star
    (*caten 3)

    (*pack-with
      (lambda (sp1 result sp2)
        ;(display "<InfixNumber> result: ")
        ;(display result)
        ;(newline)
        result))
  done))
;; ============================== Parsers defining the <InfixNumber> parser ================================

;; ============================== Parsers defining the <InfixSymbol> parser ================================
(define <InfixSymbolChar>
  (new
    (*parser 
      (const 
        (lambda (ch) 
          (or 
            (and 
              (char>=? ch #\0)
              (char<=? ch #\9))
            (and
              (char>=? ch #\a)
              (char<=? ch #\z))
            (and
              (char>=? ch #\A)
              (char<=? ch #\Z))
            (char=? ch #\!)
            (char=? ch #\$)
            (char=? ch #\_)
            (char=? ch #\=)
            (char=? ch #\<)
            (char=? ch #\>)
            (char=? ch #\?)
            ))))

    (*pack 
        (lambda (result)
          ;(display "<InfixSymbolChar> result: ")
          ;(display result)
          ;(newline)
          (char-downcase result)))
  done))


(define <InfixSymbol>
  (new
    (*parser <CommentOrWhiteSpace>) *star

    (*parser <InfixSymbolChar>) *plus
    (*parser <InfixNumber>)
    *diff
    (*pack
        (lambda (result)
          ;(display "<InfixSymbol> result: ")
          ;(display result)
          ;(newline)
          (string->symbol (list->string result))))

    (*parser <InfixNumber>)
    (*disj 2)

    (*parser <CommentOrWhiteSpace>) *star
    (*caten 3)

    (*pack-with
      (lambda (sp1 sym sp2)
        ;(display "<InfixSymbol> sym: ")
        ;(display sym)
        ;(newline)
        sym))
  done))
;; ============================== Parsers defining the <InfixSymbol> parser ================================

;; ============================ Parsers defining the <InfixSexprEscape> parser =============================
(define <InfixSexprEscape> 
  (new 
    (*parser <CommentOrWhiteSpace>) *star

    (*parser <InfixPrefixExtensionPrefix>)
    (*parser <CommentOrWhiteSpace>) *star
    (*delayed (lambda () <sexpr>))
    (*caten 3)

    (*pack
      (lambda (result)
        ;(display "<InfixSexprEscape> result2: ")
        ;(display result)
        ;(newline)
        result))

    (*pack-with
      (lambda (ifxPrfxExPrfx whtSpc sexpr)
        sexpr))

    (*parser <InfixSymbol>)
    (*disj 2)

    (*parser <CommentOrWhiteSpace>) *star
    (*caten 3)

    (*pack-with 
      	(lambda (sp1 infEsc sp2)
        	;(display "<InfixSexprEscape> infEsc: ")
        	;(display infEsc)
        	;(newline)
        	infEsc))

  done))
;; ============================ Parsers defining the <InfixSexprEscape> parser =============================

;; ================================ Parsers defining the <InfixNeg> parser =================================
(define <InfixNeg> 
  (new 
    (*parser <CommentOrWhiteSpace>) *star

    (*parser (char #\-))
    (*delayed (lambda () <InfixArrayGetOrFuncall>))
    (*parser <InfixNumber>)
    *diff
    (*caten 2)

    (*pack-with
        (lambda (minusSymbol param)
          	;(display "<InfixNeg> minusSymbol: ")
          	;(display minusSymbol)
          	;(newline)
          	;(display "<InfixNeg> param: ")
          	;(display param)
          	;(newline)
          	(list '- param)))
    
    (*parser <InfixSexprEscape>)
    (*disj 2)

    (*parser <CommentOrWhiteSpace>) *star
    (*caten 3)

    (*pack-with 
      	(lambda (sp1 infNeg sp2)
        	;(display "<InfixNeg> infNeg: ")
        	;(display infNeg)
        	;(newline)
        	infNeg))
  done))
;; ================================ Parsers defining the <InfixNeg> parser =================================

;; =============================== Parsers defining the <InfixParen> parser ================================
(define <InfixParen> 
  (new 
    (*parser <CommentOrWhiteSpace>) *star

    (*parser (char #\())
    (*delayed (lambda () <InfixExpression>))
    (*parser (char #\)))
    (*caten 3)

    (*pack-with
      	(lambda (paren1 result paren2)
        	;(display "<InfixParen> result: ")
        	;(display result)
        	;(newline)
        	result))

    (*parser <InfixNeg>)
    (*disj 2)

    (*parser <CommentOrWhiteSpace>) *star
    (*caten 3)

    (*pack-with 
      	(lambda (sp1 infParen sp2)
        	;(display "<InfixParen> infParen: ")
        	;(display infParen)
        	;(newline)
        	infParen))
  done))
;; =============================== Parsers defining the <InfixParen> parser ================================

;; ========================= Parsers defining the <InfixArrayGetOrFuncall> parser ==========================
(define <InfixArgList> 
  (new 
    (*delayed (lambda () <InfixExpression>))
    (*pack 
    	(lambda (result)
            ;(display "<InfixArgList> result: ")
            ;(display result)
            ;(newline)
            (list result)))
    
    (*parser (char #\,))
    (*parser <CommentOrWhiteSpace>) *star
    (*delayed (lambda () <InfixExpression>))
    (*caten 3)
    (*pack-with
        (lambda (psik space delayed)
          (list psik delayed)))
    *star

    (*caten 2)

    (*pack-with
        (lambda (first starred)
          	;(display "<InfixArrayGetOrFuncall> first: ")
          	;(display first)
          	;(newline)
          	;(display "<InfixArrayGetOrFuncall> starred: ")
          	;(display starred)
          	;(newline)
          	(letrec ((fixList (lambda (first starred)
                                (if (null? starred)
                                    first 
                                    (fixList (append first (cdar starred)) (cdr starred))))))
              (fixList first starred))))

    (*parser <epsilon>)
    (*disj 2)
  done))


(define <InfixArrayGetOrFuncall>
  (new 
    (*parser <CommentOrWhiteSpace>) *star
    
    ; ARRAY FIRST
    (*parser <InfixSymbol>)
    (*parser (char #\[))
    (*delayed (lambda () <InfixExpression>))
    (*parser (char #\]))
    (*caten 4)
    (*pack
        (lambda (result)
          	;(display "<InfixArrayGet> result1: ")
          	;(display result)
          	;(newline)
          	result))

    (*pack-with
        (lambda (arrName openSqBrac indexCalc closeSqBrac)
          	;(display "<InfixArrayGet> arrName: ")
          	;(display arrName)
          	;(newline)

          	;(display "<InfixArrayGet> indexCalc: ")
          	;(display indexCalc)
          	;(newline)

          	(list 'vector-ref arrName indexCalc)))
    ; FUNCALL FIRST
    (*parser <InfixSymbol>)
    (*parser (char #\())
    (*parser <InfixArgList>)
    (*parser (char #\)))
    (*caten 4)
    (*pack
        (lambda (result)
          	;(display "<InfixFuncall> result1: ")
          	;(display result)
          	;(newline)
          	result))

    (*pack-with
        (lambda (funcName openBrac argList closeBrac)
          	;(display "<InfixFuncall> arrName: ")
          	;(display funcName)
          	;(newline)

          	;(display "<InfixFuncall> argList: ")
          	;(display argList)
          	;(newline)

          	(append (list funcName) argList)))

    (*disj 2)

    ; ARRAY REST
    (*parser (char #\[))
    (*delayed (lambda () <InfixExpression>))
    (*parser (char #\]))
    (*caten 3)  
    (*pack-with (lambda (openSqBrac indexCalc closeSqBrac)
                    (list 'vector-ref indexCalc)))
    ; FUNCALL REST
    (*parser (char #\())
    (*parser <InfixArgList>)
    (*parser (char #\)))
    (*caten 3)
    (*pack-with
        (lambda (openBrac argList closeBrac)
          argList))

    (*disj 2)
    *star    
    (*caten 2) ; caten between first and rest


    (*pack
        (lambda (result)
          	;(display "<InfixArrayGet> result2: ")
          	;(display result)
          	;(newline)
          	result))

    (*pack-with
        (lambda (first starred)
          	;(display "<InfixArrayGet> first: ")
          	;(display first)
          	;(newline)
          	;(display "<InfixArrayGet> starred: ")
          	;(display starred)
          	;(newline)
          	(letrec ((reorder (lambda (frst strrd)
                                (if (null? strrd)
                                    frst
                                    (if (equal? (caar strrd) 'vector-ref)
                                        (reorder (list (caar strrd) frst (cadar strrd)) (cdr strrd)); reorder for vref
                                        (reorder (append (list frst) (car strrd)) (cdr strrd))))))) ; reorder for fcall
              (reorder first starred))))

    (*parser <InfixParen>)
    (*disj 2)

    (*parser <CommentOrWhiteSpace>) *star
    (*caten 3)
    (*pack-with 
      	(lambda (sp1 infExp sp2)
        	;(display "<InfixArrayGet> infExp: ")
        	;(display infExp)
        	;(newline)
        	infExp))
  done))
;; ========================= Parsers defining the <InfixArrayGetOrFuncall> parser ==========================

;; ================================ Parsers defining the <InfixPow> parser =================================
(define <InfixPow> 
  (new 
    (*parser <CommentOrWhiteSpace>) *star

    (*parser <InfixArrayGetOrFuncall>)
    (*parser <CommentOrWhiteSpace>) *star
    (*parser <PowerSymbol>)
    (*parser <CommentOrWhiteSpace>) *star
    (*delayed (lambda() <InfixPow>))
    (*caten 5)
    (*pack-with
      	(lambda (num1 sp1 op sp2 num2)
        	(list num1 op num2)))

    (*pack
        (lambda (result)
          	;(display "<InfixPow> result1: ")
          	;(display result)
          	;(newline)
          	result))

    (*pack-with
        (lambda (param1 InfixPow param2)
          	;(display "<InfixPow> param1: ")
          	;(display param1)
          	;(newline)

          	;(display "<InfixPow> InfixPow: ")
          	;(display InfixPow)
          	;(newline)

          	;(display "<InfixPow> param2: ")
          	;(display param2)
          	;(newline)

          	(list 'expt param1 param2)))

    (*parser <CommentOrWhiteSpace>) *star
    (*parser <PowerSymbol>)
    (*pack 
    	(lambda (InfixPow) 
            'expt))
    (*parser <CommentOrWhiteSpace>) *star

    (*parser <InfixArrayGetOrFuncall>)
    (*caten 4)
    (*pack-with 
    	(lambda (sp1 op sp2 num)
            (list op num)))
    *star
    (*caten 2)

    (*pack
        (lambda (result)
          	;(display "<InfixPow> result2: ")
          	;(display result)
          	;(newline)
          	result))

    (*pack-with
        (lambda (first starred)
          	;(display "<InfixPow> first: ")
          	;(display first)
          	;(newline)
          	;(display "<InfixPow> starred: ")
          	;(display starred)
          	;(newline)
          	(letrec ((reorder (lambda (frst strrd)
                                (if (null? strrd)
                                    frst
                                    (reorder (list (caar strrd) frst (cadar strrd)) (cdr strrd))))))
              		(reorder first starred))))

    (*parser <InfixArrayGetOrFuncall>)
    (*disj 2)

    (*parser <CommentOrWhiteSpace>) *star
    (*caten 3)
    (*pack-with 
      	(lambda (sp1 infExp sp2)
        	;(display "<InfixPow> infExp: ")
        	;(display infExp)
        	;(newline)
        	infExp))
  done))
;; ================================ Parsers defining the <InfixPow> parser =================================

;; ============================== Parsers defining the <InfixMulOrDiv> parser ==============================
(define <InfixMulOrDiv> 
  (new 
    (*parser <CommentOrWhiteSpace>) *star

    (*parser <InfixPow>)
    (*parser <CommentOrWhiteSpace>) *star
    (*parser (char #\*))
    (*parser (char #\/))
    (*disj 2)
    (*parser <CommentOrWhiteSpace>) *star
    (*parser <InfixPow>)
    (*caten 5)
    (*pack-with
      	(lambda (num1 sp1 op sp2 num2)
        	(list num1 op num2)))

    (*pack
        (lambda (result)
          	;(display "<InfixMulOrDiv> result1: ")
          	;(display result)
          	;(newline)
          	result))

    (*pack-with
        (lambda (param1 mulOrDiv param2)
          	;(display "<InfixMulOrDiv> param1: ")
          	;(display param1)
          	;(newline)

          	;(display "<InfixMulOrDiv> mulOrDiv: ")
          	;(display mulOrDiv)
          	;(newline)

          	;(display "<InfixMulOrDiv> param2: ")
          	;(display param2)
          	;(newline)

          	(if (equal? mulOrDiv #\*)
            	(list '* param1 param2)
            	(list '/ param1 param2))))

    (*parser <CommentOrWhiteSpace>) *star
    (*parser (char #\*))
    (*parser (char #\/))
    (*disj 2)
    (*pack 
    	(lambda (mulOrDiv)
            (if (equal? mulOrDiv #\*)
                '*
                '/)))
    (*parser <CommentOrWhiteSpace>) *star

    (*parser <InfixPow>)
    (*caten 4)
    (*pack-with 
    	(lambda (sp1 op sp2 num)
            (list op num)))
    *star
    (*caten 2)

    (*pack
        (lambda (result)
          	;(display "<InfixMulOrDiv> result2: ")
          	;(display result)
          	;(newline)
          	result))

    (*pack-with
        (lambda (first starred)
          	;(display "<InfixMulOrDiv> first: ")
          	;(display first)
          	;(newline)
          	;(display "<InfixMulOrDiv> starred: ")
          	;(display starred)
          	;(newline)
          	(letrec ((reorder (lambda (first starred)
                                (if (null? starred)
                                    first
                                    (reorder (list (caar starred) first (cadar starred)) (cdr starred))))))
              	(reorder first starred))))

    (*parser <InfixPow>)
    (*disj 2)

    (*parser <CommentOrWhiteSpace>) *star
    (*caten 3)
    (*pack-with 
      	(lambda (sp1 infExp sp2)
        	;(display "<InfixMulOrDiv> infExp: ")
        	;(display infExp)
        	;(newline)
        	infExp))
  done))
;; ============================== Parsers defining the <InfixMulOrDiv> parser ==============================

;; ============================== Parsers defining the <InfixAddOrSub> parser ==============================
(define <InfixAddOrSub> 
  (new 
    (*parser <CommentOrWhiteSpace>) *star

    (*parser <InfixMulOrDiv>)
    (*parser <CommentOrWhiteSpace>) *star
    (*parser (char #\+))
    (*parser (char #\-))
    (*disj 2)
    (*parser <CommentOrWhiteSpace>) *star
    (*parser <InfixMulOrDiv>)
    (*caten 5)
    (*pack-with
      	(lambda (num1 sp1 op sp2 num2)
        	(list num1 op num2)))

    (*pack
        (lambda (result)
          	;(display "<InfixAddOrSub> result1: ")
          	;(display result)
          	;(newline)
          	result))

    (*pack-with
        (lambda (param1 addOrSub param2)
          	;(display "<InfixAddOrSub> param1: ")
          	;(display param1)
          	;(newline)

          	;(display "<InfixAddOrSub> addOrSub: ")
          	;(display addOrSub)
          	;(newline)

          	;(display "<InfixAddOrSub> param2: ")
          	;(display param2)
          	;(newline)

          	(if (equal? addOrSub #\+)
            	(list '+ param1 param2)
            	(list '- param1 param2))))

    (*parser <CommentOrWhiteSpace>) *star
    (*parser (char #\+))
    (*parser (char #\-))
    (*disj 2)
    (*pack (lambda (addOrSub)
              (if (equal? addOrSub #\+)
                  '+
                  '-)))
    (*parser <CommentOrWhiteSpace>) *star

    (*parser <InfixMulOrDiv>)
    (*caten 4)
    (*pack-with (lambda (sp1 op sp2 num)
                    (list op num)))
    *star
    (*caten 2)

    (*pack
        (lambda (result)
          	;(display "<InfixAddOrSub> result2: ")
          	;(display result)
          	;(newline)
          	result))

    (*pack-with
        (lambda (first starred)
          	;(display "<InfixAddOrSub> first: ")
          	;(display first)
          	;(newline)
          	;(display "<InfixAddOrSub> starred: ")
          	;(display starred)
          	;(newline)
          	(letrec ((reorder (lambda (first starred)
                                (if (null? starred)
                                    first
                                    (reorder (list (caar starred) first (cadar starred)) (cdr starred))))))
              	(reorder first starred))))

    (*parser <InfixMulOrDiv>)
    (*disj 2)

    (*parser <CommentOrWhiteSpace>) *star
    (*caten 3)
    (*pack-with 
      	(lambda (sp1 infExp sp2)
        	;(display "<InfixAddOrSub> infExp: ")
        	;(display infExp)
        	;(newline)
        	infExp))
  done))
;; ============================== Parsers defining the <InfixAddOrSub> parser ==============================

;; ============================= Parsers defining the <InfixExpression> parser =============================
(define <InfixExpression>
  (new 
    (*parser <CommentOrWhiteSpace>) *star
    (*parser <InfixAddOrSub>)
    (*parser <CommentOrWhiteSpace>) *star
    (*parser <end-of-input>)
    (*disj 2)
    (*caten 3)

    (*pack-with 
      	(lambda (x canlyniad y) ;; welsh for result
        	;(display "<InfixExpression> result: ")
        	;(display canlyniad)
        	;(newline)
        	canlyniad))
  done))
;; ============================= Parsers defining the <InfixExpression> parser =============================

;; ===================================== Main <InfixExpression> parser =====================================
(define <InfixExtension>
  (new
    (*parser <InfixPrefixExtensionPrefix>)
    (*parser <InfixExpression>)

    (*caten 2)
    (*pack-with
      	(lambda (prefix exitum) ;; latin for result
        	;(display "<InfixExtension> result: ")
        	;(display exitum)
        	;(newline)
      		exitum))
  done))
;; ===================================== Main <InfixExpression> parser =====================================

;; =========================================================================================================
;; ============================= Parsers defining the <InfixExtension> parser ==============================
;; =========================================================================================================


;; =========================================================================================================
;; ========================================== Main <sexpr> parser ==========================================
;; =========================================================================================================
(define <sexpr>
  (new 
    (*parser <CommentOrWhiteSpace>) *star
    
    (*parser <Boolean>)
    (*parser <Char>)
    (*parser <Number>)
    (*parser <String>)
    (*parser <Symbol>)
    (*parser <ProperList>)
    (*parser <ImproperList>)
    (*parser <Vector>)
    (*parser <Quoted>)
    (*parser <QuasiQuoted>)
    (*parser <Unquoted>)
    (*parser <UnquoteAndSpliced>)
    (*parser <CBName>)
    (*parser <InfixExtension>)
    (*disj 14)

    (*parser <CommentOrWhiteSpace>) *star
    (*parser <end-of-input>)
    (*disj 2)

    (*caten 3)
    (*pack-with (lambda (x sexpr y) sexpr))
  done))
;; =========================================================================================================
;; ========================================== Main <sexpr> parser ==========================================
;; =========================================================================================================

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Parser Implementation ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;